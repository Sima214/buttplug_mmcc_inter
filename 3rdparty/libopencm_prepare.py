from pathlib import Path

import subprocess
import sys
import re
import os

VPATH_PATTERN = re.compile(r'VPATH\s*\+=\s*(.*)\n')
OBJS_PATTERN = re.compile(r'OBJS\s*\+=\s*(.*)\n')
INC_PATTERN = re.compile(r'include\s*\s*(.*)\n')


def find_obj(search_dirs, name):
    src = re.sub(r'\.o', '.c', name)
    for d in search_dirs:
        fp = d.joinpath(src).resolve()
        if fp.exists():
            return fp
    print("Couldn't find '%s'!" % src, file=sys.stderr)
    sys.exit(1)


def process_makefile(makefile: Path, search_dirs=[]):
    makepath = makefile.parent
    search_dirs.append(makepath)
    # Load lines.
    lines = []
    with open(makefile, 'r') as f:
        lines = f.readlines()
    # VPATH append.
    for l in lines:
        m = VPATH_PATTERN.match(l)
        if m is not None:
            paths = m.groups()[0].split(':')
            for p in paths:
                search_dirs.append(makepath.joinpath(p).resolve())
    # OBJS scanning.
    srcs = []
    for l in lines:
        m = OBJS_PATTERN.match(l)
        if m is not None:
            objs = m.groups()[0].split(' ')
            for o in objs:
                srcs.append(find_obj(search_dirs, o))
    # include recursion.
    for l in lines:
        m = INC_PATTERN.match(l)
        if m is not None:
            mkf = makepath.joinpath(m.groups()[0]).resolve()
            srcs.extend(process_makefile(mkf, search_dirs))
    # Exit condition.
    return srcs


# Special script for make libopencm3 compatible with meson.
source = Path(os.environ['MESON_SOURCE_ROOT']).joinpath(os.environ['MESON_SUBDIR'])
root = source.joinpath(sys.argv[1])
lib_root = root.joinpath('lib')
inc_root = root.joinpath('include')

target_family = sys.argv[2]
target_series = sys.argv[3]

# Generate headers.
irq2nvic = root.joinpath('scripts').joinpath('irq2nvic_h')
irqhp = inc_root.joinpath('libopencm3').joinpath(
    target_family).joinpath(target_series).joinpath('irq.json')
ret_proc = subprocess.run([str(irq2nvic), "./" + str(irqhp.relative_to(root))], cwd=root)
if ret_proc.returncode != 0:
    sys.exit(ret_proc.returncode)

# Find source files.
makepath = lib_root.joinpath(target_family).joinpath(target_series)
sources = process_makefile(makepath.joinpath('Makefile'))

for sp in sources:
    rp = sp.relative_to(source)
    print(str(rp))
