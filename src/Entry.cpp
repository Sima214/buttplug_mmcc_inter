#include <interface/RTOS.hpp>
#include <mmcc/OS.hpp>

#include <cstdint>

#include <boot.h>

void rtos_init() {
    // Load freeRTOS interrupts into vector table.
    runtime_vector_table.sv_call = &vPortSVCHandler;
    runtime_vector_table.pend_sv = &xPortPendSVHandler;
    runtime_vector_table.systick = &xPortSysTickHandler;
    // Switch to FreeRTOS.
    rtos::Kernel::get_instance().start();
}

int main() {
    rtos_init();
    return 0;
}
