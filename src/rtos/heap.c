#include <FreeRTOS.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

__attribute__((section(".heap"))) uint8_t ucHeap[configTOTAL_HEAP_SIZE];

void free(void* ptr) {
    vPortFree(ptr);
}

void* malloc(size_t size) {
    return pvPortMalloc(size);
}

int posix_memalign(void** memptr, size_t alignment, size_t size) {
    *memptr = pvPortMalloc(size);
    return 0;
}
