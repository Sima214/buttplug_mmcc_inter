#ifndef MMCC_ACTUATOR_HPP
#define MMCC_ACTUATOR_HPP

#include <interface/RTOSQueue.hpp>
#include <interface/RTOSTask.hpp>
#include <interface/io/GPIO.hpp>
#include <interface/io/Timer.hpp>
#include <utils/PidController.hpp>

namespace mmcc {

class Actuator final : public rtos::StaticTask<256> {
   protected:
    stm32::Pin _encoder_pins;
    stm32::timer::Timer<TIM2>& _encoder;
    uint16_t _encoder_start = 0x4000;
    uint16_t _encoder_end = 0x4000;

    stm32::Pin _pwm_pins;
    stm32::Pin _pwmn_pins;
    stm32::timer::AdvancedTimer<TIM1>& _pwm;

    stm32::Pin _aux_pins;
    stm32::timer::Timer<TIM3>& _aux;

    pid::Control _pid;
    uint32_t _pid_tick_count = 0;
    stm32::timer::Timer<TIM11>& _pid_ticker;

    bool _action_inverted = false;
    bool _action_stopped = true;
    float _action_last_position = 0;
    float _action_target_position = 0;
    float _action_speed = 0;
    uint32_t _action_ticks_remaining = 0;

    struct Event {
        bool stop;
        float target_pos;
        float duration_ms;
    };
    rtos::StaticQueue<Event, 8> _queue;

   public:
    Actuator();
    virtual ~Actuator() = default;

    virtual void run() override;
    void on_config_complete();

    void set_pwm(float duty);
    void set_pwm_freewheeling(bool fw);
    void set_encoder_limits(uint16_t start, uint16_t end);
    float get_encoder_position();
    void set_pid_tunings(float kp, float ki, float kd);
    void set_actuation_invert();
    void set_actuation(bool stop, float target_pos, float duration_ms);

   private:
    static int main(int argc, char* argv[]);
    static void on_pid_update();
};

}  // namespace mmcc

#endif /*MMCC_ACTUATOR_HPP*/