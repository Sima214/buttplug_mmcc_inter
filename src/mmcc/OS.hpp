#ifndef MMCC_OS_HPP
#define MMCC_OS_HPP

#include <interface/io/GPIO.hpp>
#include <interface/RTOS.hpp>

#include <functional>
#include <optional>

namespace mmcc {

class Kernel final : public rtos::Kernel {
   protected:
    StaticTask_t _idle_task_tcb;
    StackType_t _idle_task_stack[configMINIMAL_STACK_SIZE];
    stm32::Pin _indicator_led;

   public:
    Kernel();
    virtual ~Kernel() = default;

    virtual void get_idle_task_memory(StaticTask_t** idle_task_tcb_buffer,
                                      StackType_t** idle_task_stack_buffer,
                                      uint32_t* idle_task_stack_size) override;

    virtual void on_pre_sleep_processing(TickType_t& expected_idle_time) override;
    virtual void on_post_sleep_processing(TickType_t& expected_idle_time) override;
    virtual void on_start() override;
    virtual void on_end() override;

   public:
    static Kernel instance;
};

}  // namespace mmcc

#endif /*MMCC_OS_HPP*/