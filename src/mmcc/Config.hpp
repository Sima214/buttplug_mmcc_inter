#ifndef MMCC_CONFIG_HPP
#define MMCC_CONFIG_HPP

#include <interface/RTOSMutex.hpp>
#include <interface/io/GPIO.hpp>
#include <interface/io/I2C.hpp>

#include <cstring>
#include <optional>

namespace mmcc {

class ConfigStore final {
   public:
    enum Index { Signature };
    struct Store {
        Index index : 4;
        uint8_t version : 4;
        union __attribute__((__packed__)) {
            uint8_t raw[14];
        } data;

        Store() = default;
        Store(Index i, uint8_t v) : index(i), version(v) {
            std::memset(this->data.raw, 0x0, sizeof(this->data.raw));
        }
    };

   protected:
    stm32::Pin _i2c_pins;
    stm32::I2C<I2C1>& _i2c;

    rtos::StaticMutex<true> _config_lock;

    int16_t _eeprom_signature_sector = -1;
    int16_t _eeprom_next_sector = 0;

   public:
    ConfigStore();
    ~ConfigStore() = default;

    void eeprom_init();
    bool set(const Store& elem);
    std::optional<Store> get(Index index, const Store* def = nullptr);

   protected:
    std::optional<Store> _get(Index index, const Store* def = nullptr);
    /**
     * Get the mapped or the next free sector.
     */
    int16_t index2sector(Index i);
    int16_t eeprom_next_free_sector(int16_t start_sector);

    size_t eeprom_read(uint8_t* data, size_t n, uint16_t address);
    size_t eeprom_write(const uint8_t* data, size_t n, uint16_t address, bool verify = false);

   protected:
    static int main_i2c(int, char**);
    static int main_config(int, char**);
};

}  // namespace mmcc

#endif /*MMCC_CONFIG_HPP*/