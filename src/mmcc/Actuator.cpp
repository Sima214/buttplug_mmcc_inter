#include "Actuator.hpp"

#include <Objects.hpp>

#include <cstdint>
#include <cstdlib>

#include <boot.h>
#include <libopencm3/cm3/nvic.h>

#define PWM_PERIOD (0x600)

namespace mmcc {

int Actuator::main(int argc, char* argv[]) {
    if (argc == 1) {
        char buf[96];
        int written;

        if (actuator_task._action_stopped) {
            cli_task.print("Actuator stopped.\n");
        }
        else {
            cli_task.print("Actuator active.\n");
        }
        written = std::snprintf(buf, sizeof(buf), "Last position: %f\n",
                                actuator_task._action_last_position);
        cli_task.print(buf, written);
        written = std::snprintf(buf, sizeof(buf), "Target position: %f\n",
                                actuator_task._action_target_position);
        cli_task.print(buf, written);
        written = std::snprintf(buf, sizeof(buf), "Speed: %f\n", actuator_task._action_speed);
        cli_task.print(buf, written);
        written = std::snprintf(buf, sizeof(buf), "Ticks remain: %lu\n",
                                actuator_task._action_ticks_remaining);
        cli_task.print(buf, written);
        written = std::snprintf(buf, sizeof(buf), "PID tick count: %lu\n",
                                actuator_task._pid_tick_count);
        cli_task.print(buf, written);
        if (actuator_task._encoder_start != actuator_task._encoder_end) {
            written = std::snprintf(buf, sizeof(buf), "Current position: %f\n",
                                    actuator_task.get_encoder_position());
            cli_task.print(buf, written);
        }
        return 0;
    }
    if (!strcmp(argv[1], "encoder")) {
        char buf[96];
        int written;
        if (argc == 2) {
            // Print current state.
            written = std::snprintf(buf, sizeof(buf), "Encoder position:\t%lu\n",
                                    actuator_task._encoder.get_counter());
            cli_task.print(buf, written);
            return 0;
        }
        else {
            cli_task.print("Invalid encoder arguments.");
            return 2;
        }
    }
    else if (!strcmp(argv[1], "pwm")) {
        if (argc == 3) {
            if (!strcmp(argv[2], "fw")) {
                actuator_task.set_pwm_freewheeling(true);
            }
            else if (!strcmp(argv[2], "br")) {
                actuator_task.set_pwm_freewheeling(false);
            }
            else if (!strcmp(argv[2], "off")) {
                actuator_task.set_pwm(0.0);
            }
            else {
                actuator_task.set_pwm(std::strtof(argv[2], nullptr));
            }
            return 0;
        }
        else {
            cli_task.print("Invalid pwm arguments.");
            return 2;
        }
    }
    else if (!strcmp(argv[1], "limits")) {
        if (argc == 4) {
            uint32_t a = std::strtoul(argv[2], nullptr, 0);
            uint32_t b = std::strtoul(argv[3], nullptr, 0);
            actuator_task.set_encoder_limits(a, b);
            return 0;
        }
        else {
            cli_task.print("Not enough arguments.");
            return 2;
        }
    }
    else if (!strcmp(argv[1], "invert")) {
        if (argc == 2) {
            actuator_task.set_actuation_invert();
            return 0;
        }
        else {
            cli_task.print("Invalid arguments to invert.");
            return 2;
        }
    }
    else if (!strcmp(argv[1], "tune")) {
        if (argc == 5) {
            float a = std::strtof(argv[2], nullptr);
            float b = std::strtof(argv[3], nullptr);
            float c = std::strtof(argv[4], nullptr);
            actuator_task.set_pid_tunings(a, b, c);
            return 0;
        }
        else {
            cli_task.print("Not enough arguments.");
            return 2;
        }
    }
    else {
        char buf[96];
        int written = std::snprintf(buf, sizeof(buf), "Unknown subcommand `%s`!\n", argv[1]);
        cli_task.print(buf, written);
        return 2;
    }
}

Actuator::Actuator() :
    rtos::StaticTask<256>("actuator", 6),
    _encoder_pins(stm32::Port<GPIOA>::get_port().get_pins(1, 5)),
    _encoder(stm32::timer::Timer<TIM2>::get_instance()),
    _pwm_pins(stm32::Port<GPIOA>::get_port().get_pins(9, 10)),
    _pwmn_pins(stm32::Port<GPIOB>::get_port().get_pins(14, 15)),
    _pwm(stm32::timer::AdvancedTimer<TIM1>::get_instance()),
    _aux_pins(stm32::Port<GPIOB>::get_port().get_pins(4, 5)),
    _aux(stm32::timer::Timer<TIM3>::get_instance()),
    _pid(1, 0, 0, 0.005, -1.0, 1.0, pid::Mode::Automatic, pid::Direction::Direct),
    _pid_ticker(stm32::timer::Timer<TIM11>::get_instance()) {
    cli_task.register_command("actuator", "Actuator status, config and bypass control.", main);
}

void Actuator::on_config_complete() {
    _encoder_pins.set_direction(stm32::Pin::Direction::AlternateFunction);
    _encoder_pins.set_pullres(false, true);
    _encoder_pins.set_function(stm32::Pin::Function::AlternateFunction01);
    _encoder.set_preload(false);
    _encoder.set_period(0x8000);
    _encoder.set_counter(0x4000);
    _encoder.set_slave_mode(stm32::timer::SlaveMode::EncoderMode3);
    _encoder.set_ic_polarity(stm32::timer::InputChannel::IC1,
                             stm32::timer::InputChannelPolarity::Falling);
    _encoder.set_ic_input(stm32::timer::InputChannel::IC1,
                          stm32::timer::InputChannelConf::IN_TI1);
    _encoder.set_ic_filter(stm32::timer::InputChannel::IC1,
                           stm32::timer::InputChannelFilter::DTF_DIV_32_N_8);
    _encoder.set_ic_polarity(stm32::timer::InputChannel::IC2,
                             stm32::timer::InputChannelPolarity::Falling);
    _encoder.set_ic_input(stm32::timer::InputChannel::IC2,
                          stm32::timer::InputChannelConf::IN_TI2);
    _encoder.set_ic_filter(stm32::timer::InputChannel::IC2,
                           stm32::timer::InputChannelFilter::DTF_DIV_32_N_8);
    _encoder.enable();

    _pwm_pins.set_direction(stm32::Pin::Direction::AlternateFunction);
    // _pwm_pins.set_pullres(false, true);
    _pwm_pins.set_function(stm32::Pin::Function::AlternateFunction01);
    _pwmn_pins.set_direction(stm32::Pin::Direction::AlternateFunction);
    // _pwmn_pins.set_pullres(true, false);
    _pwmn_pins.set_function(stm32::Pin::Function::AlternateFunction01);

    _pwm.set_mode(stm32::timer::CkIntDiv::DIV1, stm32::timer::Alignment::Center1,
                  stm32::timer::Direction::Up);
    _pwm.set_prescaler(1);
    _pwm.set_period(PWM_PERIOD - 1);
    _pwm.set_counter(0x0);

    _pwm.set_oc_polarity(stm32::timer::OutputChannel::OC2,
                         stm32::timer::OutputChannelPolarity::Buffer);
    _pwm.set_oc_polarity(stm32::timer::OutputChannel::OC2N,
                         stm32::timer::OutputChannelPolarity::Buffer);
    _pwm.set_oc_polarity(stm32::timer::OutputChannel::OC3,
                         stm32::timer::OutputChannelPolarity::Buffer);
    _pwm.set_oc_polarity(stm32::timer::OutputChannel::OC3N,
                         stm32::timer::OutputChannelPolarity::Buffer);

    _pwm.set_oc_mode(stm32::timer::OutputChannel::OC2,
                     stm32::timer::OutputChannelMode::ForceLow);
    _pwm.set_oc_mode(stm32::timer::OutputChannel::OC3,
                     stm32::timer::OutputChannelMode::ForceLow);

    _pwm.set_oc_idle_state(stm32::timer::OutputChannel::OC2, false);
    _pwm.set_oc_idle_state(stm32::timer::OutputChannel::OC2N, false);
    _pwm.set_oc_idle_state(stm32::timer::OutputChannel::OC3, false);
    _pwm.set_oc_idle_state(stm32::timer::OutputChannel::OC3N, false);

    _pwm.set_idle_state_in_idle_mode(true);
    // _pwm.set_deadtime(24);  // ~250ns

    _pwm.set_oc_value(stm32::timer::OutputChannel::OC2, 0x0);
    _pwm.set_oc_value(stm32::timer::OutputChannel::OC3, 0x0);
    _pwm.set_oc_output(stm32::timer::OutputChannel::OC2, true);
    _pwm.set_oc_output(stm32::timer::OutputChannel::OC2N, true);
    _pwm.set_oc_output(stm32::timer::OutputChannel::OC3, true);
    _pwm.set_oc_output(stm32::timer::OutputChannel::OC3N, true);

    _pwm.generate_event(stm32::timer::Event::CΟΜG);
    _pwm.generate_event(stm32::timer::Event::UG);
    _pwm.enable();

    _aux_pins.set_direction(stm32::Pin::Direction::AlternateFunction);
    _aux_pins.set_function(stm32::Pin::Function::AlternateFunction02);
    // TODO: aux pwm

    _pid_ticker.set_mode(stm32::timer::CkIntDiv::DIV1, stm32::timer::Alignment::Edge,
                         stm32::timer::Direction::Up);
    _pid_ticker.set_prescaler(4800 - 1);
    _pid_ticker.set_period(100 - 1);
    _pid_ticker.set_counter(0x0);
    _pid_ticker.set_interrupt(TIM_DIER_UIE, true);
    runtime_vector_table.irq[NVIC_TIM1_TRG_COM_TIM11_IRQ] = on_pid_update;
    nvic_set_priority(NVIC_TIM1_TRG_COM_TIM11_IRQ, 0x30);
    nvic_enable_irq(NVIC_TIM1_TRG_COM_TIM11_IRQ);

    cli_task.print("Actuator is ready!\n");
}

void Actuator::run() {
    while (true) {
        auto msg = _queue.receive();
        if (msg) {
            // Optional `hardware` effects.
            if (_action_inverted) {
                msg->target_pos = 1.0 - msg->target_pos;
            }
            // Apply action.
            if (msg->stop && !_action_stopped) {
                // Quickly stop actuator.
                _pid_ticker.disable();
                set_pwm(0.0);
                _action_stopped = true;
            }
            else if (!msg->stop && _action_stopped) {
                // Start motor.
                _action_last_position = get_encoder_position();
                _action_target_position = msg->target_pos;
                _action_ticks_remaining = ((msg->duration_ms) * 200) / 1000;
                _action_speed = (_action_target_position - _action_last_position) /
                                _action_ticks_remaining;
                _action_stopped = false;
                _pid.set_setpoint(_action_last_position);
                _pid_ticker.enable();
            }
            else if (!msg->stop && !_action_stopped) {
                // Just update.
                _action_last_position = get_encoder_position();
                _action_target_position = msg->target_pos;
                _action_ticks_remaining = ((msg->duration_ms) * 200) / 1000;
                _action_speed = (_action_target_position - _action_last_position) /
                                _action_ticks_remaining;
                _pid.set_setpoint(_action_last_position);
            }
        }
    }
}

void Actuator::set_pwm(float duty) {
    bool dir = duty < 0.0;
    if (dir) {
        duty = -duty;
    }
    bool stop = duty <= 1e-2;
    const auto PWM_PERIOD_HALF = PWM_PERIOD / 2;
    uint32_t ocv = PWM_PERIOD_HALF + duty * PWM_PERIOD_HALF;
    {
        mmcc::InterruptGuard _g;
        _pwm.disable();

        if (stop) {
            _pwm.set_break_main_output(false);
            _pwm.set_oc_value(stm32::timer::OutputChannel::OC2, 0x0);
            _pwm.set_oc_value(stm32::timer::OutputChannel::OC3, 0x0);
            _pwm.set_oc_mode(stm32::timer::OutputChannel::OC2,
                             stm32::timer::OutputChannelMode::ForceLow);
            _pwm.set_oc_mode(stm32::timer::OutputChannel::OC3,
                             stm32::timer::OutputChannelMode::ForceLow);
        }
        else {
            _pwm.set_break_main_output(true);
            _pwm.set_oc_value(stm32::timer::OutputChannel::OC2, ocv);
            _pwm.set_oc_value(stm32::timer::OutputChannel::OC3, ocv);
            if (dir) {
                _pwm.set_oc_mode(stm32::timer::OutputChannel::OC2,
                                 stm32::timer::OutputChannelMode::Pwm1);
                _pwm.set_oc_mode(stm32::timer::OutputChannel::OC3,
                                 stm32::timer::OutputChannelMode::ForceLow);
            }
            else {
                _pwm.set_oc_mode(stm32::timer::OutputChannel::OC2,
                                 stm32::timer::OutputChannelMode::ForceLow);
                _pwm.set_oc_mode(stm32::timer::OutputChannel::OC3,
                                 stm32::timer::OutputChannelMode::Pwm1);
            }
        }

        _pwm.generate_event(stm32::timer::Event::CΟΜG);
        _pwm.generate_event(stm32::timer::Event::UG);
        _pwm.enable();
    }
}

void Actuator::set_pwm_freewheeling(bool fw) {
    {
        mmcc::InterruptGuard _g;
        _pwm.disable();

        if (fw) {
            _pwm.set_oc_idle_state(stm32::timer::OutputChannel::OC2, false);
            _pwm.set_oc_idle_state(stm32::timer::OutputChannel::OC2N, true);
            _pwm.set_oc_idle_state(stm32::timer::OutputChannel::OC3, false);
            _pwm.set_oc_idle_state(stm32::timer::OutputChannel::OC3N, true);
        }
        else {
            _pwm.set_oc_idle_state(stm32::timer::OutputChannel::OC2, false);
            _pwm.set_oc_idle_state(stm32::timer::OutputChannel::OC2N, false);
            _pwm.set_oc_idle_state(stm32::timer::OutputChannel::OC3, false);
            _pwm.set_oc_idle_state(stm32::timer::OutputChannel::OC3N, false);
        }

        _pwm.generate_event(stm32::timer::Event::CΟΜG);
        _pwm.generate_event(stm32::timer::Event::UG);
        _pwm.enable();
    }
}

void Actuator::set_encoder_limits(uint16_t start, uint16_t end) {
    _encoder_start = start;
    _encoder_end = end;
}

float Actuator::get_encoder_position() {
    if (_encoder_start == _encoder_end) {
        return 0;
    }
    int32_t v = _encoder.get_counter();
    int32_t delta = ((int32_t) _encoder_end) - ((int32_t) _encoder_start);
    float mapped_value = ((float) (v - _encoder_start)) / delta;
    // clamped_value
    return mapped_value;
}

void Actuator::set_pid_tunings(float kp, float ki, float kd) {
    _pid.set_tunings(kp, ki, kd);
}

void Actuator::set_actuation_invert() {
    _action_inverted = !_action_inverted;
}

void Actuator::set_actuation(bool stop, float target_pos, float duration_ms) {
    Event e = {stop, target_pos, duration_ms};
    _queue.send_to_back(e, rtos::MAX_DELAY);
}

void Actuator::on_pid_update() {
    if (actuator_task._pid_ticker.get_interrupt_flags(TIM_SR_UIF)) {
        actuator_task._pid_tick_count++;
        pid::Control& obj = actuator_task._pid;
        if (!actuator_task._action_stopped && actuator_task._action_ticks_remaining != 0) {
            if (actuator_task._action_ticks_remaining <= 1) {
                // Target reached. Hold position.
                actuator_task._action_ticks_remaining = 0;
                actuator_task._action_last_position = actuator_task._action_target_position;
            }
            else {
                // Movement linear to time.
                actuator_task._action_ticks_remaining -= 1;
                actuator_task._action_last_position += actuator_task._action_speed;
            }
            obj.set_setpoint(actuator_task._action_last_position);
        }
        obj.set_input(actuator_task.get_encoder_position());
        obj.compute();
        actuator_task.set_pwm(obj.get_output());
    }
}

}  // namespace mmcc
