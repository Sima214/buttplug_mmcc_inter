#include "Config.hpp"

#include <Objects.hpp>

#include <cstdint>
#include <cstring>

#include <_types/_uint8_t.h>

const void* DEVICE_SIGNATURE = (void*) 0x1FFF7A10U;

namespace mmcc {

ConfigStore::ConfigStore() :
    _i2c_pins(stm32::Port<GPIOB>::get_port().get_pins(8, 9)),
    _i2c(stm32::I2C<I2C1>::get_instance()) {
    _i2c_pins.set_direction(stm32::Pin::Direction::AlternateFunction);
    _i2c_pins.set_function(stm32::Pin::Function::OutputOpenDrain);
    _i2c_pins.set_function(stm32::Pin::Function::AlternateFunction04);
    _i2c.set_speed(stm32::I2C<I2C1>::Mode::i2c_speed_sm_100k, 48);
    cli_task.register_command("i2c1", "I2C bus 1. Connected to EEPROM.", main_i2c);
}

int ConfigStore::main_i2c(int argc, char* argv[]) {
    if (argc == 1) {
        cli_task.print("Must provide an operation.\n");
        cli_task.print("Available operations: speed, scan, raw [write|read].\n");
        return 2;
    }
    else if (!strcmp(argv[1], "speed")) {
        if (argc == 3) {
            if (!strcmp(argv[2], "standard")) {
                config_task._i2c.set_speed(stm32::I2C<I2C1>::Mode::i2c_speed_sm_100k, 48);
                cli_task.print("Speed set to standard.\n");
                return 0;
            }
            else if (!strcmp(argv[2], "fast")) {
                config_task._i2c.set_speed(stm32::I2C<I2C1>::Mode::i2c_speed_fm_400k, 48);
                cli_task.print("Speed set to fast.\n");
                return 0;
            }
            else {
                char buf[96];
                int written = std::snprintf(buf, sizeof(buf), "Invalid speed `%s`.\n", argv[2]);
                cli_task.print(buf, written);
                cli_task.print("Available speeds: standard, fast.\n");
                return 1;
            }
        }
        else {
            cli_task.print("Must provide argument speed.\n");
            cli_task.print("Available speeds: standard, fast.\n");
            return 1;
        }
    }
    else if (!strcmp(argv[1], "scan")) {
        uint8_t start_address = 0x1;
        uint8_t end_address = 0x7f;
        for (uint8_t address = start_address; address <= end_address; address++) {
            bool read = false;
            {
                auto bus = config_task._i2c.start_transaction();
                bus.start();
                read = bus.address(address, false);
                bus.receive(nullptr, 0);
                bus.stop();
            }
            bool write = false;
            {
                auto bus = config_task._i2c.start_transaction();
                bus.start();
                write = bus.address(address, true);
                bus.send(nullptr, 0);
                bus.stop();
            }
            if (read || write) {
                char buf[96];
                int written = std::snprintf(buf, sizeof(buf), "Found device in 0x%x", address);
                cli_task.print(buf, written);
                if (read && write) {
                    cli_task.print(" with rw capabilities!\n");
                }
                else if (read) {
                    cli_task.print(" with read capabilities!\n");
                }
                else if (write) {
                    cli_task.print(" with write capabilities!\n");
                }
            }
        }
        return 0;
    }
    else if (!strcmp(argv[1], "raw")) {
        if (argc >= 4) {
            uint32_t addr_arg = std::strtoul(argv[2], nullptr, 0);
            if (addr_arg < 1 || addr_arg > 127) {
                char buf[96];
                int written =
                     std::snprintf(buf, sizeof(buf), "Address out of range(%lu)!\n", addr_arg);
                cli_task.print(buf, written);
                return 1;
            }
            bool write;
            if (!strcmp(argv[3], "read")) {
                write = false;
            }
            else if (!strcmp(argv[3], "write")) {
                write = true;
            }
            else {
                char buf[96];
                int written =
                     std::snprintf(buf, sizeof(buf), "Invalid bus direction `%s`.\n", argv[3]);
                cli_task.print(buf, written);
                cli_task.print("ADDRESS read|write [DATA]...\n");
                return 1;
            }
            size_t n;
            std::unique_ptr<uint8_t[]> data;
            if (write) {
                n = argc - 4;
                data = std::unique_ptr<uint8_t[]>(new uint8_t[n]);
                if (data.get() == nullptr) {
                    cli_task.print("Could not allocate from heap.\n");
                    return 4;
                }
                for (size_t i = 0; i < n; i++) {
                    uint8_t v = std::strtoul(argv[i + 4], nullptr, 0);
                    data.get()[i] = v;
                }
            }
            else {
                if (argc != 5) {
                    cli_task.print("Not enough arguments.\n");
                    cli_task.print("ADDRESS read N\n");
                    return 1;
                }
                n = std::strtoul(argv[4], nullptr, 0);
                data = std::unique_ptr<uint8_t[]>(new uint8_t[n]);
                if (data.get() == nullptr) {
                    cli_task.print("Could not allocate from heap.\n");
                    return 4;
                }
                std::memset(data.get(), 0, n);
            }
            {
                auto bus = config_task._i2c.start_transaction();
                bus.start();
                bool addr_ok = bus.address(addr_arg, write);
                if (!addr_ok) {
                    char buf[96];
                    int written = std::snprintf(
                         buf, sizeof(buf), "No device responded to address 0x%x!\n", addr_arg);
                    cli_task.print(buf, written);
                    bus.stop();
                    return 1;
                }
                if (write) {
                    size_t sent = bus.send(data.get(), n);
                    bus.stop();
                    bus.end();
                    char buf[96];
                    int written = std::snprintf(
                         buf, sizeof(buf), "Sent %zu bytes to device 0x%x.\n", sent, addr_arg);
                    cli_task.print(buf, written);
                    return 0;
                }
                else {
                    size_t read = bus.receive(data.get(), n);
                    bus.stop();
                    bus.end();
                    char buf[96];
                    int written =
                         std::snprintf(buf, sizeof(buf), "Read %zu bytes from device 0x%x:\n",
                                       read, addr_arg);
                    cli_task.print(buf, written);
                    for (size_t i = 0; i < read; i++) {
                        auto v = data.get()[i];
                        int written = std::snprintf(buf, sizeof(buf), " 0x%x", v);
                        cli_task.print(buf, written);
                    }
                    cli_task.print(".\n");
                    return 0;
                }
            }
        }
        else {
            cli_task.print("Not enough arguments.\n");
            cli_task.print("ADDRESS read|write [DATA]...\n");
            return 2;
        }
    }
    else {
        char buf[96];
        int written = std::snprintf(buf, sizeof(buf), "Unknown subcommand `%s`!\n", argv[1]);
        cli_task.print(buf, written);
        return 2;
    }
}

static_assert(sizeof(ConfigStore::Store::data) == 14,
              "Config store's usable data must not exceed 14 bytes!");

void ConfigStore::eeprom_init() {
    while (!_config_lock.take()) {
        /* NOP */
    }

    char buf[48];
    int written;
    for (int16_t sector = 0; sector < 128; sector++) {
        uint8_t raw[16] = {0xff, 0xff};
        eeprom_read(raw, 16, sector * 16);
        if (raw[1] == (uint8_t) ~raw[0]) {
            Index index = (Index) (raw[0] & 0xf);
            switch (index) {
                case Index::Signature: {
                    if (std::memcmp(&raw[2], DEVICE_SIGNATURE, 12) == 0) {
                        _eeprom_signature_sector = sector;
                        written = std::snprintf(buf, sizeof(buf),
                                                "Found signature at sector 0x%x!\n",
                                                _eeprom_signature_sector);
                        cli_task.print(buf, written);
                    }
                } break;
            }
        }
    }
    if (_eeprom_signature_sector == -1) {
        cli_task.print("Invalid signature in eeprom!\n"
                       "Erasing!\n");
        // Reset eeprom to default values.
        // _eeprom_signature_sector = -1;
        uint8_t raw[16];
        std::memset(raw, 0xff, 16);
        for (int16_t sector = 0; sector < 128; sector++) {
            for (int i = 0; i < 50; i++) {
                if (eeprom_write(raw, 16, sector * 16, false) == 16) {
                    break;
                }
            }
        }
        cli_task.print("EEPROM erased!\n");
        Store sig(Index::Signature, 0x0);
        std::memcpy(sig.data.raw, DEVICE_SIGNATURE, 12);
        set(sig);
    }
    while (!_config_lock.give()) {
        /* NOP */
    }
}
bool ConfigStore::set(const Store& elem) {
    while (!_config_lock.take()) {
        /* NOP */
    }
    uint8_t raw[16];
    raw[0] = elem.index | elem.version << 4;
    raw[1] = ~raw[0];
    std::memcpy(&raw[2], &elem.data, sizeof(elem.data));
    int16_t eeprom_sector = index2sector(elem.index);
    int16_t eeprom_address = eeprom_sector * 16;
    bool ok;
    if (eeprom_address < 0) {
        ok = false;
    }
    else {
        ok = eeprom_write(raw, 16, eeprom_address, true) == 16;
    }
    while (!_config_lock.give()) {
        /* NOP */
    }
    char buf[48];
    int written = std::snprintf(buf, sizeof(buf), "config::set(%d)@0x%x %s\n", elem.index,
                                eeprom_sector, ok ? "OK" : "FAIL");
    cli_task.print(buf, written);
    return ok;
}

std::optional<ConfigStore::Store> ConfigStore::get(Index index, const Store* def) {
    while (!_config_lock.take()) {
        /* NOP */
    }
    auto r = _get(index, def);
    while (!_config_lock.give()) {
        /* NOP */
    }
    return r;
}
std::optional<ConfigStore::Store> ConfigStore::_get(Index index, const Store* def) {
    int16_t eeprom_address = index2sector(index) * 16;
    if (eeprom_address < 0) {
        return {};
    }
    uint8_t raw[16];
    size_t read = eeprom_read(raw, 16, eeprom_address);
    bool failed_read = false;
    bool invalid_version = false;
    if (read != 16) {
        failed_read = true;
    }
    else if (raw[1] != (uint8_t) ~raw[0]) {
        failed_read = true;
    }
    else if ((raw[0] & 0xf) != index) {
        failed_read = true;
    }
    else if (def != nullptr && ((raw[0] >> 4) & 0xf) != def->version) {
        invalid_version = true;
    }
    if (failed_read || invalid_version) {
        // Write valid data.
        if (def != nullptr && set(*def)) {
            return *def;
        }
        else {
            return {};
        }
    }
    else {
        // Read valid data. Build return object.
        Store ret;
        ret.index = (Index) (raw[0] & 0xf);
        ret.version = (raw[0] >> 4) & 0xf;
        std::memcpy(&ret.data, &raw[2], sizeof(ret.data));
        return ret;
    }
}

int16_t ConfigStore::index2sector(Index i) {
    switch (i) {
        case Index::Signature: {
            if (_eeprom_signature_sector < 0) {
                _eeprom_signature_sector = _eeprom_next_sector;
                _eeprom_next_sector = eeprom_next_free_sector(_eeprom_next_sector);
            }
            return _eeprom_signature_sector;
        } break;
        default: {
            return -1;
        }
    }
}
int16_t ConfigStore::eeprom_next_free_sector(int16_t start_sector) {
    int16_t next_sector = (start_sector + 1) % 128;
    if (_eeprom_signature_sector != -1 && next_sector == _eeprom_signature_sector) {
        return eeprom_next_free_sector(next_sector);
    }
    else {
        return next_sector;
    }
}
size_t ConfigStore::eeprom_read(uint8_t* data, size_t n, uint16_t address) {
    uint8_t addr = 0x50 | ((address >> 8) & 0b111);
    auto bus = config_task._i2c.start_transaction();
    bus.start();
    if (bus.address(addr, true)) {
        uint8_t address_l8 = address & 0xff;
        if (bus.send(&address_l8, 1) != 1) {
            // Could not send address byte.
            bus.stop();
            return 0;
        }
        else {
            // Repeated start condition.
            bus.start();
            if (bus.address(addr, false)) {
                size_t read = bus.receive(data, n);
                bus.stop();
                return read;
            }
            else {
                // Read address not ACKed.
                bus.stop();
                return 0;
            }
        }
    }
    else {
        // Write address not ACKed.
        bus.stop();
        return 0;
    }
}
size_t ConfigStore::eeprom_write(const uint8_t* data, size_t n, uint16_t address, bool verify) {
    uint8_t* cmd = new uint8_t[n + 1];
    uint8_t* ver;
    if (cmd == nullptr) {
        // Could not allocate command buffer.
        return 0;
    }
    if (verify) {
        ver = new uint8_t[n];
        if (ver == nullptr) {
            // Could not allocate verify read back buffer.
            return 0;
        }
    }

    uint8_t address_l8 = address & 0xff;
    cmd[0] = address_l8;
    std::memcpy(&cmd[1], data, n);
    uint8_t addr = 0x50 | ((address >> 8) & 0b111);

    auto bus = config_task._i2c.start_transaction();
    bus.start();
    if (bus.address(addr, true)) {
        size_t sent = bus.send(cmd, n + 1);
        bus.stop();
        delete[] cmd;
        if (verify) {
            // Reuse bus for reading in order to verify write.
            bool addr_ok = false;
            while (!addr_ok) {
                bus.start();
                if (bus.address(addr, true)) {
                    addr_ok = true;
                }
                else {
                    bus.stop();
                }
            }
            if (bus.send(&address_l8, 1) != 1) {
                // Could not send address byte.
                bus.stop();
                return 0;
            }
            else {
                // Repeated start condition.
                bus.start();
                if (bus.address(addr, false)) {
                    size_t read = bus.receive(ver, n);
                    bool verified = false;
                    if (read == n) {
                        verified = std::memcmp(data, ver, n) == 0;
                    }
                    bus.stop();
                    delete[] ver;
                    return verified ? read : 0;
                }
                else {
                    // Read address not ACKed.
                    bus.stop();
                    return 0;
                }
            }
        }
        else {
            return sent - 1;
        }
    }
    else {
        bus.stop();
        // Invalid address.
        return 0;
    }
}

}  // namespace mmcc
