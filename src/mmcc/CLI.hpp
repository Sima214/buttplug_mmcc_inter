#ifndef MMCC_CLI_HPP
#define MMCC_CLI_HPP

#include <interface/RTOS.hpp>
#include <interface/RTOSBuffers.hpp>
#include <interface/RTOSMutex.hpp>
#include <interface/RTOSTask.hpp>
#include <interface/io/USART.hpp>

#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <limits>
#include <tuple>

#include <etl/vector.h>

namespace mmcc {

inline char* _split_line(char* line, char** args) {
    char* end = std::strchr(line, ' ');

    if (end != nullptr) {
        *end = '\0';
        end++;
        while ((*end != '\0') && (*end == ' ')) {
            end++;
        }
        if (*end == '\0') {
            end = NULL;
        }
    }

    *args = end;
    return end;
}

/**
 * Breaks up a line into multiple arguments.
 *
 * @param line Line to be broken up.
 * @return Number of components found.
 * @param argv Array of individual components
 */
inline int _split_shell(char* line, size_t len, char* argv[], size_t argc_max) {
    {
        // Protect spaces inside quotes and nullify quotes.
        bool in_quotes = false;
        for (size_t i = 0; i < len; i++) {
            if ((!in_quotes) && (line[i] == '"')) {
                in_quotes = true;
                line[i] = ' ';
            }
            else if ((in_quotes) && (line[i] == '"')) {
                in_quotes = false;
                line[i] = ' ';
            }
            else if ((in_quotes) && (line[i] == ' ')) {
                line[i] = '\1';
            }
        }
    }

    // Init output variables.
    size_t argc = 1;
    argv[0] = line;

    char* next = line;
    while ((next != nullptr) && (argc < argc_max)) {
        next = _split_line(next, argv + argc);
        if (next != nullptr) {
            argc += 1;
        }
    }

    // Reverse protection of spaces inside quotes.
    for (size_t i = 0; i < len; i++) {
        if (line[i] == '\1') {
            line[i] = ' ';
        }
    }

    return argc;
}

template<size_t C = 8> class SerialInterface : rtos::StaticTask<384> {
   public:
    using command_cb = int(int, char**);

   protected:
    stm32::USART& _port;
    etl::vector<std::tuple<const char*, const char*, command_cb*>, C> _registry;
    rtos::StaticStreamBuffer<128> _send_buffer;
    char _receive_buffer[128];
    size_t _receive_len;
    bool _connected;

   public:
    SerialInterface(stm32::USART& port) :
        rtos::StaticTask<384>("cli", 1), _port(port), _registry(), _send_buffer(1),
        _receive_len(0), _connected(false) {
        std::memset(_receive_buffer, 0, sizeof(_receive_buffer));
    }
    ~SerialInterface() = default;

    void run() override {
        on_user_connect();
        while (true) {
            if (_port.rx_ready()) {
                char ch = _port.rx(false);
                if (ch == '\n' || ch == '\r' || ch == '\0' ||
                    _receive_len == sizeof(_receive_buffer) - 1) {
                    _port.tx('\n');
                    // Split command.
                    _receive_buffer[_receive_len] = '\0';
                    char* argv[24];
                    int argc = _split_shell(_receive_buffer, _receive_len, argv,
                                            sizeof(argv) / sizeof(char*));
                    // Execute command.
                    int status = execute(argc, argv);
                    // Cleanup and prepare for next.
                    print_header(status);
                    _receive_len = 0;
                }
                else if (ch == '\b' || ch == 127) {
                    if (_receive_len > 0) {
                        _receive_len--;
                        _port.tx(ch);
                    }
                }
                else {
                    _port.tx(ch);
                    // Append input.
                    _receive_buffer[_receive_len] = ch;
                    _receive_len++;
                }
            }
            // Periodically send messages from interrupts.
            while (!_send_buffer.is_empty()) {
                char buf[32];
                size_t len = _send_buffer.receive(buf, sizeof(buf));
                on_data_send(buf, len);
            }
            delay(100);
        }
    }

    void register_command(const char* command, const char* description, command_cb* cb) {
        std::tuple<const char*, const char*, command_cb*> r(command, description, cb);
        if (!_registry.full()) {
            _registry.push_back(r);
        }
    }

    int execute(int argc, char* argv[]) {
        if (argc >= 1) {
            // Find command.
            if (!std::strcmp(argv[0], "help")) {
                print_help();
                return 0;
            }
            else if (!std::strcmp(argv[0], "echo")) {
                return execute_echo(argc, argv);
            }
            else if (!std::strcmp(argv[0], "watch")) {
                return execute_watch(argc, argv);
            }
            else {
                for (auto& it : _registry) {
                    const char* name = std::get<0>(it);
                    command_cb* cb = std::get<2>(it);
                    if (!std::strcmp(argv[0], name)) {
                        return cb(argc, argv);
                    }
                }
            }
            // Invalid command.
            char buf[96];
            int written = std::snprintf(buf, sizeof(buf), "Unknown command `%s`!\n", argv[0]);
            print(buf, written);
            return 80;
        }
        else {
            // Empty command.
            return 127;
        }
    }

    void print_help() {
        print("List of available commands:\n\n");
        print("help\tShow this help message.\n");
        print("echo\tDisplay a line of text.\n");
        print("watch\tExecute a command multiple times in an interval.\n");
        print("-----\tFirst argument is the interval in milliseconds, second is repetition "
              "count.\n");
        for (auto& it : _registry) {
            const char* name = std::get<0>(it);
            const char* desc = std::get<1>(it);
            char buf[96];
            if (desc != nullptr) {
                int written = std::snprintf(buf, sizeof(buf), "%s\t%s\n", name, desc);
                print(buf, written);
            }
            else {
                int written = std::snprintf(buf, sizeof(buf), "%s\n", name);
                print(buf, written);
            }
        }
    }

    int execute_echo(int argc, char* argv[]) {
        for (int i = 1; i < argc; i++) {
            print(argv[i]);
            print((i + 1) == argc ? "\n" : " ", 1);
        }
        return 0;
    }

    int execute_watch(int argc, char* argv[]) {
        if (argc >= 3) {
            float delay_ms = std::strtof(argv[1], nullptr);
            uint32_t count = std::strtoul(argv[2], nullptr, 0);
            for (uint32_t i = 0; i < count; i++) {
                int status = execute(argc - 3, argv + 3);
                if (status != 0) {
                    return status;
                }
                delay(delay_ms);
            }
            return 0;
        }
        else {
            print("Not enough arguments!");
            return 2;
        }
    }

    void print_header(int status = 0) {
        if (status) {
            char buf[16];
            int written = std::snprintf(buf, sizeof(buf), "\n%d>>> ", status);
            print(buf, written);
        }
        else {
            print("\n>>> ");
        }
    }
    void print_welcome() {
        print("Welcome to Buttplug MMCC firmware!\n");
    }

    void print(const char* msg, size_t len) {
        if (_connected) {
            if (rtos::is_inside_interrupt()) {
                _send_buffer.send_from_isr(msg, len);
            }
            else {
                on_data_send(msg, len);
            }
        }
    }

    void print(const char* msg) {
        print(msg, std::strlen(msg));
    }

    void on_user_connect() {
        _connected = true;
        print_welcome();
        print_header();
    }
    void on_data_send(const char* dat, size_t len) {
        for (size_t i = 0; i < len; i++) {
            _port.tx(dat[i]);
        }
    }
};

}  // namespace mmcc

#endif /*MMCC_CLI_HPP*/
