#include "OS.hpp"

mmcc::Kernel mmcc::Kernel::instance;
rtos::Kernel& rtos::Kernel::instance_ref = mmcc::Kernel::instance;

namespace mmcc {

Kernel::Kernel() : _indicator_led(stm32::Port<GPIOC>::get_port().get_pins(13)) {
    // Prepare running indicator.
    _indicator_led.set_direction(stm32::Pin::Direction::Output);
    _indicator_led.set_speed(stm32::Pin::Speed::Medium);
    _indicator_led.set();
}

void Kernel::on_start() {
    _indicator_led.clear();
}

void Kernel::on_end() {
    _indicator_led.set();
}

void Kernel::on_pre_sleep_processing(TickType_t& expected_idle_time) {
    rtos::Kernel::on_pre_sleep_processing(expected_idle_time);
    _indicator_led.set();
}
void Kernel::on_post_sleep_processing(TickType_t& expected_idle_time) {
    rtos::Kernel::on_post_sleep_processing(expected_idle_time);
    _indicator_led.clear();
}

void Kernel::get_idle_task_memory(StaticTask_t** idle_task_tcb_buffer,
                                  StackType_t** idle_task_stack_buffer,
                                  uint32_t* idle_task_stack_size) {
    *idle_task_tcb_buffer = &_idle_task_tcb;
    *idle_task_stack_buffer = _idle_task_stack;
    *idle_task_stack_size = sizeof(_idle_task_stack) / sizeof(decltype(_idle_task_stack[0]));
}

}  // namespace mmcc