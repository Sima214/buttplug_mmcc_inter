#ifndef MMCC_ALLOCABLESTACK_HPP
#define MMCC_ALLOCABLESTACK_HPP

#include <Class.hpp>

#include <cstddef>
#include <cstdint>

namespace mmcc {

/**
 * Implements an upward growing stack.
 * Does not support freeing individual allocations.
 */
class AllocableStack : public mmcc::INonCopyable {
   protected:
    uint8_t* _buffer;
    size_t _len;
    uint8_t* _end;

   public:
    AllocableStack(void* buffer, size_t length);

    /**
     * Make a new allocation.
     * Returns nullptr if there is not enough space.
     * Optionally accepts an alignment argument.
     */
    __attribute__((malloc)) void* allocate(size_t size, size_t alignment = 1);
    /**
     * Reset allocations.
     */
    void reset();
    /**
     * Clear internal storage.
     */
    void clear();
};

template<size_t N> class StaticAllocableStack : public AllocableStack {
   protected:
    uint8_t _data[N];

   public:
    StaticAllocableStack() : AllocableStack(_data, N) {}
};

}  // namespace mmcc

#endif /*MMCC_ALLOCABLESTACK_HPP*/