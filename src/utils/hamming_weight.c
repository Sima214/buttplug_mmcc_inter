#include "hamming_weight.h"

int hamming_weight_u8(uint8_t a) {
    return __builtin_popcount(a);
}

int hamming_weight_u16(uint16_t a) {
    return __builtin_popcount(a);
}

int hamming_weight_u32(uint32_t a) {
    return __builtin_popcountl(a);
}

int hamming_weight_u64(uint64_t a) {
    return __builtin_popcountll(a);
}
