#ifndef MMCC_CLASS_HPP
#define MMCC_CLASS_HPP

namespace mmcc {

/**
 * @brief Inheritable class with disabled copy constructors.
 */
class INonCopyable {
   public:
    INonCopyable& operator=(const INonCopyable&) = delete;
    INonCopyable(const INonCopyable&) = delete;

    INonCopyable& operator=(INonCopyable&&) = default;
    INonCopyable(INonCopyable&&) = default;

    INonCopyable() = default;
    ~INonCopyable() = default;
};

}  // namespace mmcc

#endif /*MMCC_CLASS_HPP*/