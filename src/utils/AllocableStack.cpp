#include "AllocableStack.hpp"

#include <cstring>

namespace mmcc {

AllocableStack::AllocableStack(void* buffer, size_t length) :
    _buffer((uint8_t*) buffer), _len(length), _end(_buffer) {}

void* AllocableStack::allocate(size_t size, size_t alignment) {
    uint8_t* start = _end;
    uintptr_t offset = ((uintptr_t) start) % alignment;
    if (offset) {
        start += alignment - offset;
    }
    uint8_t* new_end = start + size;
    if (new_end <= (_buffer + _len)) {
        _end = new_end;
        return start;
    }
    else {
        return nullptr;
    }
}

void AllocableStack::reset() {
    _end = _buffer;
}

void AllocableStack::clear() {
    std::memset(_buffer, 0, _len);
}

}  // namespace mmcc
