//*********************************************************************************
// Arduino PID Library Version 1.0.1 Modified Version for C++
// Platform Independent
//
// Revision: 1.1
//
// Description: The PID Controller module originally meant for Arduino made
// platform independent. Some small bugs present in the original Arduino source
// have been rectified as well.
//
// For a detailed explanation of the theory behind this library, go to:
// http://brettbeauregard.com/blog/2011/04/improving-the-beginners-pid-introduction/
//
// Revisions can be found here:
// https://github.com/tcleg
//
// Modified by: Trent Cleghorn , <trentoncleghorn@gmail.com>
//
// Copyright (C) Brett Beauregard , <br3ttb@gmail.com>
//
//                                 GPLv3 License
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <http://www.gnu.org/licenses/>.
//*********************************************************************************

//*********************************************************************************
// Headers
//*********************************************************************************
#include "PidController.hpp"

//*********************************************************************************
// Macros and Globals
//*********************************************************************************
#define CONSTRAIN(x, lower, upper) ((x) < (lower) ? (lower) : ((x) > (upper) ? (upper) : (x)))

//*********************************************************************************
// Public Class Functions
//*********************************************************************************

namespace pid {

Control::Control(float kp, float ki, float kd, float sampleTimeSeconds, float minOutput,
                 float maxOutput, Mode mode, Direction controllerDirection) {
    _controller_direction = controllerDirection;
    _mode = mode;
    _i_term = 0.0f;
    _input = 0.0f;
    _last_input = 0.0f;
    _output = 0.0f;
    _setpoint = 0.0f;

    if (sampleTimeSeconds > 0.0f) {
        _sample_time = sampleTimeSeconds;
    }
    else {
        // If the passed parameter was incorrect, set to 1 second
        _sample_time = 1.0f;
    }

    set_output_limits(minOutput, maxOutput);
    set_tunings(kp, ki, kd);
}

bool Control::compute() {
    float error, dInput;

    if (_mode == Mode::Manual) {
        return false;
    }

    // The classic PID error term
    error = _setpoint - _input;

    // Compute the integral term separately ahead of time
    _i_term += _altered_ki * error;

    // Constrain the integrator to make sure it does not exceed output bounds
    _i_term = CONSTRAIN(_i_term, _out_min, _out_max);

    // Take the "derivative on measurement" instead of "derivative on error"
    dInput = _input - _last_input;

    // Run all the terms together to get the overall output
    _output = _altered_kp * error + _i_term - _altered_kd * dInput;

    // Bound the output
    _output = CONSTRAIN(_output, _out_min, _out_max);

    // Make the current input the former input
    _last_input = _input;

    return true;
}

void Control::set_mode(Mode mode) {
    // If the mode changed from MANUAL to AUTOMATIC
    if (_mode != mode && mode == Mode::Automatic) {
        // Initialize a few PID parameters to new values
        _i_term = _output;
        _last_input = _input;

        // Constrain the integrator to make sure it does not exceed output bounds
        _i_term = CONSTRAIN(_i_term, _out_min, _out_max);
    }

    _mode = mode;
}

void Control::set_output_limits(float min, float max) {
    // Check if the parameters are valid
    if (min >= max) {
        return;
    }

    // Save the parameters
    _out_min = min;
    _out_max = max;

    // If in automatic, apply the new constraints
    if (_mode == Mode::Automatic) {
        _output = CONSTRAIN(_output, min, max);
        _i_term = CONSTRAIN(_i_term, min, max);
    }
}

void Control::set_tunings(float kp, float ki, float kd) {
    // Check if the parameters are valid
    if (kp < 0.0f || ki < 0.0f || kd < 0.0f) {
        return;
    }

    // Save the parameters for displaying purposes
    _disp_kp = kp;
    _disp_ki = ki;
    _disp_kd = kd;

    // Alter the parameters for PID
    _altered_kp = kp;
    _altered_ki = ki * _sample_time;
    _altered_kd = kd / _sample_time;

    // Apply reverse direction to the altered values if necessary
    if (_controller_direction == Direction::Reverse) {
        _altered_kp = -(_altered_kp);
        _altered_ki = -(_altered_ki);
        _altered_kd = -(_altered_kd);
    }
}

void Control::set_tuning_kp(float kp) {
    set_tunings(kp, _disp_ki, _disp_kd);
}

void Control::set_tuning_ki(float ki) {
    set_tunings(_disp_kp, ki, _disp_kd);
}

void Control::set_tuning_kd(float kd) {
    set_tunings(_disp_kp, _disp_ki, kd);
}

void Control::set_controller_direction(Direction controllerDirection) {
    // If in automatic mode and the controller's sense of direction is reversed
    if (_mode == Mode::Automatic && controllerDirection == Direction::Reverse) {
        // Reverse sense of direction of PID gain constants
        _altered_kp = -(_altered_kp);
        _altered_ki = -(_altered_ki);
        _altered_kd = -(_altered_kd);
    }

    _controller_direction = controllerDirection;
}

void Control::set_sample_time(float sampleTimeSeconds) {
    float ratio;

    if (sampleTimeSeconds > 0.0f) {
        // Find the ratio of change and apply to the altered values
        ratio = sampleTimeSeconds / _sample_time;
        _altered_ki *= ratio;
        _altered_kd /= ratio;

        // Save the new sampling time
        _sample_time = sampleTimeSeconds;
    }
}

}  // namespace pid
