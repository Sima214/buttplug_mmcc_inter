#ifndef MMCC_HAMMINGWEIGHT_HPP
#define MMCC_HAMMINGWEIGHT_HPP

extern "C" {
#include <hamming_weight.h>
}

#include <cstdint>

namespace mmcc {

template<typename T> inline int hamming_weight(T a);

template<> inline int hamming_weight(uint8_t a) {
    return hamming_weight_u8(a);
}
template<> inline int hamming_weight(uint16_t a) {
    return hamming_weight_u16(a);
}
template<> inline int hamming_weight(uint32_t a) {
    return hamming_weight_u32(a);
}
template<> inline int hamming_weight(uint64_t a) {
    return hamming_weight_u64(a);
}

}  // namespace mmcc

#endif /*MMCC_HAMMINGWEIGHT_HPP*/
