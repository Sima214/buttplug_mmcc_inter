#ifndef MMCC_OBJECTS_HPP
#define MMCC_OBJECTS_HPP

#include <interface/buttplug/COM.hpp>
#include <interface/io/GPIO.hpp>
#include <mmcc/Actuator.hpp>
#include <mmcc/CLI.hpp>
#include <mmcc/Config.hpp>

namespace mmcc {

extern buttplug::USBTask usb_task;

class SerialInterface1 : public SerialInterface<> {
   protected:
    stm32::Pin _pins;
    stm32::USART _usart;

   public:
    SerialInterface1();

    virtual void run() override;
};

extern SerialInterface1 cli_task;

extern Actuator actuator_task;

extern ConfigStore config_task;

}  // namespace mmcc

#endif /*MMCC_OBJECTS_HPP*/