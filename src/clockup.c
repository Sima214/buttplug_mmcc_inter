#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/syscfg.h>

#define SYSCFG_CMPCR_CMP_PD (1 << 0)

__attribute__((constructor)) void clockup() {
    // Setup clocks.
    const struct rcc_clock_scale rcc_hse_25mhz_pll_96mhz_3v3 = {
         25,
         192,
         2,
         4,
         0,
         RCC_CFGR_PLLSRC_HSE_CLK,
         FLASH_ACR_DCEN | FLASH_ACR_ICEN | FLASH_ACR_LATENCY_3WS,
         RCC_CFGR_HPRE_NODIV,
         RCC_CFGR_PPRE_DIV2,
         RCC_CFGR_PPRE_NODIV,
         PWR_SCALE1,
         96000000,
         48000000,
         96000000,
    };
    rcc_clock_setup_pll(&rcc_hse_25mhz_pll_96mhz_3v3);
    rcc_periph_clock_enable(RCC_SYSCFG);
    flash_prefetch_enable();
    SYSCFG_CMPCR |= SYSCFG_CMPCR_CMP_PD;
}
