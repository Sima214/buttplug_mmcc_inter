#include "boot.h"

#include <config.h>
#include <core.h>
#include <stdlib.h>
#include <string.h>

small_vector_table_t static_vector_table = {.initial_sp_value = &_stack,
                                            .reset = mmcc_reset_handler,
                                            .nmi = null_handler,
                                            .hard_fault = null_handler,
                                            .memory_manage_fault = null_handler,
                                            .bus_fault = null_handler,
                                            .usage_fault = null_handler,
                                            .sv_call = null_handler,
                                            .debug_monitor = null_handler,
                                            .pend_sv = null_handler,
                                            .systick = null_handler};

vector_table_t runtime_vector_table;

int main();

void mmcc_reset_handler() {
    /**
     * Ensure 8-byte alignment of stack pointer on interrupts.
     * Enabled by default on most Cortex-M parts, but not M3 r1
     */
    SCB_CCR |= SCB_CCR_STKALIGN;
    // CRT Startup.
    memset(&__bss_start, 0, __bss_size);
    memcpy(&__data_start, &__data_loadaddr, __data_size);
    // Vector table initialization.
    memcpy(&runtime_vector_table, &static_vector_table, sizeof(static_vector_table));
    for (int i = 0; i < NVIC_IRQ_COUNT; i++) {
        runtime_vector_table.irq[i] = null_handler;
    }
    SCB_VTOR = (uintptr_t) &runtime_vector_table;
    // Constructors.
    funcp_t* fp;
    for (fp = &__preinit_array_start; fp < &__preinit_array_end; fp++) {
        (*fp)();
    }
    for (fp = &__init_array_start; fp < &__init_array_end; fp++) {
        (*fp)();
    }
    // Ensure proper Cortex-M core state.
    mmcc_set_priority_grouping(SCB_AIRCR_PRIGROUP_GROUP16_NOSUB);
    mmcc_enable_interrupts();
    mmcc_enable_faults();
#if CPU_HAS_FPU
    // Enable access to Floating-Point coprocessor.
    SCB_CPACR |= SCB_CPACR_FULL * (SCB_CPACR_CP10 | SCB_CPACR_CP11);
#endif
    // Call main.
    main();
    // Destructors.
    for (fp = &__fini_array_start; fp < &__fini_array_end; fp++) {
        (*fp)();
    }
    // Reset cpu/core.
}

void null_handler() {
    nop;
}
