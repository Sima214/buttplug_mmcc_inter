#include "Config.hpp"
#include "GPIO.hpp"
#include "Objects.hpp"

mmcc::SerialInterface1::SerialInterface1() :
    SerialInterface<>(_usart), _pins(stm32::Port<GPIOB>::get_port().get_pins(6, 7)),
    _usart(USART1) {
    _pins.set_direction(stm32::Pin::Direction::AlternateFunction);
    _pins.set_function(stm32::Pin::Function::AlternateFunction07);
    _usart.set_mode(true, true);
    _usart.set_baudrate(115200);
    _usart.set_databits(9);
    _usart.set_stopbits(stm32::USART::StopBits::_2);
    _usart.set_parity(stm32::USART::Parity::Even);
    _usart.set_flow_control(false, false);
    _usart.enable();
}

void mmcc::SerialInterface1::run() {
    // Enable early prints.
    _connected = true;
    config_task.eeprom_init();
    actuator_task.on_config_complete();
    usb_task.on_config_complete();
    SerialInterface<>::run();
}

extern "C" {

__attribute__((noreturn)) void __stack_chk_fail() {
    mmcc::cli_task.print("Stack corruption!");
    abort();
}

__attribute__((noreturn, weak)) void __assert_fail(const char* expr, const char* file,
                                                   unsigned int line, const char* function) {
    (void) expr, (void) file, (void) line, (void) function;
    char buf[128];
    int written =
         std::snprintf(buf, sizeof(buf), "%s at %s %s:%u!\n", expr, function, file, line);
    mmcc::cli_task.print(buf, written);
    abort();
}

// Trap function.
__attribute__((noreturn)) void _Exit(int) {
    mmcc::cli_task.print("Aborting!");
    mmcc_reset_system();
}
}

mmcc::SerialInterface1 mmcc::cli_task;
buttplug::USBTask mmcc::usb_task;
mmcc::Actuator mmcc::actuator_task;
mmcc::ConfigStore mmcc::config_task;
