#ifndef MMCC_RTOSBUFFERS_HPP
#define MMCC_RTOSBUFFERS_HPP

#include <Class.hpp>
#include <RTOS.hpp>

#include <cstddef>
#include <utility>

#include <FreeRTOS.h>
#include <message_buffer.h>
#include <stream_buffer.h>

namespace rtos {

class StreamBuffer : public mmcc::INonCopyable {
   protected:
    StreamBufferHandle_t _handle = nullptr;

   protected:
    StreamBuffer() = default;

   public:
    StreamBuffer(size_t size, size_t trigger_level);
    ~StreamBuffer();
    StreamBuffer(StreamBuffer&& o) : _handle(std::exchange(o._handle, nullptr)) {}
    StreamBuffer& operator=(StreamBuffer&& o) {
        _handle = std::exchange(o._handle, nullptr);
        return *this;
    }

    size_t send(const void* data, size_t len, float ms_to_wait = 0);
    size_t send_from_isr(const void* data, size_t len);
    size_t receive(void* data, size_t len, float ms_to_wait = 0);
    size_t receive_from_isr(void* data, size_t len);

    size_t bytes_available();
    size_t spaces_available();
    bool set_trigger_level(size_t trigger_level);
    bool reset();
    bool is_empty();
    bool is_full();
};

template<size_t N> class StaticStreamBuffer : public StreamBuffer {
   protected:
    StaticStreamBuffer_t _handle_storage;
    uint8_t _data[N];

   public:
    StaticStreamBuffer(size_t trigger_level) : StreamBuffer() {
        _handle = xStreamBufferCreateStatic(N, trigger_level, _data, &_handle_storage);
    }
};

class MessageBuffer : public mmcc::INonCopyable {
   protected:
    MessageBufferHandle_t _handle = nullptr;

   protected:
    MessageBuffer() = default;

   public:
    MessageBuffer(size_t size);
    ~MessageBuffer();
    MessageBuffer(MessageBuffer&& o) : _handle(std::exchange(o._handle, nullptr)) {}
    MessageBuffer& operator=(MessageBuffer&& o) {
        _handle = std::exchange(o._handle, nullptr);
        return *this;
    }

    size_t send(const void* data, size_t len, float ms_to_wait = 0);
    size_t send_from_isr(const void* data, size_t len);
    size_t receive(void* data, size_t len, float ms_to_wait = 0);
    size_t receive_from_isr(void* data, size_t len);
    size_t spaces_available();
    bool reset();
    bool is_empty();
    bool is_full();
};

template<size_t N> class StaticMessageBuffer : public MessageBuffer {
   protected:
   public:
};

}  // namespace rtos

#endif /*MMCC_RTOSBUFFERS_HPP*/
