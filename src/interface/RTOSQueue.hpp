#ifndef MMCC_RTOSQUEUE_HPP
#define MMCC_RTOSQUEUE_HPP

#include <Class.hpp>
#include <RTOS.hpp>

#include <cstdint>
#include <optional>
#include <utility>

#include <FreeRTOS.h>
#include <queue.h>

namespace rtos {

template<typename T> class Queue : public mmcc::INonCopyable {
   protected:
    QueueHandle_t _handle = nullptr;

   protected:
    Queue() = default;

   public:
    Queue(uint32_t length) {
        _handle = xQueueCreate(length, sizeof(T));
    }

    Queue(uint32_t length, const char* name) : Queue(length) {
        add_to_registry(name);
    }

    ~Queue() {
        if (_handle != nullptr) {
            vQueueDelete(_handle);
            unregister_queue();
            _handle = nullptr;
        }
    }
    Queue(Queue&& o) : _handle(std::exchange(o._handle, nullptr)) {}
    Queue& operator=(Queue&& o) {
        _handle = std::exchange(o._handle, nullptr);
        return *this;
    }

    bool send_to_back(T& v, float ms = 0) {
        TickType_t ticks = ms2ticks(ms);
        return xQueueSendToBack(_handle, &v, ticks) == pdTRUE;
    }
    bool send_to_back_from_isr(T& v) {
        BaseType_t task_switch = 0;
        auto r = xQueueSendToBackFromISR(_handle, &v, &task_switch);
        if (task_switch) {
            taskYIELD();
        }
        return r == pdTRUE;
    }
    bool send_to_front(T& v, float ms = 0) {
        TickType_t ticks = ms2ticks(ms);
        return xQueueSendToFront(_handle, &v, ticks) == pdTRUE;
    }
    bool send_to_front_from_isr(T& v) {
        BaseType_t task_switch = 0;
        auto r = xQueueSendToFrontFromISR(_handle, &v, &task_switch);
        if (task_switch) {
            taskYIELD();
        }
        return r == pdTRUE;
    }

    std::optional<T> receive(float ms = rtos::MAX_DELAY) {
        TickType_t ticks = ms2ticks(ms);
        T v;
        bool ok = xQueueReceive(_handle, &v, ticks) == pdTRUE;
        if (ok) {
            return v;
        }
        else {
            return {};
        }
    }
    std::optional<T> receive_from_isr() {
        BaseType_t task_switch = 0;
        T v;
        bool ok = xQueueReceiveFromISR(_handle, &v, &task_switch) == pdTRUE;
        if (task_switch) {
            taskYIELD();
        }
        if (ok) {
            return v;
        }
        else {
            return {};
        }
    }

    uint32_t messages_waiting() {
        return uxQueueMessagesWaiting(_handle);
    }
    uint32_t messages_waiting_from_isr() {
        return uxQueueMessagesWaitingFromISR(_handle);
    }

    uint32_t spaces_available() {
        return uxQueueSpacesAvailable(_handle);
    }
    void reset() {
        xQueueReset(_handle);
    }

    void overwrite(T& v) {
        xQueueOverwrite(_handle, &v);
    }
    void overwrite_from_isr(T& v) {
        BaseType_t task_switch = 0;
        xQueueOverwriteFromISR(_handle, &v, &task_switch);
        if (task_switch) {
            taskYIELD();
        }
    }

    std::optional<T> peek(float ms = 0) {
        TickType_t ticks = ms2ticks(ms);
        T v;
        bool ok = xQueuePeek(_handle, &v, ticks) == pdTRUE;
        if (ok) {
            return v;
        }
        else {
            return {};
        }
    }
    std::optional<T> peek_from_isr() {
        T v;
        bool ok = xQueuePeekFromISR(_handle, &v) == pdTRUE;
        if (ok) {
            return v;
        }
        else {
            return {};
        }
    }

    void add_to_registry(const char* name) {
        (void) name;
        vQueueAddToRegistry(_handle, name);
    }
    void unregister_queue() {
        vQueueUnregisterQueue(_handle);
    }
    const char* get_name() {
        return pcQueueGetName(_handle);
    }

    bool is_queue_full_from_isr() {
        return xQueueIsQueueFullFromISR(_handle) != pdFALSE;
    }
    bool is_queue_empty_from_isr() {
        return xQueueIsQueueEmptyFromISR(_handle) != pdFALSE;
    }
};

template<typename T, size_t N> class StaticQueue : public Queue<T> {
   protected:
    StaticQueue_t _handle_storage;
    T _element_storage[N];

   public:
    StaticQueue() {
        Queue<T>::_handle =
             xQueueCreateStatic(N, sizeof(T), (uint8_t*) _element_storage, &_handle_storage);
    }

    StaticQueue(const char* name) : StaticQueue() {
        Queue<T>::add_to_registry(name);
    }
};

}  // namespace rtos

#endif /*MMCC_RTOSQUEUE_HPP*/
