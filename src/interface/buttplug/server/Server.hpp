#ifndef MMCC_SERVER_HPP
#define MMCC_SERVER_HPP

#include <AllocableStack.hpp>
#include <Class.hpp>
#include <Messages.hpp>

#include <cstddef>
#include <cstdint>

namespace buttplug {

class Server : public mmcc::INonCopyable {
   protected:
    size_t _read_buffer_len;
    uint8_t _read_buffer[256];
    uint8_t _write_buffer[768];
    buttplug_Server _write_obj;
    bool _connected = false;

    mmcc::StaticAllocableStack<1024> _message_heap;

   public:
    Server();
    virtual ~Server();

    virtual void on_ping(const buttplug::message::status::Ping&) = 0;
    virtual void on_request_server_info(
         const buttplug::message::handshake::RequestServerInfo&) = 0;
    virtual void on_start_scanning(const buttplug::message::enumeration::StartScanning&) = 0;
    virtual void on_stop_scanning(const buttplug::message::enumeration::StopScanning&) = 0;
    virtual void on_request_device_list(
         const buttplug::message::enumeration::RequestDeviceList&) = 0;
    virtual void on_raw_write_cmd(const buttplug::message::device::raw::RawWriteCmd&) = 0;
    virtual void on_raw_read_cmd(const buttplug::message::device::raw::RawReadCmd&) = 0;
    virtual void on_raw_subscribe_cmd(
         const buttplug::message::device::raw::RawSubscribeCmd&) = 0;
    virtual void on_raw_unsubscribe_cmd(
         const buttplug::message::device::raw::RawUnsubscribeCmd&) = 0;
    virtual void on_stop_device_cmd(
         const buttplug::message::device::generic::StopDeviceCmd&) = 0;
    virtual void on_stop_all_devices(
         const buttplug::message::device::generic::StopAllDevices&) = 0;
    virtual void on_vibrate_cmd(const buttplug::message::device::generic::VibrateCmd&) = 0;
    virtual void on_linear_cmd(const buttplug::message::device::generic::LinearCmd&) = 0;
    virtual void on_rotate_cmd(const buttplug::message::device::generic::RotateCmd&) = 0;
    virtual void on_battery_level_cmd(
         const buttplug::message::device::sensor::BatteryLevelCmd&) = 0;
    virtual void on_rssi_level_cmd(const buttplug::message::device::sensor::RSSILevelCmd&) = 0;

    void send_ok(const buttplug::message::status::Ok&);
    void send_error(const buttplug::message::status::Error&);
    void send_server_info(const buttplug::message::handshake::ServerInfo&);
    void send_scanning_finished(const buttplug::message::enumeration::ScanningFinished&);
    void send_device_list(const buttplug::message::enumeration::DeviceList&);
    void send_device_added(const buttplug::message::enumeration::DeviceAdded&);
    void send_device_removed(const buttplug::message::enumeration::DeviceRemoved&);
    void send_raw_reading(const buttplug::message::device::raw::RawReading&);
    void send_battery_level_reading(
         const buttplug::message::device::sensor::BatteryLevelReading&);
    void send_rssi_level_reading(const buttplug::message::device::sensor::RSSILevelReading&);

    virtual void on_hw_connect();
    virtual void on_hw_disconnect();
    virtual void on_hw_receive(void* data, size_t size);
    virtual void on_hw_send(void* data, size_t size) = 0;
};

}  // namespace buttplug

#endif /*MMCC_SERVER_HPP*/