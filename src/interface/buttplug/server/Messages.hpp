#ifndef MMCC_MESSAGES_HPP
#define MMCC_MESSAGES_HPP

#include <AllocableStack.hpp>

#include <utility>

#include <buttplug.pb.h>

namespace buttplug::message {

// Forward declarations for friendships.
class Client;
class Server;

bool _encode_string(pb_ostream_t* stream, const pb_field_t* field, void* const* arg);
bool _encode_bytes(pb_ostream_t* stream, const pb_field_t* field, void* const* arg);

namespace status {

class Ping : protected buttplug_status_Ping {
   public:
    Ping();

    void set_id(uint32_t id);
    uint32_t get_id() const;

   protected:
    buttplug_status_Ping& get_nanopb_message();

    friend class buttplug::message::Client;
};

class Ok : protected buttplug_status_Ok {
   public:
    Ok();

    void set_id(uint32_t id);
    uint32_t get_id() const;

   protected:
    const buttplug_status_Ok& get_nanopb_message() const;

    friend class buttplug::message::Server;
};

class Error {
   public:
    enum Code {
        Unknown = buttplug_status_Error_Code_UNKNOWN,
        Init = buttplug_status_Error_Code_INIT,
        Ping = buttplug_status_Error_Code_PING,
        Msg = buttplug_status_Error_Code_MSG,
        Device = buttplug_status_Error_Code_DEVICE
    };

   protected:
    buttplug_status_Error _store;

   public:
    Error();

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_error_message(const char* msg);
    const char* get_error_message() const;

    void set_error_code(Code code);
    Code get_error_code() const;

   protected:
    const buttplug_status_Error& get_nanopb_message() const;

    friend class buttplug::message::Server;
};

}  // namespace status

namespace handshake {

class RequestServerInfo {
   protected:
    buttplug_handshake_RequestServerInfo _store;
    const char* _client_name;
    mmcc::AllocableStack& _allocator;

   public:
    RequestServerInfo(mmcc::AllocableStack& allocator);

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_client_name(const char* msg);
    const char* get_client_name() const;

    void set_message_version(uint32_t msg_ver);
    uint32_t get_message_version() const;

   protected:
    buttplug_handshake_RequestServerInfo& get_nanopb_message();

    friend class buttplug::message::Client;

    static bool _decode_client_name(pb_istream_t* stream, const pb_field_t* field, void** arg);
};

class ServerInfo {
   protected:
    buttplug_handshake_ServerInfo _store;

   public:
    ServerInfo();

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_server_name(const char* msg);
    const char* get_server_name() const;

    void set_message_version(uint32_t msg_ver);
    uint32_t get_message_version() const;

    void set_max_ping_time(uint32_t msg_ver);
    uint32_t get_max_ping_time() const;

   protected:
    const buttplug_handshake_ServerInfo& get_nanopb_message() const;

    friend class buttplug::message::Server;
};

}  // namespace handshake

namespace enumeration {

class StartScanning : protected buttplug_enumeration_StartScanning {
   public:
    StartScanning();

    void set_id(uint32_t id);
    uint32_t get_id() const;

   protected:
    buttplug_enumeration_StartScanning& get_nanopb_message();

    friend class buttplug::message::Client;
};

class StopScanning : protected buttplug_enumeration_StopScanning {
   public:
    StopScanning();

    void set_id(uint32_t id);
    uint32_t get_id() const;

   protected:
    buttplug_enumeration_StopScanning& get_nanopb_message();

    friend class buttplug::message::Client;
};

class RequestDeviceList : protected buttplug_enumeration_RequestDeviceList {
   public:
    RequestDeviceList();

    void set_id(uint32_t id);
    uint32_t get_id() const;

   protected:
    buttplug_enumeration_RequestDeviceList& get_nanopb_message();

    friend class buttplug::message::Client;
};

class ScanningFinished : protected buttplug_enumeration_ScanningFinished {
   public:
    ScanningFinished();

    void set_id(uint32_t id);
    uint32_t get_id() const;

   protected:
    const buttplug_enumeration_ScanningFinished& get_nanopb_message() const;

    friend class buttplug::message::Server;
};

class Device {
   public:
    class MessageAttributeEntry {
       public:
        enum Command {
            StopDeviceCmd = buttplug_enumeration_Device_Commands_StopDeviceCmd,
            RawWriteCmd = buttplug_enumeration_Device_Commands_RawWriteCmd,
            RawReadCmd = buttplug_enumeration_Device_Commands_RawReadCmd,
            RawSubscribeCmd = buttplug_enumeration_Device_Commands_RawSubscribeCmd,
            RawUnsubscribeCmd = buttplug_enumeration_Device_Commands_RawUnsubscribeCmd,
            VibrateCmd = buttplug_enumeration_Device_Commands_VibrateCmd,
            LinearCmd = buttplug_enumeration_Device_Commands_LinearCmd,
            RotateCmd = buttplug_enumeration_Device_Commands_RotateCmd,
            BatteryLevelCmd = buttplug_enumeration_Device_Commands_BatteryLevelCmd,
            RSSILevelCmd = buttplug_enumeration_Device_Commands_RSSILevelCmd
        };

       protected:
        buttplug_enumeration_Device_DeviceMessagesEntry _store;
        const uint32_t* _step_count_list;
        size_t _step_count_len;

       public:
        constexpr MessageAttributeEntry() :
            _store(buttplug_enumeration_Device_DeviceMessagesEntry_init_default),
            _step_count_list(nullptr), _step_count_len(0) {
            _store.has_value = true;
            _store.value.step_count.funcs.encode = _encode_list;
            _store.value.step_count.arg = this;
        }
        constexpr MessageAttributeEntry(Command cmd, uint32_t ftc, const uint32_t* steps,
                                        size_t steps_len) :
            MessageAttributeEntry() {
            _store.key = cmd;
            _store.value.feature_count = ftc;
            _step_count_list = steps;
            _step_count_len = steps_len;
        }

        void set_command_id(Command cmd);
        Command get_command_id() const;

        void set_feature_count(uint32_t ftc);
        uint32_t get_feature_count() const;

        void set_step_counts(const uint32_t* l, size_t n);
        std::pair<const uint32_t*, size_t> get_step_counts() const;

       protected:
        const buttplug_enumeration_Device_DeviceMessagesEntry& get_nanopb_message() const;

        static bool _encode_list(pb_ostream_t* stream, const pb_field_t* field,
                                 void* const* arg);

        friend class Device;
    };

   protected:
    buttplug_enumeration_Device _store;
    const MessageAttributeEntry* _msg_attr_map;
    size_t _msg_attr_len;

   public:
    constexpr Device() :
        _store(buttplug_enumeration_Device_init_default), _msg_attr_map(nullptr),
        _msg_attr_len(0) {
        _store.device_name.funcs.encode = _encode_string;
        _store.device_name.arg = nullptr;
        _store.device_messages.funcs.encode = _encode_attributes;
        _store.device_messages.arg = this;
    }
    template<size_t N>
    constexpr Device(uint32_t index, const char* name, const MessageAttributeEntry (&map)[N]) :
        Device() {
        _store.device_index = index;
        _store.device_name.arg = (void*) name;
        _msg_attr_map = map;
        _msg_attr_len = N;
    }

    void set_name(const char* name);
    const char* get_name() const;

    void set_index(uint32_t index);
    uint32_t get_index() const;

    void set_messages(const MessageAttributeEntry* map, size_t n);
    std::pair<const MessageAttributeEntry*, size_t> get_messages() const;

   protected:
    const buttplug_enumeration_Device& get_nanopb_message() const;

    static bool _encode_attributes(pb_ostream_t* stream, const pb_field_t* field,
                                   void* const* arg);

    friend class DeviceList;
    friend class DeviceAdded;
};

class DeviceList {
   protected:
    buttplug_enumeration_DeviceList _store;
    const Device* _dev_list;
    size_t _dev_len;

   public:
    DeviceList();

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_devices(const Device* l, size_t n);
    std::pair<const Device*, size_t> get_devices() const;

   protected:
    const buttplug_enumeration_DeviceList& get_nanopb_message() const;

    friend class buttplug::message::Server;

    static bool _encode_devices(pb_ostream_t* stream, const pb_field_t* field,
                                void* const* arg);
};

class DeviceAdded {
   protected:
    buttplug_enumeration_DeviceAdded _store;
    Device _dev;

   public:
    DeviceAdded();

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_device(const Device& dev);
    const Device& get_device() const;

   protected:
    const buttplug_enumeration_DeviceAdded& get_nanopb_message() const;

    friend class buttplug::message::Server;

    static bool _encode_device(pb_ostream_t* stream, const pb_field_t* field, void* const* arg);
};

class DeviceRemoved : protected buttplug_enumeration_DeviceRemoved {
   public:
    DeviceRemoved();

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_device(const Device& dev);
    void set_device(uint32_t dev_id);
    uint32_t get_device() const;

   protected:
    const buttplug_enumeration_DeviceRemoved& get_nanopb_message() const;

    friend class buttplug::message::Server;
};

}  // namespace enumeration

namespace device {

namespace raw {

class RawWriteCmd {
   protected:
    buttplug_device_raw_RawWriteCmd _store;
    const char* _endpoint;
    const void* _data;
    size_t _data_len;

    mmcc::AllocableStack& _allocator;

   public:
    RawWriteCmd(mmcc::AllocableStack& allocator);

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_device_index(uint32_t dev_id);
    uint32_t get_device_index() const;

    void set_endpoint(const char* ep);
    const char* get_endpoint() const;

    void set_data(const void* d, size_t l);
    std::pair<const void*, size_t> get_data() const;

    void set_write_with_response(bool v);
    bool get_write_with_response() const;

   protected:
    buttplug_device_raw_RawWriteCmd& get_nanopb_message();

    friend class buttplug::message::Client;

    static bool _decode_endpoint(pb_istream_t* stream, const pb_field_t* field, void** arg);
    static bool _decode_data(pb_istream_t* stream, const pb_field_t* field, void** arg);
};

class RawReadCmd {
   protected:
    buttplug_device_raw_RawReadCmd _store;
    const char* _endpoint;

    mmcc::AllocableStack& _allocator;

   public:
    RawReadCmd(mmcc::AllocableStack& allocator);

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_device_index(uint32_t dev_id);
    uint32_t get_device_index() const;

    void set_endpoint(const char* ep);
    const char* get_endpoint() const;

    void set_expected_length(uint32_t len);
    uint32_t get_expected_length() const;

    void set_wait_for_data(bool v);
    bool get_wait_for_data() const;

   protected:
    buttplug_device_raw_RawReadCmd& get_nanopb_message();

    friend class buttplug::message::Client;

    static bool _decode_endpoint(pb_istream_t* stream, const pb_field_t* field, void** arg);
};

class RawSubscribeCmd {
   protected:
    buttplug_device_raw_RawSubscribeCmd _store;
    const char* _endpoint;

    mmcc::AllocableStack& _allocator;

   public:
    RawSubscribeCmd(mmcc::AllocableStack& allocator);

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_device_index(uint32_t dev_id);
    uint32_t get_device_index() const;

    void set_endpoint(const char* ep);
    const char* get_endpoint() const;

   protected:
    buttplug_device_raw_RawSubscribeCmd& get_nanopb_message();

    friend class buttplug::message::Client;

    static bool _decode_endpoint(pb_istream_t* stream, const pb_field_t* field, void** arg);
};

class RawUnsubscribeCmd {
   protected:
    buttplug_device_raw_RawUnsubscribeCmd _store;
    const char* _endpoint;

    mmcc::AllocableStack& _allocator;

   public:
    RawUnsubscribeCmd(mmcc::AllocableStack& allocator);

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_device_index(uint32_t dev_id);
    uint32_t get_device_index() const;

    void set_endpoint(const char* ep);
    const char* get_endpoint() const;

   protected:
    buttplug_device_raw_RawUnsubscribeCmd& get_nanopb_message();

    friend class buttplug::message::Client;

    static bool _decode_endpoint(pb_istream_t* stream, const pb_field_t* field, void** arg);
};

class RawReading {
   protected:
    buttplug_device_raw_RawReading _store;
    std::pair<const void*, size_t> _data;

   public:
    RawReading();

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_device_index(uint32_t dev_id);
    uint32_t get_device_index() const;

    void set_endpoint(const char* ep);
    const char* get_endpoint() const;

    void set_data(const void* d, size_t l);
    std::pair<const void*, size_t> get_data();

   protected:
    const buttplug_device_raw_RawReading& get_nanopb_message() const;

    friend class buttplug::message::Server;
};

}  // namespace raw

namespace generic {

class StopDeviceCmd : protected buttplug_device_generic_StopDeviceCmd {
   public:
    StopDeviceCmd();

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_device_index(uint32_t dev_id);
    uint32_t get_device_index() const;

   protected:
    buttplug_device_generic_StopDeviceCmd& get_nanopb_message();

    friend class buttplug::message::Client;
};

class StopAllDevices : protected buttplug_device_generic_StopAllDevices {
   public:
    StopAllDevices();

    void set_id(uint32_t id);
    uint32_t get_id() const;

   protected:
    buttplug_device_generic_StopAllDevices& get_nanopb_message();

    friend class buttplug::message::Client;
};

class VibrateCmd {
   public:
    class SpeedEntry : protected buttplug_device_generic_VibrateCmd_SpeedsEntry {
       public:
        SpeedEntry();

        void set_index(uint32_t index);
        uint32_t get_index() const;

        void set_speed(float speed);
        float get_speed() const;

       protected:
        buttplug_device_generic_VibrateCmd_SpeedsEntry& get_nanopb_message();

        friend class VibrateCmd;
    };

   protected:
    buttplug_device_generic_VibrateCmd _store;
    const SpeedEntry* _speed_map;
    size_t _speed_len;

    mmcc::AllocableStack& _allocator;

   public:
    VibrateCmd(mmcc::AllocableStack& allocator);

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_device_index(uint32_t dev_id);
    uint32_t get_device_index() const;

    void set_speeds(const SpeedEntry* d, size_t l);
    std::pair<const SpeedEntry*, size_t> get_speeds() const;

   protected:
    buttplug_device_generic_VibrateCmd& get_nanopb_message();

    friend class buttplug::message::Client;

    static bool _decode_speeds(pb_istream_t* stream, const pb_field_t* field, void** arg);
};

class LinearCmd {
   public:
    class VectorEntry : protected buttplug_device_generic_LinearCmd_VectorsEntry {
       public:
        VectorEntry();

        void set_index(uint32_t index);
        uint32_t get_index() const;

        void set_duration(uint32_t dur);
        uint32_t get_duration() const;

        void set_position(float position);
        float get_position() const;

       protected:
        buttplug_device_generic_LinearCmd_VectorsEntry& get_nanopb_message();

        friend class LinearCmd;
    };

   protected:
    buttplug_device_generic_LinearCmd _store;
    const VectorEntry* _vector_map;
    size_t _vector_len;

    mmcc::AllocableStack& _allocator;

   public:
    LinearCmd(mmcc::AllocableStack& allocator);

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_device_index(uint32_t dev_id);
    uint32_t get_device_index() const;

    void set_vectors(const VectorEntry* d, size_t l);
    std::pair<const VectorEntry*, size_t> get_vectors() const;

   protected:
    buttplug_device_generic_LinearCmd& get_nanopb_message();

    friend class buttplug::message::Client;

    static bool _decode_vecs(pb_istream_t* stream, const pb_field_t* field, void** arg);
};

class RotateCmd {
   public:
    class RotationEntry : protected buttplug_device_generic_RotateCmd_RotationsEntry {
       public:
        RotationEntry();

        void set_index(uint32_t index);
        uint32_t get_index() const;

        void set_speed(float speed);
        float get_speed() const;

        void set_clockwise(bool cw);
        bool get_clockwise() const;

       protected:
        buttplug_device_generic_RotateCmd_RotationsEntry& get_nanopb_message();

        friend class RotateCmd;
    };

   protected:
    buttplug_device_generic_RotateCmd _store;
    const RotationEntry* _rotation_map;
    size_t _rotation_len;

    mmcc::AllocableStack& _allocator;

   public:
    RotateCmd(mmcc::AllocableStack& allocator);

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_device_index(uint32_t dev_id);
    uint32_t get_device_index() const;

    void set_rotations(const RotationEntry* d, size_t l);
    std::pair<const RotationEntry*, size_t> get_rotations() const;

   protected:
    buttplug_device_generic_RotateCmd& get_nanopb_message();

    friend class buttplug::message::Client;

    static bool _decode_rotations(pb_istream_t* stream, const pb_field_t* field, void** arg);
};

}  // namespace generic

namespace sensor {

class BatteryLevelCmd : protected buttplug_device_sensor_BatteryLevelCmd {
   public:
    BatteryLevelCmd();

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_device_index(uint32_t dev_id);
    uint32_t get_device_index() const;

   protected:
    buttplug_device_sensor_BatteryLevelCmd& get_nanopb_message();

    friend class buttplug::message::Client;
};

class RSSILevelCmd : protected buttplug_device_sensor_RSSILevelCmd {
   public:
    RSSILevelCmd();

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_device_index(uint32_t dev_id);
    uint32_t get_device_index() const;

   protected:
    buttplug_device_sensor_RSSILevelCmd& get_nanopb_message();

    friend class buttplug::message::Client;
};

class BatteryLevelReading : protected buttplug_device_sensor_BatteryLevelReading {
   public:
    BatteryLevelReading();

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_device_index(uint32_t dev_id);
    uint32_t get_device_index() const;

    void set_battery_level(float lvl);
    float get_battery_level();

   protected:
    const buttplug_device_sensor_BatteryLevelReading& get_nanopb_message() const;

    friend class buttplug::message::Server;
};

class RSSILevelReading : protected buttplug_device_sensor_RSSILevelReading {
   public:
    RSSILevelReading();

    void set_id(uint32_t id);
    uint32_t get_id() const;

    void set_device_index(uint32_t dev_id);
    uint32_t get_device_index() const;

    void set_rssi_level(int32_t lvl);
    int32_t get_rssi_level();

   protected:
    const buttplug_device_sensor_RSSILevelReading& get_nanopb_message() const;

    friend class buttplug::message::Server;
};

}  // namespace sensor

}  // namespace device

class Client {
   public:
    enum Message {
        Ping = buttplug_Client_ping_tag,
        StopDeviceCMD = buttplug_Client_stop_device_cmd_tag,
        StopAllDevices = buttplug_Client_stop_all_devices_tag,
        VibrateCMD = buttplug_Client_vibrate_cmd_tag,
        LinearCMD = buttplug_Client_linear_cmd_tag,
        RotateCMD = buttplug_Client_rotate_cmd_tag,
        BatteryLevelCMD = buttplug_Client_battery_level_cmd_tag,
        RssiLevelCMD = buttplug_Client_rssi_level_cmd_tag,
        RawWriteCMD = buttplug_Client_raw_write_cmd_tag,
        RawReadCMD = buttplug_Client_raw_read_cmd_tag,
        RequestServerInfo = buttplug_Client_request_server_info_tag,
        StartScanning = buttplug_Client_start_scanning_tag,
        StopScanning = buttplug_Client_stop_scanning_tag,
        RequestDeviceList = buttplug_Client_request_device_list_tag,
        RawSubscribeCMD = buttplug_Client_raw_subscribe_cmd_tag,
        RawUnsubscribeCMD = buttplug_Client_raw_unsubscribe_cmd_tag
    };

   protected:
    _buttplug_Client _store;

    mmcc::AllocableStack& _allocator;
    void* _msg;

   public:
    Client(mmcc::AllocableStack& allocator);

    size_t decode(uint8_t* data, size_t len);

    Message which() const;

    const status::Ping& get_ping() const;
    const device::generic::StopDeviceCmd& get_stop_device_cmd() const;
    const device::generic::StopAllDevices& get_stop_all_devices() const;
    const device::generic::VibrateCmd& get_vibrate_cmd() const;
    const device::generic::LinearCmd& get_linear_cmd() const;
    const device::generic::RotateCmd& get_rotate_cmd() const;
    const device::sensor::BatteryLevelCmd& get_battery_level_cmd() const;
    const device::sensor::RSSILevelCmd& get_rssi_level_cmd() const;
    const device::raw::RawWriteCmd& get_raw_write_cmd() const;
    const device::raw::RawReadCmd& get_raw_read_cmd() const;
    const handshake::RequestServerInfo& get_request_server_info() const;
    const enumeration::StartScanning& get_start_scanning() const;
    const enumeration::StopScanning& get_stop_scanning() const;
    const enumeration::RequestDeviceList& get_request_device_list() const;
    const device::raw::RawSubscribeCmd& get_raw_subscribe_cmd() const;
    const device::raw::RawUnsubscribeCmd& get_raw_unsubscribe_cmd() const;

   protected:
    static bool _decode_client_msg(pb_istream_t* stream, const pb_field_t* field, void** arg);
};

class Server {
   public:
    enum Message {
        Ok = buttplug_Server_ok_tag,
        DeviceAdded = buttplug_Server_device_added_tag,
        DeviceRemoved = buttplug_Server_device_removed_tag,
        RSSILevelReading = buttplug_Server_rssi_level_reading_tag,
        BatteryLevelReading = buttplug_Server_battery_level_reading_tag,
        RawReading = buttplug_Server_raw_reading_tag,
        Error = buttplug_Server_error_tag,
        ServerInfo = buttplug_Server_server_info_tag,
        ScanningFinished = buttplug_Server_scanning_finished_tag,
        DeviceList = buttplug_Server_device_list_tag
    };

   protected:
    _buttplug_Server _store;

   public:
    Server();

    void set_ok(const status::Ok& msg);
    void set_device_added(const enumeration::DeviceAdded& msg);
    void set_device_removed(const enumeration::DeviceRemoved& msg);
    void set_rssi_level_reading(const device::sensor::RSSILevelReading& msg);
    void set_battery_level_reading(const device::sensor::BatteryLevelReading& msg);
    void set_raw_reading(const device::raw::RawReading& msg);
    void set_error(const status::Error& msg);
    void set_server_info(const handshake::ServerInfo& msg);
    void set_scanning_finished(const enumeration::ScanningFinished& msg);
    void set_device_list(const enumeration::DeviceList& msg);

    size_t encode(uint8_t* data, size_t len) const;
};

}  // namespace buttplug::message

#endif /*MMCC_MESSAGES_HPP*/