#include "Messages.hpp"
#include "status.pb.h"

#include <Objects.hpp>

#include <cstdlib>
#include <cstring>
#include <new>

#include <pb_decode.h>
#include <pb_encode.h>

namespace buttplug::message {

bool _encode_string(pb_ostream_t* stream, const pb_field_t* field, void* const* arg) {
    const char* str = static_cast<const char*>(*arg);
    if (str != nullptr) {
        if (!pb_encode_tag_for_field(stream, field)) {
            return false;
        }

        return pb_encode_string(stream, (pb_byte_t*) str, strlen(str));
    }
    return true;
}

bool _encode_bytes(pb_ostream_t* stream, const pb_field_t* field, void* const* arg) {
    const std::pair<const void*, size_t>* a =
         static_cast<std::pair<const void*, size_t>*>(*arg);
    if (a->first != nullptr) {
        if (!pb_encode_tag_for_field(stream, field)) {
            return false;
        }

        return pb_encode_string(stream, (pb_byte_t*) a->first, a->second);
    }
    return true;
}

namespace status {

Ping::Ping() : buttplug_status_Ping(buttplug_status_Ping_init_default) {}
void Ping::set_id(uint32_t id) {
    buttplug_status_Ping::id = id;
}
uint32_t Ping::get_id() const {
    return buttplug_status_Ping::id;
}
buttplug_status_Ping& Ping::get_nanopb_message() {
    return *this;
}

Ok::Ok() : buttplug_status_Ok(buttplug_status_Ok_init_default) {}
void Ok::set_id(uint32_t id) {
    buttplug_status_Ok::id = id;
}
uint32_t Ok::get_id() const {
    return buttplug_status_Ok::id;
}
const buttplug_status_Ok& Ok::get_nanopb_message() const {
    return *this;
}

Error::Error() : _store(buttplug_status_Error_init_default) {
    _store.error_message.funcs.encode = _encode_string;
    _store.error_message.arg = nullptr;
}
void Error::set_id(uint32_t id) {
    _store.id = id;
}
uint32_t Error::get_id() const {
    return _store.id;
}
void Error::set_error_message(const char* msg) {
    _store.error_message.arg = (void*) msg;
}
const char* Error::get_error_message() const {
    return (const char*) _store.error_message.arg;
}
void Error::set_error_code(Error::Code code) {
    _store.error_code = (buttplug_status_Error_Code) code;
}
Error::Code Error::get_error_code() const {
    return (Error::Code) _store.error_code;
}
const buttplug_status_Error& Error::get_nanopb_message() const {
    return _store;
}

}  // namespace status

namespace handshake {

RequestServerInfo::RequestServerInfo(mmcc::AllocableStack& allocator) :
    _store(buttplug_handshake_RequestServerInfo_init_default), _client_name(nullptr),
    _allocator(allocator) {
    _store.client_name.funcs.decode = _decode_client_name;
    _store.client_name.arg = this;
}
void RequestServerInfo::set_id(uint32_t id) {
    _store.id = id;
}
uint32_t RequestServerInfo::get_id() const {
    return _store.id;
}
void RequestServerInfo::set_client_name(const char* msg) {
    _client_name = msg;
}
const char* RequestServerInfo::get_client_name() const {
    return _client_name;
}
void RequestServerInfo::set_message_version(uint32_t msg_ver) {
    _store.message_version = msg_ver;
}
uint32_t RequestServerInfo::get_message_version() const {
    return _store.message_version;
}
buttplug_handshake_RequestServerInfo& RequestServerInfo::get_nanopb_message() {
    return _store;
}
bool RequestServerInfo::_decode_client_name(pb_istream_t* stream, const pb_field_t*,
                                            void** arg) {
    RequestServerInfo* obj = (RequestServerInfo*) *arg;
    size_t n = stream->bytes_left;
    char* s = (char*) obj->_allocator.allocate(n + 1);
    if (s != nullptr) {
        pb_read(stream, (pb_byte_t*) s, n);
        s[n] = '\0';
        obj->_client_name = s;
        return true;
    }
    return false;
}

ServerInfo::ServerInfo() : _store(buttplug_handshake_ServerInfo_init_default) {
    _store.server_name.funcs.encode = _encode_string;
    _store.server_name.arg = nullptr;
}
void ServerInfo::set_id(uint32_t id) {
    _store.id = id;
}
uint32_t ServerInfo::get_id() const {
    return _store.id;
}
void ServerInfo::set_server_name(const char* msg) {
    _store.server_name.arg = (void*) msg;
}
const char* ServerInfo::get_server_name() const {
    return (const char*) _store.server_name.arg;
}
void ServerInfo::set_message_version(uint32_t msg_ver) {
    _store.message_version = msg_ver;
}
uint32_t ServerInfo::get_message_version() const {
    return _store.message_version;
}
void ServerInfo::set_max_ping_time(uint32_t msg_ver) {
    _store.max_ping_time = msg_ver;
}
uint32_t ServerInfo::get_max_ping_time() const {
    return _store.max_ping_time;
}
const buttplug_handshake_ServerInfo& ServerInfo::get_nanopb_message() const {
    return _store;
}

}  // namespace handshake

namespace enumeration {

StartScanning::StartScanning() :
    buttplug_enumeration_StartScanning(buttplug_enumeration_StartScanning_init_default) {}
void StartScanning::set_id(uint32_t id) {
    buttplug_enumeration_StartScanning::id = id;
}
uint32_t StartScanning::get_id() const {
    return buttplug_enumeration_StartScanning::id;
}
buttplug_enumeration_StartScanning& StartScanning::get_nanopb_message() {
    return *this;
}

StopScanning::StopScanning() :
    buttplug_enumeration_StopScanning(buttplug_enumeration_StopScanning_init_default) {}
void StopScanning::set_id(uint32_t id) {
    buttplug_enumeration_StopScanning::id = id;
}
uint32_t StopScanning::get_id() const {
    return buttplug_enumeration_StopScanning::id;
}
buttplug_enumeration_StopScanning& StopScanning::get_nanopb_message() {
    return *this;
}

RequestDeviceList::RequestDeviceList() :
    buttplug_enumeration_RequestDeviceList(
         buttplug_enumeration_RequestDeviceList_init_default) {}
void RequestDeviceList::set_id(uint32_t id) {
    buttplug_enumeration_RequestDeviceList::id = id;
}
uint32_t RequestDeviceList::get_id() const {
    return buttplug_enumeration_RequestDeviceList::id;
}
buttplug_enumeration_RequestDeviceList& RequestDeviceList::get_nanopb_message() {
    return *this;
}

ScanningFinished::ScanningFinished() :
    buttplug_enumeration_ScanningFinished(buttplug_enumeration_ScanningFinished_init_default) {}
void ScanningFinished::set_id(uint32_t id) {
    buttplug_enumeration_ScanningFinished::id = id;
}
uint32_t ScanningFinished::get_id() const {
    return buttplug_enumeration_ScanningFinished::id;
}
const buttplug_enumeration_ScanningFinished& ScanningFinished::get_nanopb_message() const {
    return *this;
}

void Device::MessageAttributeEntry::set_command_id(Device::MessageAttributeEntry::Command cmd) {
    _store.key = cmd;
}
Device::MessageAttributeEntry::Command Device::MessageAttributeEntry::get_command_id() const {
    return (Device::MessageAttributeEntry::Command) _store.key;
}
void Device::MessageAttributeEntry::set_feature_count(uint32_t ftc) {
    _store.value.feature_count = ftc;
}
uint32_t Device::MessageAttributeEntry::get_feature_count() const {
    return _store.value.feature_count;
}
void Device::MessageAttributeEntry::set_step_counts(const uint32_t* l, size_t n) {
    _step_count_list = l;
    _step_count_len = n;
}
std::pair<const uint32_t*, size_t> Device::MessageAttributeEntry::get_step_counts() const {
    return {_step_count_list, _step_count_len};
}
const buttplug_enumeration_Device_DeviceMessagesEntry&
Device::MessageAttributeEntry::get_nanopb_message() const {
    return _store;
}
bool Device::MessageAttributeEntry::_encode_list(pb_ostream_t* stream, const pb_field_t* field,
                                                 void* const* arg) {
    const Device::MessageAttributeEntry* obj = (const Device::MessageAttributeEntry*) *arg;
    auto* l = obj->_step_count_list;
    size_t n = obj->_step_count_len;
    for (size_t i = 0; i < n; i++) {
        if (!pb_encode_tag_for_field(stream, field)) {
            return false;
        }
        if (!pb_encode_varint(stream, l[i])) {
            return false;
        }
    }
    return true;
}
void Device::set_name(const char* name) {
    _store.device_name.arg = (void*) name;
}
const char* Device::get_name() const {
    return (const char*) _store.device_name.arg;
}
void Device::set_index(uint32_t index) {
    _store.device_index = index;
}
uint32_t Device::get_index() const {
    return _store.device_index;
}
void Device::set_messages(const Device::MessageAttributeEntry* map, size_t n) {
    _msg_attr_map = map;
    _msg_attr_len = n;
}
std::pair<const Device::MessageAttributeEntry*, size_t> Device::get_messages() const {
    return {_msg_attr_map, _msg_attr_len};
}
const buttplug_enumeration_Device& Device::get_nanopb_message() const {
    return _store;
}
bool Device::_encode_attributes(pb_ostream_t* stream, const pb_field_t* field,
                                void* const* arg) {
    const Device* obj = (const Device*) *arg;
    auto* l = obj->_msg_attr_map;
    size_t n = obj->_msg_attr_len;
    for (size_t i = 0; i < n; i++) {
        if (!pb_encode_tag_for_field(stream, field)) {
            return false;
        }
        if (!pb_encode_submessage(stream,
                                  buttplug_enumeration_Device_DeviceMessagesEntry_fields,
                                  &(l[i].get_nanopb_message()))) {
            return false;
        }
    }
    return true;
}

DeviceList::DeviceList() : _store(buttplug_enumeration_DeviceList_init_default) {
    _store.devices.funcs.encode = _encode_devices;
    _store.devices.arg = this;
    _dev_list = nullptr;
    _dev_len = 0;
}
void DeviceList::set_id(uint32_t id) {
    _store.id = id;
}
uint32_t DeviceList::get_id() const {
    return _store.id;
}
void DeviceList::set_devices(const Device* l, size_t n) {
    _dev_list = l;
    _dev_len = n;
}
std::pair<const Device*, size_t> DeviceList::get_devices() const {
    return {_dev_list, _dev_len};
}
const buttplug_enumeration_DeviceList& DeviceList::get_nanopb_message() const {
    return _store;
}
bool DeviceList::_encode_devices(pb_ostream_t* stream, const pb_field_t* field,
                                 void* const* arg) {
    const DeviceList* obj = (const DeviceList*) *arg;
    auto* l = obj->_dev_list;
    size_t n = obj->_dev_len;
    for (size_t i = 0; i < n; i++) {
        if (!pb_encode_tag_for_field(stream, field)) {
            return false;
        }
        if (!pb_encode_submessage(stream, buttplug_enumeration_Device_fields,
                                  &(l[i].get_nanopb_message()))) {
            return false;
        }
    }
    return true;
}

DeviceAdded::DeviceAdded() : _store(buttplug_enumeration_DeviceAdded_init_default) {
    _store.device.funcs.encode = _encode_device;
    _store.device.arg = this;
}
void DeviceAdded::set_id(uint32_t id) {
    _store.id = id;
}
uint32_t DeviceAdded::get_id() const {
    return _store.id;
}
void DeviceAdded::set_device(const Device& dev) {
    _dev = dev;
}
const Device& DeviceAdded::get_device() const {
    return _dev;
}
const buttplug_enumeration_DeviceAdded& DeviceAdded::get_nanopb_message() const {
    return _store;
}
bool DeviceAdded::_encode_device(pb_ostream_t* stream, const pb_field_t* field,
                                 void* const* arg) {
    const DeviceAdded* obj = (const DeviceAdded*) *arg;
    if (!pb_encode_tag_for_field(stream, field)) {
        return false;
    }
    if (!pb_encode_submessage(stream, buttplug_enumeration_Device_fields,
                              &(obj->_dev.get_nanopb_message()))) {
        return false;
    }
    return true;
}

DeviceRemoved::DeviceRemoved() :
    buttplug_enumeration_DeviceRemoved(buttplug_enumeration_DeviceRemoved_init_default) {}
void DeviceRemoved::set_id(uint32_t id) {
    buttplug_enumeration_DeviceRemoved::id = id;
}
uint32_t DeviceRemoved::get_id() const {
    return buttplug_enumeration_DeviceRemoved::id;
}
void DeviceRemoved::set_device(const Device& dev) {
    set_device(dev.get_index());
}
void DeviceRemoved::set_device(uint32_t dev_id) {
    buttplug_enumeration_DeviceRemoved::device_index = dev_id;
}
uint32_t DeviceRemoved::get_device() const {
    return buttplug_enumeration_DeviceRemoved::device_index;
}
const buttplug_enumeration_DeviceRemoved& DeviceRemoved::get_nanopb_message() const {
    return *this;
}

}  // namespace enumeration

namespace device {

namespace raw {

RawWriteCmd::RawWriteCmd(mmcc::AllocableStack& allocator) :
    _store(buttplug_device_raw_RawWriteCmd_init_default), _allocator(allocator) {
    _store.endpoint.funcs.decode = _decode_endpoint;
    _store.endpoint.arg = this;
    _store.data.funcs.decode = _decode_data;
    _store.data.arg = this;
    _endpoint = nullptr;
    _data = nullptr;
    _data_len = 0;
}
void RawWriteCmd::set_id(uint32_t id) {
    _store.id = id;
}
uint32_t RawWriteCmd::get_id() const {
    return _store.id;
}
void RawWriteCmd::set_device_index(uint32_t dev_id) {
    _store.device_index = dev_id;
}
uint32_t RawWriteCmd::get_device_index() const {
    return _store.device_index;
}
void RawWriteCmd::set_endpoint(const char* ep) {
    _endpoint = ep;
}
const char* RawWriteCmd::get_endpoint() const {
    return _endpoint;
}
void RawWriteCmd::set_data(const void* d, size_t l) {
    _data = d;
    _data_len = l;
}
std::pair<const void*, size_t> RawWriteCmd::get_data() const {
    return {_data, _data_len};
}
void RawWriteCmd::set_write_with_response(bool v) {
    _store.write_with_response = v;
}
bool RawWriteCmd::get_write_with_response() const {
    return _store.write_with_response;
}
buttplug_device_raw_RawWriteCmd& RawWriteCmd::get_nanopb_message() {
    return _store;
}
bool RawWriteCmd::_decode_endpoint(pb_istream_t* stream, const pb_field_t*, void** arg) {
    RawWriteCmd* obj = (RawWriteCmd*) *arg;
    size_t n = stream->bytes_left;
    char* s = (char*) obj->_allocator.allocate(n + 1);
    if (s != nullptr) {
        pb_read(stream, (pb_byte_t*) s, n);
        s[n] = '\0';
        obj->_endpoint = s;
        return true;
    }
    return false;
}
bool RawWriteCmd::_decode_data(pb_istream_t* stream, const pb_field_t*, void** arg) {
    RawWriteCmd* obj = (RawWriteCmd*) *arg;
    size_t n = stream->bytes_left;
    pb_byte_t* s = (pb_byte_t*) obj->_allocator.allocate(n);
    if (s != nullptr) {
        pb_read(stream, s, n);
        obj->_data = s;
        obj->_data_len = n;
        return true;
    }
    return false;
}

RawReadCmd::RawReadCmd(mmcc::AllocableStack& allocator) :
    _store(buttplug_device_raw_RawReadCmd_init_default), _allocator(allocator) {
    _store.endpoint.funcs.decode = _decode_endpoint;
    _store.endpoint.arg = this;
}
void RawReadCmd::set_id(uint32_t id) {
    _store.id = id;
}
uint32_t RawReadCmd::get_id() const {
    return _store.id;
}
void RawReadCmd::set_device_index(uint32_t dev_id) {
    _store.device_index = dev_id;
}
uint32_t RawReadCmd::get_device_index() const {
    return _store.device_index;
}
void RawReadCmd::set_endpoint(const char* ep) {
    _endpoint = ep;
}
const char* RawReadCmd::get_endpoint() const {
    return _endpoint;
}
void RawReadCmd::set_expected_length(uint32_t len) {
    _store.expected_length = len;
}
uint32_t RawReadCmd::get_expected_length() const {
    return _store.expected_length;
}
void RawReadCmd::set_wait_for_data(bool v) {
    _store.wait_for_data = v;
}
bool RawReadCmd::get_wait_for_data() const {
    return _store.wait_for_data;
}
buttplug_device_raw_RawReadCmd& RawReadCmd::get_nanopb_message() {
    return _store;
}
bool RawReadCmd::_decode_endpoint(pb_istream_t* stream, const pb_field_t*, void** arg) {
    RawReadCmd* obj = (RawReadCmd*) *arg;
    size_t n = stream->bytes_left;
    char* s = (char*) obj->_allocator.allocate(n + 1);
    if (s != nullptr) {
        pb_read(stream, (pb_byte_t*) s, n);
        s[n] = '\0';
        obj->_endpoint = s;
        return true;
    }
    return false;
}

RawSubscribeCmd::RawSubscribeCmd(mmcc::AllocableStack& allocator) :
    _store(buttplug_device_raw_RawSubscribeCmd_init_default), _allocator(allocator) {
    _store.endpoint.funcs.decode = _decode_endpoint;
    _store.endpoint.arg = this;
}
void RawSubscribeCmd::set_id(uint32_t id) {
    _store.id = id;
}
uint32_t RawSubscribeCmd::get_id() const {
    return _store.id;
}
void RawSubscribeCmd::set_device_index(uint32_t dev_id) {
    _store.device_index = dev_id;
}
uint32_t RawSubscribeCmd::get_device_index() const {
    return _store.device_index;
}
void RawSubscribeCmd::set_endpoint(const char* ep) {
    _endpoint = ep;
}
const char* RawSubscribeCmd::get_endpoint() const {
    return _endpoint;
}
buttplug_device_raw_RawSubscribeCmd& RawSubscribeCmd::get_nanopb_message() {
    return _store;
}
bool RawSubscribeCmd::_decode_endpoint(pb_istream_t* stream, const pb_field_t*, void** arg) {
    RawSubscribeCmd* obj = (RawSubscribeCmd*) *arg;
    size_t n = stream->bytes_left;
    char* s = (char*) obj->_allocator.allocate(n + 1);
    if (s != nullptr) {
        pb_read(stream, (pb_byte_t*) s, n);
        s[n] = '\0';
        obj->_endpoint = s;
        return true;
    }
    return false;
}

RawUnsubscribeCmd::RawUnsubscribeCmd(mmcc::AllocableStack& allocator) :
    _store(buttplug_device_raw_RawUnsubscribeCmd_init_default), _allocator(allocator) {
    _store.endpoint.funcs.decode = _decode_endpoint;
    _store.endpoint.arg = this;
}
void RawUnsubscribeCmd::set_id(uint32_t id) {
    _store.id = id;
}
uint32_t RawUnsubscribeCmd::get_id() const {
    return _store.id;
}
void RawUnsubscribeCmd::set_device_index(uint32_t dev_id) {
    _store.device_index = dev_id;
}
uint32_t RawUnsubscribeCmd::get_device_index() const {
    return _store.device_index;
}
void RawUnsubscribeCmd::set_endpoint(const char* ep) {
    _endpoint = ep;
}
const char* RawUnsubscribeCmd::get_endpoint() const {
    return _endpoint;
}
buttplug_device_raw_RawUnsubscribeCmd& RawUnsubscribeCmd::get_nanopb_message() {
    return _store;
}
bool RawUnsubscribeCmd::_decode_endpoint(pb_istream_t* stream, const pb_field_t*, void** arg) {
    RawUnsubscribeCmd* obj = (RawUnsubscribeCmd*) *arg;
    size_t n = stream->bytes_left;
    char* s = (char*) obj->_allocator.allocate(n + 1);
    if (s != nullptr) {
        pb_read(stream, (pb_byte_t*) s, n);
        s[n] = '\0';
        obj->_endpoint = s;
        return true;
    }
    return false;
}

RawReading::RawReading() : _store(buttplug_device_raw_RawReading_init_default) {
    _store.endpoint.funcs.encode = _encode_string;
    _store.data.funcs.encode = _encode_bytes;
    _store.data.arg = &_data;
}
void RawReading::set_id(uint32_t id) {
    _store.id = id;
}
uint32_t RawReading::get_id() const {
    return _store.id;
}
void RawReading::set_device_index(uint32_t dev_id) {
    _store.device_index = dev_id;
}
uint32_t RawReading::get_device_index() const {
    return _store.device_index;
}
void RawReading::set_endpoint(const char* ep) {
    _store.endpoint.arg = (void*) ep;
}
const char* RawReading::get_endpoint() const {
    return (const char*) _store.endpoint.arg;
}
void RawReading::set_data(const void* d, size_t l) {
    _data.first = d;
    _data.second = l;
}
std::pair<const void*, size_t> RawReading::get_data() {
    return _data;
}
const buttplug_device_raw_RawReading& RawReading::get_nanopb_message() const {
    return _store;
}

}  // namespace raw

namespace generic {

StopDeviceCmd::StopDeviceCmd() :
    buttplug_device_generic_StopDeviceCmd(buttplug_device_generic_StopDeviceCmd_init_default){};
void StopDeviceCmd::set_id(uint32_t id) {
    buttplug_device_generic_StopDeviceCmd::id = id;
}
uint32_t StopDeviceCmd::get_id() const {
    return buttplug_device_generic_StopDeviceCmd::id;
}
void StopDeviceCmd::set_device_index(uint32_t dev_id) {
    buttplug_device_generic_StopDeviceCmd::device_index = dev_id;
}
uint32_t StopDeviceCmd::get_device_index() const {
    return buttplug_device_generic_StopDeviceCmd::device_index;
}
buttplug_device_generic_StopDeviceCmd& StopDeviceCmd::get_nanopb_message() {
    return *this;
}

StopAllDevices::StopAllDevices() :
    buttplug_device_generic_StopAllDevices(
         buttplug_device_generic_StopAllDevices_init_default) {}
void StopAllDevices::set_id(uint32_t id) {
    buttplug_device_generic_StopAllDevices::id = id;
}
uint32_t StopAllDevices::get_id() const {
    return buttplug_device_generic_StopAllDevices::id;
}
buttplug_device_generic_StopAllDevices& StopAllDevices::get_nanopb_message() {
    return *this;
}

VibrateCmd::SpeedEntry::SpeedEntry() :
    buttplug_device_generic_VibrateCmd_SpeedsEntry(
         buttplug_device_generic_VibrateCmd_SpeedsEntry_init_default) {}
void VibrateCmd::SpeedEntry::set_index(uint32_t index) {
    buttplug_device_generic_VibrateCmd_SpeedsEntry::key = index;
}
uint32_t VibrateCmd::SpeedEntry::get_index() const {
    return buttplug_device_generic_VibrateCmd_SpeedsEntry::key;
}
void VibrateCmd::SpeedEntry::set_speed(float speed) {
    buttplug_device_generic_VibrateCmd_SpeedsEntry::value = speed;
}
float VibrateCmd::SpeedEntry::get_speed() const {
    return buttplug_device_generic_VibrateCmd_SpeedsEntry::value;
}
buttplug_device_generic_VibrateCmd_SpeedsEntry& VibrateCmd::SpeedEntry::get_nanopb_message() {
    return *this;
}

VibrateCmd::VibrateCmd(mmcc::AllocableStack& allocator) :
    _store(buttplug_device_generic_VibrateCmd_init_default), _allocator(allocator) {
    _store.speeds.funcs.decode = _decode_speeds;
    _store.speeds.arg = this;
    _speed_map = nullptr;
    _speed_len = 0;
}
void VibrateCmd::set_id(uint32_t id) {
    _store.id = id;
}
uint32_t VibrateCmd::get_id() const {
    return _store.id;
}
void VibrateCmd::set_device_index(uint32_t dev_id) {
    _store.device_index = dev_id;
}
uint32_t VibrateCmd::get_device_index() const {
    return _store.device_index;
}
void VibrateCmd::set_speeds(const SpeedEntry* d, size_t l) {
    _speed_map = d;
    _speed_len = l;
}
std::pair<const VibrateCmd::SpeedEntry*, size_t> VibrateCmd::get_speeds() const {
    return {_speed_map, _speed_len};
}
buttplug_device_generic_VibrateCmd& VibrateCmd::get_nanopb_message() {
    return _store;
}
bool VibrateCmd::_decode_speeds(pb_istream_t* stream, const pb_field_t*, void** arg) {
    VibrateCmd* obj = (VibrateCmd*) *arg;
    size_t n = 0;
    // First pass to find size of array.
    pb_istream_t fp_stream = *stream;
    while (fp_stream.bytes_left != 0) {
        buttplug_device_generic_VibrateCmd_SpeedsEntry tmp;
        if (!pb_decode(&fp_stream, buttplug_device_generic_VibrateCmd_SpeedsEntry_fields,
                       &tmp)) {
            return false;
        }
        else {
            n += 1;
        }
    }
    if (n == 0) {
        return true;
    }
    // Second pass, allocate and popularize array.
    VibrateCmd::SpeedEntry* m = (VibrateCmd::SpeedEntry*) obj->_allocator.allocate(
         sizeof(VibrateCmd::SpeedEntry) * n, alignof(VibrateCmd::SpeedEntry));
    for (size_t i = 0; i < n; i++) {
        if (!pb_decode(&fp_stream, buttplug_device_generic_VibrateCmd_SpeedsEntry_fields,
                       &m[i].get_nanopb_message())) {
            return false;
        }
    }
    obj->_speed_map = m;
    obj->_speed_len = n;
    return true;
}

LinearCmd::VectorEntry::VectorEntry() :
    buttplug_device_generic_LinearCmd_VectorsEntry(
         buttplug_device_generic_LinearCmd_VectorsEntry_init_default) {
    buttplug_device_generic_LinearCmd_VectorsEntry::has_value = true;
};
void LinearCmd::VectorEntry::set_index(uint32_t index) {
    buttplug_device_generic_LinearCmd_VectorsEntry::key = index;
}
uint32_t LinearCmd::VectorEntry::get_index() const {
    return buttplug_device_generic_LinearCmd_VectorsEntry::key;
}
void LinearCmd::VectorEntry::set_duration(uint32_t dur) {
    buttplug_device_generic_LinearCmd_VectorsEntry::value.duration = dur;
}
uint32_t LinearCmd::VectorEntry::get_duration() const {
    return buttplug_device_generic_LinearCmd_VectorsEntry::value.duration;
}
void LinearCmd::VectorEntry::set_position(float position) {
    buttplug_device_generic_LinearCmd_VectorsEntry::value.position = position;
}
float LinearCmd::VectorEntry::get_position() const {
    return buttplug_device_generic_LinearCmd_VectorsEntry::value.position;
}
buttplug_device_generic_LinearCmd_VectorsEntry& LinearCmd::VectorEntry::get_nanopb_message() {
    return *this;
}
LinearCmd::LinearCmd(mmcc::AllocableStack& allocator) :
    _store(buttplug_device_generic_LinearCmd_init_default), _allocator(allocator) {
    _store.vectors.funcs.decode = _decode_vecs;
    _store.vectors.arg = this;
    _vector_map = nullptr;
    _vector_len = 0;
}
void LinearCmd::set_id(uint32_t id) {
    _store.id = id;
}
uint32_t LinearCmd::get_id() const {
    return _store.id;
}
void LinearCmd::set_device_index(uint32_t dev_id) {
    _store.device_index = dev_id;
}
uint32_t LinearCmd::get_device_index() const {
    return _store.device_index;
}
void LinearCmd::set_vectors(const VectorEntry* d, size_t l) {
    _vector_map = d;
    _vector_len = l;
}
std::pair<const LinearCmd::VectorEntry*, size_t> LinearCmd::get_vectors() const {
    return {_vector_map, _vector_len};
}
buttplug_device_generic_LinearCmd& LinearCmd::get_nanopb_message() {
    return _store;
}
bool LinearCmd::_decode_vecs(pb_istream_t* stream, const pb_field_t*, void** arg) {
    LinearCmd* obj = (LinearCmd*) *arg;
    LinearCmd::VectorEntry* m = nullptr;
    size_t n = 0;
    while (stream->bytes_left != 0) {
        // Subsequently allocated blocks.
        LinearCmd::VectorEntry* c = (LinearCmd::VectorEntry*) obj->_allocator.allocate(
             sizeof(LinearCmd::VectorEntry), alignof(LinearCmd::VectorEntry));
        if (!m) {
            m = c;
        }
        if (!pb_decode(stream, buttplug_device_generic_LinearCmd_VectorsEntry_fields, c)) {
            return false;
        }
        else {
            n += 1;
        }
    }

    if (n == 0) {
        return true;
    }

    obj->_vector_map = m;
    obj->_vector_len = n;
    return true;
}

RotateCmd::RotationEntry::RotationEntry() :
    buttplug_device_generic_RotateCmd_RotationsEntry(
         buttplug_device_generic_RotateCmd_RotationsEntry_init_default) {
    buttplug_device_generic_RotateCmd_RotationsEntry::has_value = true;
}
void RotateCmd::RotationEntry::set_index(uint32_t index) {
    buttplug_device_generic_RotateCmd_RotationsEntry::key = index;
}
uint32_t RotateCmd::RotationEntry::get_index() const {
    return buttplug_device_generic_RotateCmd_RotationsEntry::key;
}
void RotateCmd::RotationEntry::set_speed(float speed) {
    buttplug_device_generic_RotateCmd_RotationsEntry::value.speed = speed;
}
float RotateCmd::RotationEntry::get_speed() const {
    return buttplug_device_generic_RotateCmd_RotationsEntry::value.speed;
}
void RotateCmd::RotationEntry::set_clockwise(bool cw) {
    buttplug_device_generic_RotateCmd_RotationsEntry::value.clockwise = cw;
}
bool RotateCmd::RotationEntry::get_clockwise() const {
    return buttplug_device_generic_RotateCmd_RotationsEntry::value.clockwise;
}
buttplug_device_generic_RotateCmd_RotationsEntry&
RotateCmd::RotationEntry::get_nanopb_message() {
    return *this;
}
RotateCmd::RotateCmd(mmcc::AllocableStack& allocator) :
    _store(buttplug_device_generic_RotateCmd_init_default), _allocator(allocator) {
    _store.rotations.funcs.decode = _decode_rotations;
    _store.rotations.arg = this;
    _rotation_map = nullptr;
    _rotation_len = 0;
}
void RotateCmd::set_id(uint32_t id) {
    _store.id = id;
}
uint32_t RotateCmd::get_id() const {
    return _store.id;
}
void RotateCmd::set_device_index(uint32_t dev_id) {
    _store.device_index = dev_id;
}
uint32_t RotateCmd::get_device_index() const {
    return _store.device_index;
}
void RotateCmd::set_rotations(const RotationEntry* d, size_t l) {
    _rotation_map = d;
    _rotation_len = l;
}
std::pair<const RotateCmd::RotationEntry*, size_t> RotateCmd::get_rotations() const {
    return {_rotation_map, _rotation_len};
}
buttplug_device_generic_RotateCmd& RotateCmd::get_nanopb_message() {
    return _store;
}
bool RotateCmd::_decode_rotations(pb_istream_t* stream, const pb_field_t*, void** arg) {
    RotateCmd* obj = (RotateCmd*) *arg;
    size_t n = 0;
    // First pass to find size of array.
    pb_istream_t fp_stream = *stream;
    while (fp_stream.bytes_left != 0) {
        buttplug_device_generic_RotateCmd_RotationsEntry tmp;
        if (!pb_decode(&fp_stream, buttplug_device_generic_RotateCmd_RotationsEntry_fields,
                       &tmp)) {
            return false;
        }
        else {
            n += 1;
        }
    }
    if (n == 0) {
        return true;
    }
    // Second pass, allocate and popularize array.
    RotateCmd::RotationEntry* m = (RotateCmd::RotationEntry*) obj->_allocator.allocate(
         sizeof(RotateCmd::RotationEntry) * n, alignof(RotateCmd::RotationEntry));
    for (size_t i = 0; i < n; i++) {
        if (!pb_decode(&fp_stream, buttplug_device_generic_RotateCmd_RotationsEntry_fields,
                       &m[i].get_nanopb_message())) {
            return false;
        }
    }
    obj->_rotation_map = m;
    obj->_rotation_len = n;
    return true;
}

}  // namespace generic

namespace sensor {

BatteryLevelCmd::BatteryLevelCmd() :
    buttplug_device_sensor_BatteryLevelCmd(
         buttplug_device_sensor_BatteryLevelCmd_init_default) {}
void BatteryLevelCmd::set_id(uint32_t id) {
    buttplug_device_sensor_BatteryLevelCmd::id = id;
}
uint32_t BatteryLevelCmd::get_id() const {
    return buttplug_device_sensor_BatteryLevelCmd::id;
}
void BatteryLevelCmd::set_device_index(uint32_t dev_id) {
    buttplug_device_sensor_BatteryLevelCmd::device_index = dev_id;
}
uint32_t BatteryLevelCmd::get_device_index() const {
    return buttplug_device_sensor_BatteryLevelCmd::device_index;
}
buttplug_device_sensor_BatteryLevelCmd& BatteryLevelCmd::get_nanopb_message() {
    return *this;
}

RSSILevelCmd::RSSILevelCmd() :
    buttplug_device_sensor_RSSILevelCmd(buttplug_device_sensor_RSSILevelCmd_init_default) {}
void RSSILevelCmd::set_id(uint32_t id) {
    buttplug_device_sensor_RSSILevelCmd::id = id;
}
uint32_t RSSILevelCmd::get_id() const {
    return buttplug_device_sensor_RSSILevelCmd::id;
}
void RSSILevelCmd::set_device_index(uint32_t dev_id) {
    buttplug_device_sensor_RSSILevelCmd::device_index = dev_id;
}
uint32_t RSSILevelCmd::get_device_index() const {
    return buttplug_device_sensor_RSSILevelCmd::device_index;
}
buttplug_device_sensor_RSSILevelCmd& RSSILevelCmd::get_nanopb_message() {
    return *this;
}

BatteryLevelReading::BatteryLevelReading() :
    buttplug_device_sensor_BatteryLevelReading(
         buttplug_device_sensor_BatteryLevelReading_init_default) {}
void BatteryLevelReading::set_id(uint32_t id) {
    buttplug_device_sensor_BatteryLevelReading::id = id;
}
uint32_t BatteryLevelReading::get_id() const {
    return buttplug_device_sensor_BatteryLevelReading::id;
}
void BatteryLevelReading::set_device_index(uint32_t dev_id) {
    buttplug_device_sensor_BatteryLevelReading::device_index = dev_id;
}
uint32_t BatteryLevelReading::get_device_index() const {
    return buttplug_device_sensor_BatteryLevelReading::device_index;
}
void BatteryLevelReading::set_battery_level(float lvl) {
    buttplug_device_sensor_BatteryLevelReading::battery_level = lvl;
}
float BatteryLevelReading::get_battery_level() {
    return buttplug_device_sensor_BatteryLevelReading::battery_level;
}
const buttplug_device_sensor_BatteryLevelReading& BatteryLevelReading::get_nanopb_message()
     const {
    return *this;
}

RSSILevelReading::RSSILevelReading() :
    buttplug_device_sensor_RSSILevelReading(
         buttplug_device_sensor_RSSILevelReading_init_default) {}
void RSSILevelReading::set_id(uint32_t id) {
    buttplug_device_sensor_RSSILevelReading::id = id;
}
uint32_t RSSILevelReading::get_id() const {
    return buttplug_device_sensor_RSSILevelReading::id;
}
void RSSILevelReading::set_device_index(uint32_t dev_id) {
    buttplug_device_sensor_RSSILevelReading::device_index = dev_id;
}
uint32_t RSSILevelReading::get_device_index() const {
    return buttplug_device_sensor_RSSILevelReading::device_index;
}
void RSSILevelReading::set_rssi_level(int32_t lvl) {
    buttplug_device_sensor_RSSILevelReading::rssi_level = lvl;
}
int32_t RSSILevelReading::get_rssi_level() {
    return buttplug_device_sensor_RSSILevelReading::rssi_level;
}
const buttplug_device_sensor_RSSILevelReading& RSSILevelReading::get_nanopb_message() const {
    return *this;
}

}  // namespace sensor

}  // namespace device

Client::Client(mmcc::AllocableStack& allocator) :
    _store(buttplug_Client_init_default), _allocator(allocator) {
    _store.cb_Union.funcs.decode = _decode_client_msg;
    _store.cb_Union.arg = this;
    _msg = nullptr;
}
size_t Client::decode(uint8_t* data, size_t len) {
    pb_istream_t stream = pb_istream_from_buffer(data, len);
    bool decoded = pb_decode_ex(&stream, buttplug_Client_fields, &_store,
                                PB_DECODE_NOINIT | PB_DECODE_DELIMITED);
    if (decoded) {
        return len - stream.bytes_left;
    }
    else {
        return 0;
    }
}
Client::Message Client::which() const {
    return (Client::Message) _store.which_Union;
}
const status::Ping& Client::get_ping() const {
    if (_msg == nullptr) {
        std::abort();
    }
    return *((status::Ping*) _msg);
}
const device::generic::StopDeviceCmd& Client::get_stop_device_cmd() const {
    if (_msg == nullptr) {
        std::abort();
    }
    return *((device::generic::StopDeviceCmd*) _msg);
}
const device::generic::StopAllDevices& Client::get_stop_all_devices() const {
    if (_msg == nullptr) {
        std::abort();
    }
    return *((device::generic::StopAllDevices*) _msg);
}
const device::generic::VibrateCmd& Client::get_vibrate_cmd() const {
    if (_msg == nullptr) {
        std::abort();
    }
    return *((device::generic::VibrateCmd*) _msg);
}
const device::generic::LinearCmd& Client::get_linear_cmd() const {
    if (_msg == nullptr) {
        std::abort();
    }
    return *((device::generic::LinearCmd*) _msg);
}
const device::generic::RotateCmd& Client::get_rotate_cmd() const {
    if (_msg == nullptr) {
        std::abort();
    }
    return *((device::generic::RotateCmd*) _msg);
}
const device::sensor::BatteryLevelCmd& Client::get_battery_level_cmd() const {
    if (_msg == nullptr) {
        std::abort();
    }
    return *((device::sensor::BatteryLevelCmd*) _msg);
}
const device::sensor::RSSILevelCmd& Client::get_rssi_level_cmd() const {
    if (_msg == nullptr) {
        std::abort();
    }
    return *((device::sensor::RSSILevelCmd*) _msg);
}
const device::raw::RawWriteCmd& Client::get_raw_write_cmd() const {
    if (_msg == nullptr) {
        std::abort();
    }
    return *((device::raw::RawWriteCmd*) _msg);
}
const device::raw::RawReadCmd& Client::get_raw_read_cmd() const {
    if (_msg == nullptr) {
        std::abort();
    }
    return *((device::raw::RawReadCmd*) _msg);
}
const handshake::RequestServerInfo& Client::get_request_server_info() const {
    if (_msg == nullptr) {
        std::abort();
    }
    return *((handshake::RequestServerInfo*) _msg);
}
const enumeration::StartScanning& Client::get_start_scanning() const {
    if (_msg == nullptr) {
        std::abort();
    }
    return *((enumeration::StartScanning*) _msg);
}
const enumeration::StopScanning& Client::get_stop_scanning() const {
    if (_msg == nullptr) {
        std::abort();
    }
    return *((enumeration::StopScanning*) _msg);
}
const enumeration::RequestDeviceList& Client::get_request_device_list() const {
    if (_msg == nullptr) {
        std::abort();
    }
    return *((enumeration::RequestDeviceList*) _msg);
}
const device::raw::RawSubscribeCmd& Client::get_raw_subscribe_cmd() const {
    if (_msg == nullptr) {
        std::abort();
    }
    return *((device::raw::RawSubscribeCmd*) _msg);
}
const device::raw::RawUnsubscribeCmd& Client::get_raw_unsubscribe_cmd() const {
    if (_msg == nullptr) {
        std::abort();
    }
    return *((device::raw::RawUnsubscribeCmd*) _msg);
}
bool Client::_decode_client_msg(pb_istream_t* stream, const pb_field_t* field, void** arg) {
    Client* obj = (Client*) *arg;
    if (obj->_msg != nullptr) {
        return false;
    }
    switch (field->tag) {
        case buttplug_Client_ping_tag: {
            obj->_msg = obj->_allocator.allocate(sizeof(status::Ping), alignof(status::Ping));
            status::Ping* mo = new (obj->_msg) status::Ping;
            return pb_decode(stream, buttplug_status_Ping_fields, &mo->get_nanopb_message());
        }
        case buttplug_Client_stop_device_cmd_tag: {
            obj->_msg = obj->_allocator.allocate(sizeof(device::generic::StopDeviceCmd),
                                                 alignof(device::generic::StopDeviceCmd));
            device::generic::StopDeviceCmd* mo = new (obj->_msg) device::generic::StopDeviceCmd;
            return pb_decode(stream, buttplug_device_generic_StopDeviceCmd_fields,
                             &mo->get_nanopb_message());
        }
        case buttplug_Client_stop_all_devices_tag: {
            obj->_msg = obj->_allocator.allocate(sizeof(device::generic::StopAllDevices),
                                                 alignof(device::generic::StopAllDevices));
            device::generic::StopAllDevices* mo =
                 new (obj->_msg) device::generic::StopAllDevices;
            return pb_decode(stream, buttplug_device_generic_StopAllDevices_fields,
                             &mo->get_nanopb_message());
        }
        case buttplug_Client_vibrate_cmd_tag: {
            obj->_msg = obj->_allocator.allocate(sizeof(device::generic::VibrateCmd),
                                                 alignof(device::generic::VibrateCmd));
            device::generic::VibrateCmd* mo =
                 new (obj->_msg) device::generic::VibrateCmd(obj->_allocator);
            return pb_decode(stream, buttplug_device_generic_VibrateCmd_fields,
                             &mo->get_nanopb_message());
        }
        case buttplug_Client_linear_cmd_tag: {
            obj->_msg = obj->_allocator.allocate(sizeof(device::generic::LinearCmd),
                                                 alignof(device::generic::LinearCmd));
            device::generic::LinearCmd* mo =
                 new (obj->_msg) device::generic::LinearCmd(obj->_allocator);
            return pb_decode(stream, buttplug_device_generic_LinearCmd_fields,
                             &mo->get_nanopb_message());
        }
        case buttplug_Client_rotate_cmd_tag: {
            obj->_msg = obj->_allocator.allocate(sizeof(device::generic::RotateCmd),
                                                 alignof(device::generic::RotateCmd));
            device::generic::RotateCmd* mo =
                 new (obj->_msg) device::generic::RotateCmd(obj->_allocator);
            return pb_decode(stream, buttplug_device_generic_RotateCmd_fields,
                             &mo->get_nanopb_message());
        }
        case buttplug_Client_battery_level_cmd_tag: {
            obj->_msg = obj->_allocator.allocate(sizeof(device::sensor::BatteryLevelCmd),
                                                 alignof(device::sensor::BatteryLevelCmd));
            device::sensor::BatteryLevelCmd* mo =
                 new (obj->_msg) device::sensor::BatteryLevelCmd;
            return pb_decode(stream, buttplug_device_sensor_BatteryLevelCmd_fields,
                             &mo->get_nanopb_message());
        }
        case buttplug_Client_rssi_level_cmd_tag: {
            obj->_msg = obj->_allocator.allocate(sizeof(device::sensor::RSSILevelCmd),
                                                 alignof(device::sensor::RSSILevelCmd));
            device::sensor::RSSILevelCmd* mo = new (obj->_msg) device::sensor::RSSILevelCmd;
            return pb_decode(stream, buttplug_device_sensor_RSSILevelCmd_fields,
                             &mo->get_nanopb_message());
        }
        case buttplug_Client_raw_write_cmd_tag: {
            obj->_msg = obj->_allocator.allocate(sizeof(device::raw::RawWriteCmd),
                                                 alignof(device::raw::RawWriteCmd));
            device::raw::RawWriteCmd* mo =
                 new (obj->_msg) device::raw::RawWriteCmd(obj->_allocator);
            return pb_decode(stream, buttplug_device_raw_RawWriteCmd_fields,
                             &mo->get_nanopb_message());
        }
        case buttplug_Client_raw_read_cmd_tag: {
            obj->_msg = obj->_allocator.allocate(sizeof(device::raw::RawReadCmd),
                                                 alignof(device::raw::RawReadCmd));
            device::raw::RawReadCmd* mo =
                 new (obj->_msg) device::raw::RawReadCmd(obj->_allocator);
            return pb_decode(stream, buttplug_device_raw_RawReadCmd_fields,
                             &mo->get_nanopb_message());
        }
        case buttplug_Client_request_server_info_tag: {
            obj->_msg = obj->_allocator.allocate(sizeof(handshake::RequestServerInfo),
                                                 alignof(handshake::RequestServerInfo));
            handshake::RequestServerInfo* mo =
                 new (obj->_msg) handshake::RequestServerInfo(obj->_allocator);
            return pb_decode(stream, buttplug_handshake_RequestServerInfo_fields,
                             &mo->get_nanopb_message());
        }
        case buttplug_Client_start_scanning_tag: {
            obj->_msg = obj->_allocator.allocate(sizeof(enumeration::StartScanning),
                                                 alignof(enumeration::StartScanning));
            enumeration::StartScanning* mo = new (obj->_msg) enumeration::StartScanning;
            return pb_decode(stream, buttplug_enumeration_StartScanning_fields,
                             &mo->get_nanopb_message());
        }
        case buttplug_Client_stop_scanning_tag: {
            obj->_msg = obj->_allocator.allocate(sizeof(enumeration::StopScanning),
                                                 alignof(enumeration::StopScanning));
            enumeration::StopScanning* mo = new (obj->_msg) enumeration::StopScanning;
            return pb_decode(stream, buttplug_enumeration_StopScanning_fields,
                             &mo->get_nanopb_message());
        }
        case buttplug_Client_request_device_list_tag: {
            obj->_msg = obj->_allocator.allocate(sizeof(enumeration::RequestDeviceList),
                                                 alignof(enumeration::RequestDeviceList));
            enumeration::RequestDeviceList* mo = new (obj->_msg) enumeration::RequestDeviceList;
            return pb_decode(stream, buttplug_enumeration_RequestDeviceList_fields,
                             &mo->get_nanopb_message());
        }
        case buttplug_Client_raw_subscribe_cmd_tag: {
            obj->_msg = obj->_allocator.allocate(sizeof(device::raw::RawSubscribeCmd),
                                                 alignof(device::raw::RawSubscribeCmd));
            device::raw::RawSubscribeCmd* mo =
                 new (obj->_msg) device::raw::RawSubscribeCmd(obj->_allocator);
            return pb_decode(stream, buttplug_device_raw_RawSubscribeCmd_fields,
                             &mo->get_nanopb_message());
        }
        case buttplug_Client_raw_unsubscribe_cmd_tag: {
            obj->_msg = obj->_allocator.allocate(sizeof(device::raw::RawUnsubscribeCmd),
                                                 alignof(device::raw::RawUnsubscribeCmd));
            device::raw::RawUnsubscribeCmd* mo =
                 new (obj->_msg) device::raw::RawUnsubscribeCmd(obj->_allocator);
            return pb_decode(stream, buttplug_device_raw_RawUnsubscribeCmd_fields,
                             &mo->get_nanopb_message());
        }
        default: {
            return false;
        }
    }
}

Server::Server() : _store(buttplug_Server_init_default) {}
void Server::set_ok(const status::Ok& msg) {
    _store.which_Union = Message::Ok;
    _store.ok = msg.get_nanopb_message();
}
void Server::set_device_added(const enumeration::DeviceAdded& msg) {
    _store.which_Union = Message::DeviceAdded;
    _store.device_added = msg.get_nanopb_message();
}
void Server::set_device_removed(const enumeration::DeviceRemoved& msg) {
    _store.which_Union = Message::DeviceRemoved;
    _store.device_removed = msg.get_nanopb_message();
}
void Server::set_rssi_level_reading(const device::sensor::RSSILevelReading& msg) {
    _store.which_Union = Message::RSSILevelReading;
    _store.rssi_level_reading = msg.get_nanopb_message();
}
void Server::set_battery_level_reading(const device::sensor::BatteryLevelReading& msg) {
    _store.which_Union = Message::BatteryLevelReading;
    _store.battery_level_reading = msg.get_nanopb_message();
}
void Server::set_raw_reading(const device::raw::RawReading& msg) {
    _store.which_Union = Message::RawReading;
    _store.raw_reading = msg.get_nanopb_message();
}
void Server::set_error(const status::Error& msg) {
    _store.which_Union = Message::Error;
    _store.error = msg.get_nanopb_message();
}
void Server::set_server_info(const handshake::ServerInfo& msg) {
    _store.which_Union = Message::ServerInfo;
    _store.server_info = msg.get_nanopb_message();
}
void Server::set_scanning_finished(const enumeration::ScanningFinished& msg) {
    _store.which_Union = Message::ScanningFinished;
    _store.scanning_finished = msg.get_nanopb_message();
}
void Server::set_device_list(const enumeration::DeviceList& msg) {
    _store.which_Union = Message::DeviceList;
    _store.device_list = msg.get_nanopb_message();
}
size_t Server::encode(uint8_t* data, size_t len) const {
    pb_ostream_t stream = pb_ostream_from_buffer(data, len);
    pb_encode_ex(&stream, buttplug_Server_fields, &_store, PB_ENCODE_DELIMITED);
    return stream.bytes_written;
}

}  // namespace buttplug::message
