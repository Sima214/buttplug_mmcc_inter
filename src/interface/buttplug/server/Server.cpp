#include "Server.hpp"

#include <cstring>

#include <pb_decode.h>
#include <pb_encode.h>

namespace buttplug {

Server::Server() = default;
Server::~Server() = default;

void Server::on_hw_connect() {
    if (!_connected) {
        _read_buffer_len = 0;
        std::memset(_read_buffer, 0x0, sizeof(_read_buffer));
        _connected = true;
    }
}
void Server::on_hw_disconnect() {
    _connected = false;
}
void Server::on_hw_receive(void* data, size_t size) {
    if (_connected && (_read_buffer_len + size) <= sizeof(_read_buffer)) {
        // Append data to read buffer.
        std::memcpy(_read_buffer + _read_buffer_len, data, size);
        _read_buffer_len += size;

        // Try to decode message.
        _message_heap.reset();
        buttplug::message::Client obj(_message_heap);
        size_t read = obj.decode(_read_buffer, _read_buffer_len);

        if (read) {
            // Consume from read buffer.
            _read_buffer_len -= read;
            std::memmove(_read_buffer, _read_buffer + read, _read_buffer_len);

            // Call event handler.
            switch (obj.which()) {
                case buttplug::message::Client::Message::Ping: return on_ping(obj.get_ping());
                case buttplug::message::Client::Message::RequestServerInfo:
                    return on_request_server_info(obj.get_request_server_info());
                case buttplug::message::Client::Message::StartScanning:
                    return on_start_scanning(obj.get_start_scanning());
                case buttplug::message::Client::Message::StopScanning:
                    return on_stop_scanning(obj.get_stop_scanning());
                case buttplug::message::Client::Message::RequestDeviceList:
                    return on_request_device_list(obj.get_request_device_list());
                case buttplug::message::Client::Message::RawWriteCMD:
                    return on_raw_write_cmd(obj.get_raw_write_cmd());
                case buttplug::message::Client::Message::RawReadCMD:
                    return on_raw_read_cmd(obj.get_raw_read_cmd());
                case buttplug::message::Client::Message::RawSubscribeCMD:
                    return on_raw_subscribe_cmd(obj.get_raw_subscribe_cmd());
                case buttplug::message::Client::Message::RawUnsubscribeCMD:
                    return on_raw_unsubscribe_cmd(obj.get_raw_unsubscribe_cmd());
                case buttplug::message::Client::Message::StopDeviceCMD:
                    return on_stop_device_cmd(obj.get_stop_device_cmd());
                case buttplug::message::Client::Message::StopAllDevices:
                    return on_stop_all_devices(obj.get_stop_all_devices());
                case buttplug::message::Client::Message::VibrateCMD:
                    return on_vibrate_cmd(obj.get_vibrate_cmd());
                case buttplug::message::Client::Message::LinearCMD:
                    return on_linear_cmd(obj.get_linear_cmd());
                case buttplug::message::Client::Message::RotateCMD:
                    return on_rotate_cmd(obj.get_rotate_cmd());
                case buttplug::message::Client::Message::BatteryLevelCMD:
                    return on_battery_level_cmd(obj.get_battery_level_cmd());
                case buttplug::message::Client::Message::RssiLevelCMD:
                    return on_rssi_level_cmd(obj.get_rssi_level_cmd());
            }
        }
    }
}

void Server::send_ok(const buttplug::message::status::Ok& msg) {
    buttplug::message::Server obj;
    obj.set_ok(msg);
    size_t obj_len = obj.encode(_write_buffer, sizeof(_write_buffer));
    on_hw_send(_write_buffer, obj_len);
}
void Server::send_error(const buttplug::message::status::Error& msg) {
    buttplug::message::Server obj;
    obj.set_error(msg);
    size_t obj_len = obj.encode(_write_buffer, sizeof(_write_buffer));
    on_hw_send(_write_buffer, obj_len);
}
void Server::send_server_info(const buttplug::message::handshake::ServerInfo& msg) {
    buttplug::message::Server obj;
    obj.set_server_info(msg);
    size_t obj_len = obj.encode(_write_buffer, sizeof(_write_buffer));
    on_hw_send(_write_buffer, obj_len);
}
void Server::send_scanning_finished(
     const buttplug::message::enumeration::ScanningFinished& msg) {
    buttplug::message::Server obj;
    obj.set_scanning_finished(msg);
    size_t obj_len = obj.encode(_write_buffer, sizeof(_write_buffer));
    on_hw_send(_write_buffer, obj_len);
}
void Server::send_device_list(const buttplug::message::enumeration::DeviceList& msg) {
    buttplug::message::Server obj;
    obj.set_device_list(msg);
    size_t obj_len = obj.encode(_write_buffer, sizeof(_write_buffer));
    on_hw_send(_write_buffer, obj_len);
}
void Server::send_device_added(const buttplug::message::enumeration::DeviceAdded& msg) {
    buttplug::message::Server obj;
    obj.set_device_added(msg);
    size_t obj_len = obj.encode(_write_buffer, sizeof(_write_buffer));
    on_hw_send(_write_buffer, obj_len);
}
void Server::send_device_removed(const buttplug::message::enumeration::DeviceRemoved& msg) {
    buttplug::message::Server obj;
    obj.set_device_removed(msg);
    size_t obj_len = obj.encode(_write_buffer, sizeof(_write_buffer));
    on_hw_send(_write_buffer, obj_len);
}
void Server::send_raw_reading(const buttplug::message::device::raw::RawReading& msg) {
    buttplug::message::Server obj;
    obj.set_raw_reading(msg);
    size_t obj_len = obj.encode(_write_buffer, sizeof(_write_buffer));
    on_hw_send(_write_buffer, obj_len);
}
void Server::send_battery_level_reading(
     const buttplug::message::device::sensor::BatteryLevelReading& msg) {
    buttplug::message::Server obj;
    obj.set_battery_level_reading(msg);
    size_t obj_len = obj.encode(_write_buffer, sizeof(_write_buffer));
    on_hw_send(_write_buffer, obj_len);
}
void Server::send_rssi_level_reading(
     const buttplug::message::device::sensor::RSSILevelReading& msg) {
    buttplug::message::Server obj;
    obj.set_rssi_level_reading(msg);
    size_t obj_len = obj.encode(_write_buffer, sizeof(_write_buffer));
    on_hw_send(_write_buffer, obj_len);
}

}  // namespace buttplug
