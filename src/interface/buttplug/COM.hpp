#ifndef MMCC_COM_HPP
#define MMCC_COM_HPP

#include <interface/RTOSTask.hpp>
#include <interface/RTOSBuffers.hpp>
#include <interface/buttplug/server/Server.hpp>
#include <interface/io/GPIO.hpp>
#include <interface/io/USART.hpp>
#include <interface/io/USARTHandlers.hpp>
#include <interface/io/USB.hpp>

#include <cstdint>

namespace buttplug {

class USBTask final : public rtos::StaticTask<756>, public Server {
   public:
    
    static constexpr uint32_t NOTIFICATION_CONNECT_MASK = 0b0001;
    static constexpr uint32_t NOTIFICATION_DATA_RECEIVED_MASK = 0b0010;
    static constexpr uint32_t NOTIFICATION_DISCONNECT_MASK = 0b0100;
    static constexpr uint32_t NOTIFICATION_DATA_SENT_MASK = 0b1000;

   protected:
    uint8_t usb_control_buffer[128];
    const stm32::USB::DeviceDescriptor _dev;
    const stm32::USB::EndpointDescriptor _comm_endp[1];
    const stm32::USB::EndpointDescriptor _data_endp[2];
    const stm32::USB::CDCACMFunctionalDescriptors _comm_desc;
    const stm32::USB::InterfaceDescriptor _comm_iface[1];
    const stm32::USB::InterfaceDescriptor _data_iface[1];
    const stm32::USB::ConfigDescriptor::Interface _iface[2];
    const stm32::USB::ConfigDescriptor _config;
    const char* _strings[3];

    rtos::StaticStreamBuffer<256> _receive_buffer;

   public:
    USBTask();
    ~USBTask() = default;

    void on_set_config(stm32::USB& usb, uint16_t v);

    usbd_request_return_codes cdcacm_control_request(stm32::USB&, usb_setup_data* req,
                                                     uint8_t**, uint16_t* len,
                                                     usbd_control_complete_callback*);

    void on_data_received(stm32::USB&, uint8_t ep);
    void on_data_sent(stm32::USB&, uint8_t ep);

    static void on_interrupt(stm32::USB& usb);
    static void on_set_config(usbd_device*, uint16_t v);
    static void on_data_received(usbd_device*, uint8_t ep);
    static void on_data_sent(usbd_device*, uint8_t ep);

    static usbd_request_return_codes cdcacm_control_request(
         usbd_device*, usb_setup_data* req, uint8_t** buf, uint16_t* len,
         usbd_control_complete_callback* complete);

    void run() override;
    void on_config_complete();

    virtual void on_ping(const message::status::Ping& msg) override;
    virtual void on_request_server_info(
         const message::handshake::RequestServerInfo& msg) override;
    virtual void on_start_scanning(const message::enumeration::StartScanning& msg) override;
    virtual void on_stop_scanning(const message::enumeration::StopScanning& msg) override;
    virtual void on_request_device_list(
         const message::enumeration::RequestDeviceList& msg) override;
    virtual void on_raw_write_cmd(const message::device::raw::RawWriteCmd& msg) override;
    virtual void on_raw_read_cmd(const message::device::raw::RawReadCmd& msg) override;
    virtual void on_raw_subscribe_cmd(
         const message::device::raw::RawSubscribeCmd& msg) override;
    virtual void on_raw_unsubscribe_cmd(
         const message::device::raw::RawUnsubscribeCmd& msg) override;
    virtual void on_stop_device_cmd(
         const message::device::generic::StopDeviceCmd& msg) override;
    virtual void on_stop_all_devices(
         const message::device::generic::StopAllDevices& msg) override;
    virtual void on_vibrate_cmd(const message::device::generic::VibrateCmd& msg) override;
    virtual void on_linear_cmd(const message::device::generic::LinearCmd& msg) override;
    virtual void on_rotate_cmd(const message::device::generic::RotateCmd& msg) override;
    virtual void on_battery_level_cmd(
         const message::device::sensor::BatteryLevelCmd& msg) override;
    virtual void on_rssi_level_cmd(const message::device::sensor::RSSILevelCmd& msg) override;

    virtual void on_hw_send(void* data, size_t size) override;
};

}  // namespace buttplug

#endif /*MMCC_COM_HPP*/