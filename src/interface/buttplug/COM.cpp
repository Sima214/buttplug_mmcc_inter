#include "COM.hpp"

#include <Objects.hpp>
#include <interface/RTOS.hpp>

#include <algorithm>
#include <cstddef>
#include <cstdlib>
#include <optional>

namespace buttplug {

USBTask::USBTask() :
    rtos::StaticTask<756>("usb", 4), _dev(stm32::USB::Class::CDC, 0x0483, 0x5740, 0x0200),
    _comm_endp{
         stm32::USB::EndpointDescriptor(0x81, stm32::USB::TransferType::Interrupt, 16, 255)},
    _data_endp{stm32::USB::EndpointDescriptor(0x02, stm32::USB::TransferType::Bulk, 64, 1),
               stm32::USB::EndpointDescriptor(0x82, stm32::USB::TransferType::Bulk, 64, 1)},
    _comm_desc(0, 1), _comm_iface{stm32::USB::InterfaceDescriptor(
                           0, 0, _comm_endp, stm32::USB::Class::CDC,
                           stm32::USB::CDCSubClass::ACM, stm32::USB::CDCProtocol::AT,
                           _comm_desc)},
    _data_iface{stm32::USB::InterfaceDescriptor(1, 0, _data_endp, stm32::USB::Class::DATA)},
    _iface{stm32::USB::ConfigDescriptor::Interface(_comm_iface),
           stm32::USB::ConfigDescriptor::Interface(_data_iface)},
    _config(_iface), _strings{"LewdSima", "CDC-ACM Buttplug Server", "STM32F411"},
    _receive_buffer(1) {}

void USBTask::on_set_config(stm32::USB& usb, uint16_t) {
    _data_endp[0].setup(usb, on_data_received);
    _data_endp[1].setup(usb, on_data_sent);
    _comm_endp[0].setup(usb);

    usb.register_control_callback(cdcacm_control_request,
                                  USB_REQ_TYPE_CLASS | USB_REQ_TYPE_INTERFACE,
                                  USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT);
}

usbd_request_return_codes USBTask::cdcacm_control_request(stm32::USB&, usb_setup_data* req,
                                                          uint8_t**, uint16_t* len,
                                                          usbd_control_complete_callback*) {
    switch (req->bRequest) {
        case USB_CDC_REQ_SET_CONTROL_LINE_STATE: {
            if (req->wValue) {
                notify(1, NotificationAction::eSetBits, NOTIFICATION_CONNECT_MASK);
            }
            else {
                notify(1, NotificationAction::eSetBits, NOTIFICATION_DISCONNECT_MASK);
            }
            return USBD_REQ_HANDLED;
        }
        case USB_CDC_REQ_SET_LINE_CODING:
            if (*len < sizeof(struct usb_cdc_line_coding)) {
                return USBD_REQ_NOTSUPP;
            }

            return USBD_REQ_HANDLED;
    }
    return USBD_REQ_NEXT_CALLBACK;
}

void USBTask::on_data_received(stm32::USB&, uint8_t) {
    char buf[64];
    uint16_t len = _data_endp[0].read(buf, sizeof(buf));
    if (len > 0) {
        uint16_t len_ok = _receive_buffer.send_from_isr(buf, len);
        if (len_ok != len) {
            // TODO: Make use of stm32's USB core auto nack-ing.
            std::abort();
        }
        notify(1, NotificationAction::eSetBits, NOTIFICATION_DATA_RECEIVED_MASK);
    }
}

void USBTask::on_data_sent(stm32::USB&, uint8_t) {
    notify(1, NotificationAction::eSetBits, NOTIFICATION_DATA_SENT_MASK);
}

void USBTask::on_interrupt(stm32::USB& usb) {
    usb.poll();
}

void USBTask::on_set_config(usbd_device*, uint16_t v) {
    stm32::USB& usb = stm32::USB::get_instance();
    USBTask* obj = static_cast<USBTask*>(usb.get_user_pointer());
    if (obj != nullptr) {
        obj->on_set_config(usb, v);
    }
}

void USBTask::on_data_received(usbd_device*, uint8_t ep) {
    stm32::USB& usb = stm32::USB::get_instance();
    USBTask* obj = static_cast<USBTask*>(usb.get_user_pointer());
    if (obj != nullptr) {
        obj->on_data_received(usb, ep);
    }
}

void USBTask::on_data_sent(usbd_device*, uint8_t ep) {
    stm32::USB& usb = stm32::USB::get_instance();
    USBTask* obj = static_cast<USBTask*>(usb.get_user_pointer());
    if (obj != nullptr) {
        obj->on_data_sent(usb, ep);
    }
}

usbd_request_return_codes USBTask::cdcacm_control_request(
     usbd_device*, usb_setup_data* req, uint8_t** buf, uint16_t* len,
     usbd_control_complete_callback* complete) {
    stm32::USB& usb = stm32::USB::get_instance();
    USBTask* obj = static_cast<USBTask*>(usb.get_user_pointer());
    if (obj != nullptr) {
        return obj->cdcacm_control_request(usb, req, buf, len, complete);
    }
    return USBD_REQ_NOTSUPP;
}

void USBTask::on_config_complete() {
    stm32::USB& usb = stm32::USB::get_instance();
    usb.set_user_pointer(this);
    usb.register_interrupt_callback(on_interrupt);
    nvic_set_priority(NVIC_OTG_FS_IRQ, 0xc0);
    nvic_enable_irq(NVIC_OTG_FS_IRQ);
    usb.init(_dev, _config, std::make_pair(_strings, 3),
             std::make_pair(usb_control_buffer, sizeof(usb_control_buffer)));
    usb.register_config_callback(on_set_config);
    mmcc::cli_task.print("USB setup complete!\n");
}

void USBTask::run() {
    while (true) {
        // Wait for an event.
        const uint32_t NOTIFICATION_MASK = NOTIFICATION_CONNECT_MASK |
                                           NOTIFICATION_DISCONNECT_MASK |
                                           NOTIFICATION_DATA_RECEIVED_MASK;
        auto state = notify_wait(1, 0x0, NOTIFICATION_MASK, 1000);
        if (state.first) {
            if (state.second & NOTIFICATION_CONNECT_MASK) {
                on_hw_connect();
            }
            if (state.second & NOTIFICATION_DISCONNECT_MASK) {
                on_hw_disconnect();
            }
            if (state.second & NOTIFICATION_DATA_RECEIVED_MASK) {
                constexpr size_t cpybuf_size = 32;
                uint8_t cpybuf[cpybuf_size];
                size_t cpybuf_len = _receive_buffer.receive(cpybuf, cpybuf_size);
                on_hw_receive(cpybuf, cpybuf_len);
            }
        }
    }
}

void USBTask::on_ping(const message::status::Ping& msg) {
    message::status::Ok rsp;
    rsp.set_id(msg.get_id());
    send_ok(rsp);
}
void USBTask::on_request_server_info(const message::handshake::RequestServerInfo& msg) {
    message::handshake::ServerInfo rsp;
    rsp.set_id(msg.get_id());
    rsp.set_server_name("STM32F4");
    rsp.set_message_version(2);
    rsp.set_max_ping_time(64);
    send_server_info(rsp);
}
void USBTask::on_start_scanning(const message::enumeration::StartScanning& msg) {
    message::status::Ok rsp;
    rsp.set_id(msg.get_id());
    send_ok(rsp);
}
void USBTask::on_stop_scanning(const message::enumeration::StopScanning& msg) {
    message::status::Ok rsp;
    rsp.set_id(msg.get_id());
    send_ok(rsp);
}
void USBTask::on_request_device_list(const message::enumeration::RequestDeviceList& msg) {
    const message::enumeration::Device::MessageAttributeEntry linear_device_cmds[] = {
         message::enumeration::Device::MessageAttributeEntry(
              message::enumeration::Device::MessageAttributeEntry::Command::StopDeviceCmd, 0,
              nullptr, 0),
         message::enumeration::Device::MessageAttributeEntry(
              message::enumeration::Device::MessageAttributeEntry::Command::LinearCmd, 1,
              nullptr, 0),
    };
    const message::enumeration::Device::MessageAttributeEntry nipple_device_cmds[] = {
         message::enumeration::Device::MessageAttributeEntry(
              message::enumeration::Device::MessageAttributeEntry::Command::StopDeviceCmd, 0,
              nullptr, 0),
         message::enumeration::Device::MessageAttributeEntry(
              message::enumeration::Device::MessageAttributeEntry::Command::RotateCmd, 1,
              nullptr, 0),
    };
    const message::enumeration::Device devices[] = {
         message::enumeration::Device(1, "Launch", linear_device_cmds),
         message::enumeration::Device(2, "Vorze UFO SA", nipple_device_cmds)};

    message::enumeration::DeviceList rsp;
    rsp.set_id(msg.get_id());
    rsp.set_devices(devices, 2);
    send_device_list(rsp);
}
void USBTask::on_raw_write_cmd(const message::device::raw::RawWriteCmd& msg) {
    message::status::Error rsp;
    rsp.set_id(msg.get_id());
    rsp.set_error_message("Unsupported cmd!");
    rsp.set_error_code(message::status::Error::Code::Msg);
    send_error(rsp);
}
void USBTask::on_raw_read_cmd(const message::device::raw::RawReadCmd& msg) {
    message::status::Error rsp;
    rsp.set_id(msg.get_id());
    rsp.set_error_message("Unsupported cmd!");
    rsp.set_error_code(message::status::Error::Code::Msg);
    send_error(rsp);
}
void USBTask::on_raw_subscribe_cmd(const message::device::raw::RawSubscribeCmd& msg) {
    message::status::Error rsp;
    rsp.set_id(msg.get_id());
    rsp.set_error_message("Unsupported cmd!");
    rsp.set_error_code(message::status::Error::Code::Msg);
    send_error(rsp);
}
void USBTask::on_raw_unsubscribe_cmd(const message::device::raw::RawUnsubscribeCmd& msg) {
    message::status::Error rsp;
    rsp.set_id(msg.get_id());
    rsp.set_error_message("Unsupported cmd!");
    rsp.set_error_code(message::status::Error::Code::Msg);
    send_error(rsp);
}
void USBTask::on_stop_device_cmd(const message::device::generic::StopDeviceCmd& msg) {
    message::status::Ok rsp;
    rsp.set_id(msg.get_id());
    send_ok(rsp);
    mmcc::actuator_task.set_actuation(true, 0, 0);
}
void USBTask::on_stop_all_devices(const message::device::generic::StopAllDevices& msg) {
    mmcc::actuator_task.set_actuation(true, 0, 0);
    message::status::Ok rsp;
    rsp.set_id(msg.get_id());
    send_ok(rsp);
}
void USBTask::on_vibrate_cmd(const message::device::generic::VibrateCmd& msg) {
    message::status::Error rsp;
    rsp.set_id(msg.get_id());
    rsp.set_error_message("Unsupported cmd!");
    rsp.set_error_code(message::status::Error::Code::Msg);
    send_error(rsp);
}
void USBTask::on_linear_cmd(const message::device::generic::LinearCmd& msg) {
    if (msg.get_vectors().second != 1) {
        message::status::Error rsp;
        rsp.set_id(msg.get_id());
        rsp.set_error_message("Unsupported cmd!");
        rsp.set_error_code(message::status::Error::Code::Msg);
        send_error(rsp);
    }
    else {
        auto vecs = msg.get_vectors();
        mmcc::actuator_task.set_actuation(false, vecs.first[0].get_position(),
                                          vecs.first[0].get_duration());

        message::status::Ok rsp;
        rsp.set_id(msg.get_id());
        send_ok(rsp);
    }
}
void USBTask::on_rotate_cmd(const message::device::generic::RotateCmd& msg) {
    // message::status::Error rsp;
    // rsp.set_id(msg.get_id());
    // rsp.set_error_message("Unsupported cmd!");
    // rsp.set_error_code(message::status::Error::Code::Msg);
    // send_error(rsp);
    message::status::Ok rsp;
    rsp.set_id(msg.get_id());
    send_ok(rsp);
}
void USBTask::on_battery_level_cmd(const message::device::sensor::BatteryLevelCmd& msg) {
    message::status::Error rsp;
    rsp.set_id(msg.get_id());
    rsp.set_error_message("Unsupported cmd!");
    rsp.set_error_code(message::status::Error::Code::Msg);
    send_error(rsp);
}
void USBTask::on_rssi_level_cmd(const message::device::sensor::RSSILevelCmd& msg) {
    message::status::Error rsp;
    rsp.set_id(msg.get_id());
    rsp.set_error_message("Unsupported cmd!");
    rsp.set_error_code(message::status::Error::Code::Msg);
    send_error(rsp);
}

void USBTask::on_hw_send(void* data, size_t size) {
    while (size != 0) {
        size_t packet_size = std::min(size, (size_t) _data_endp[1].wMaxPacketSize);
        bool sent = _data_endp[1].write(data, packet_size);
        if (sent) {
            // Prepare next data/packet.
            data = (void*) (((uintptr_t) data) + packet_size);
            size -= packet_size;
        }
        else {
            // Wait until last packet has been sent or timeout occurs.
            notify_wait(1, 0x0, NOTIFICATION_DATA_SENT_MASK, 100);
        }
    }
}

}  // namespace buttplug