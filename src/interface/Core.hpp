#ifndef MMCC_CORE_HPP
#define MMCC_CORE_HPP

#include <cstdint>

extern "C" {
#include <core.h>
}

namespace mmcc {

inline void enable_interrupts() {
    ::mmcc_enable_interrupts();
}
inline void disable_interrupts() {
    ::mmcc_disable_interrupts();
}
inline void enable_faults() {
    ::mmcc_enable_faults();
}
inline void disable_faults() {
    ::mmcc_disable_faults();
}
inline bool is_masked_interrupts() {
    return ::mmcc_is_masked_interrupts();
}
inline bool is_masked_faults() {
    return ::mmcc_is_masked_faults();
}
inline uint32_t mask_interrupts(uint32_t mask) {
    return ::mmcc_mask_interrupts(mask);
}
inline uint32_t mask_faults(uint32_t mask) {
    return ::mmcc_mask_faults(mask);
}

inline void reset_system() {
    ::mmcc_reset_system();
};
inline void reset_core() {
    ::mmcc_reset_core();
};
inline void set_priority_grouping(uint32_t prigroup) {
    ::mmcc_set_priority_grouping(prigroup);
};

/**
 * Disables interrupts while this object is alive in the stack.
 */
class InterruptGuard {
   private:
    const uint32_t _old_mask;

   public:
    InterruptGuard() : _old_mask(mask_interrupts(true)) {}
    ~InterruptGuard() {
        mask_interrupts(_old_mask);
    }

    InterruptGuard(InterruptGuard&) = delete;
    InterruptGuard& operator=(InterruptGuard&) = delete;
};

}  // namespace mmcc

#endif /*MMCC_CORE_HPP*/
