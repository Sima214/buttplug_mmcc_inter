#ifndef MMCC_CORE
#define MMCC_CORE

#include <stdbool.h>
#include <stdint.h>

void mmcc_enable_interrupts();
void mmcc_disable_interrupts();
void mmcc_enable_faults();
void mmcc_disable_faults();
bool mmcc_is_masked_interrupts();
bool mmcc_is_masked_faults();
uint32_t mmcc_mask_interrupts(uint32_t mask);
uint32_t mmcc_mask_faults(uint32_t mask);

void mmcc_reset_system() __attribute__((noreturn));
void mmcc_reset_core() __attribute__((noreturn));
void mmcc_set_priority_grouping(uint32_t prigroup);

#endif /*MMCC_CORE*/
