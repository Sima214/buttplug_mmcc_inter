#include "RTOSTask.hpp"

namespace rtos {

void iTask::delay(float ms) {
    TickType_t ticks = ms2ticks(ms);
    delay_ticks(ticks);
}

void iTask::delay_ticks(TickType_t ticks) {
    vTaskDelay(ticks);
}

void iTask::delay_ticks_until(TickType_t& previous_wake_time, TickType_t time_increment) {
    xTaskDelayUntil(&previous_wake_time, time_increment);
}

void iTask::set_priority(UBaseType_t prio) {
    vTaskPrioritySet(_handle, prio);
}
UBaseType_t iTask::get_priority() {
    return uxTaskPriorityGet(_handle);
}
void iTask::suspend() {
    vTaskSuspend(_handle);
}
void iTask::resume() {
    if (is_inside_interrupt()) {
        xTaskResumeFromISR(_handle);
    }
    else {
        vTaskResume(_handle);
    }
}
void iTask::stop() {
    vTaskDelete(_handle);
}
iTask::State iTask::get_state() {
    return (iTask::State) eTaskGetState(_handle);
}
const char* iTask::task_get_name() {
    return pcTaskGetName(_handle);
}

bool iTask::notify(UBaseType_t i, NotificationAction a, uint32_t v) {
    if (is_inside_interrupt()) {
        BaseType_t task_switch = 0;
        auto r = xTaskNotifyIndexedFromISR(_handle, i, v, a, &task_switch);
        if (task_switch) {
            taskYIELD();
        }
        return r == pdPASS;
    }
    else {
        return xTaskNotifyIndexed(_handle, i, v, a) == pdPASS;
    }
}
uint32_t iTask::notify_take(UBaseType_t i, bool c, float ms) {
    TickType_t ticks = ms2ticks(ms);
    return ulTaskNotifyTakeIndexed(i, c, ticks);
}
std::pair<bool, uint32_t> iTask::notify_and_query(UBaseType_t i, NotificationAction a,
                                                  uint32_t v) {
    if (is_inside_interrupt()) {
        BaseType_t task_switch = 0;
        uint32_t previous_value;
        auto r =
             xTaskNotifyAndQueryIndexedFromISR(_handle, i, v, a, &previous_value, &task_switch);
        if (task_switch) {
            taskYIELD();
        }
        return {r == pdPASS, previous_value};
    }
    else {
        uint32_t previous_value;
        auto r = xTaskNotifyAndQueryIndexed(_handle, i, v, a, &previous_value);
        return {r == pdPASS, previous_value};
    }
}
std::pair<bool, uint32_t> iTask::notify_wait(UBaseType_t i, uint32_t cm_entry, uint32_t cm_exit,
                                             float ms) {
    TickType_t ticks = ms2ticks(ms);
    uint32_t value = 0;
    auto r = xTaskNotifyWaitIndexed(i, cm_entry, cm_exit, &value, ticks);
    return {r == pdPASS, value};
}
bool iTask::notify_state_clear(UBaseType_t i) {
    return xTaskNotifyStateClearIndexed(_handle, i) == pdTRUE;
}
uint32_t iTask::notify_value_clear(UBaseType_t i, uint32_t cm) {
    return ulTaskNotifyValueClearIndexed(_handle, i, cm);
}

Task::~Task() {
    // If object has not been invalidated by a move copy.
    if (_handle != nullptr) {
        // Then mark rtos object task for deletion.
        stop();
    }
}

static TaskHandle_t _heap_task_create_wrapper(TaskFunction_t task_code, const char* const name,
                                              const configSTACK_DEPTH_TYPE stack_depth,
                                              void* const parameters, UBaseType_t priority) {
    TaskHandle_t handle = nullptr;
    BaseType_t status =
         xTaskCreate(task_code, name, stack_depth, parameters, priority, &handle);
    if (status != pdPASS) {
        return nullptr;
    }
    return handle;
}

void Task::_stack_run_bindings(void* arg) {
    Task* obj = static_cast<Task*>(arg);
    obj->run();
}

HeapTask::HeapTask(const char* const name, const uint32_t stack_size, UBaseType_t priority) :
    Task(_heap_task_create_wrapper(_stack_run_bindings, name, stack_size, this, priority)) {}

}  // namespace rtos
