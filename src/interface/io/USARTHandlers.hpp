#ifndef MMCC_USARTHANDLERS_HPP
#define MMCC_USARTHANDLERS_HPP

#include <USART.hpp>

namespace stm32 {

/**
 * Synchronous USART wrapper.
 */
class USARTSync {
   protected:
   USART& _hw;
    bool _caps_tx : 1;
    bool _caps_rx : 1;
    char _sep;

   public:
    /**
     * Initialize a new wrapper with optional seperator character.
     */
    USARTSync(USART& hw, char sep = '\0');

    /**
     * Send a null terminated string.
     */
    void send(const char*);
};

}  // namespace stm32

#endif /*MMCC_USARTHANDLERS_HPP*/