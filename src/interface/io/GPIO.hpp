#ifndef MMCC_GPIO_HPP
#define MMCC_GPIO_HPP

#include <interface/Core.hpp>
#include <utils/Class.hpp>

#include <cassert>
#include <cstdint>
#include <functional>

#include <libopencm3/stm32/gpio.h>

namespace stm32 {

typedef uint16_t pin_mask_t;

class iPort : public mmcc::INonCopyable {
   protected:
    const uintptr_t _port;
    pin_mask_t _locked_pins;

   public:
    iPort(uintptr_t port);

    iPort(iPort&& o) = delete;
    iPort& operator=(iPort&& o) = delete;

    uintptr_t get_port_addr() const {
        return _port;
    }

   private:
    void _unlock_pins(pin_mask_t mask);

    friend class Pin;
};

class Pin : public mmcc::INonCopyable {
   public:
    enum class Direction { Input, Output, AlternateFunction, Analog };
    enum class Speed { Low, Medium, High, VeryHigh };
    enum class Function {
        OutputPushPull,
        OutputOpenDrain,
        AlternateFunction00,
        AlternateFunction01,
        AlternateFunction02,
        AlternateFunction03,
        AlternateFunction04,
        AlternateFunction05,
        AlternateFunction06,
        AlternateFunction07,
        AlternateFunction08,
        AlternateFunction09,
        AlternateFunction10,
        AlternateFunction11,
        AlternateFunction12,
        AlternateFunction13,
        AlternateFunction14,
        AlternateFunction15
    };

   protected:
    std::reference_wrapper<iPort> _port;
    pin_mask_t _pins;

   public:
    Pin(iPort& port, pin_mask_t pins);
    ~Pin();

    Pin(Pin&& o);
    Pin& operator=(Pin&& o);

    bool is_valid();
    uint8_t get_pin_count();

    void set_direction(Direction);
    void set_function(Function);
    void set_speed(Speed);
    void set_pullres(bool up, bool down);

    pin_mask_t read();
    void write(pin_mask_t);

    void set();
    void clear();
    void toogle();
};

template<uintptr_t port> class Port : public iPort {
    static_assert(port == GPIOA || port == GPIOB || port == GPIOC || port == GPIOD ||
                       port == GPIOE || port == GPIOF || port == GPIOG || port == GPIOH ||
                       port == GPIOI || port == GPIOJ || port == GPIOK,
                  "Unknown GPIO port!");

   protected:
    Port();

   public:
    static Port<port>& get_port();
    ~Port();

    template<typename... Args> Pin get_pins(Args... pins) {
        // Convert pin index arguments to OpenCM gpio mask.
        pin_mask_t pins_mask = _get_pins_mask(pins...);
        {
            // Ensure atomicity.
            mmcc::InterruptGuard _g;
            // Filter only unlocked pins.
            pin_mask_t unlocked_pins = ~_locked_pins;
            pins_mask &= unlocked_pins;
            // Lock pins.
            _locked_pins |= pins_mask;
        }
        // Create and return Pin object.
        return Pin(*this, pins_mask);
    }

   private:
    template<typename... Args>
    static constexpr pin_mask_t _get_pins_mask(uint8_t pin, Args... more_pins) {
        pin_mask_t current_mask = _get_pin_mask(pin);
        return current_mask | _get_pins_mask(more_pins...);
    }
    template<> static constexpr pin_mask_t _get_pins_mask<>(uint8_t pin) {
        return _get_pin_mask(pin);
    }

    static constexpr pin_mask_t _get_pin_mask(uint8_t pin) {
        assert(pin < 16);
        switch (pin) {
            case 0: return GPIO0;
            case 1: return GPIO1;
            case 2: return GPIO2;
            case 3: return GPIO3;
            case 4: return GPIO4;
            case 5: return GPIO5;
            case 6: return GPIO6;
            case 7: return GPIO7;
            case 8: return GPIO8;
            case 9: return GPIO9;
            case 10: return GPIO10;
            case 11: return GPIO11;
            case 12: return GPIO12;
            case 13: return GPIO13;
            case 14: return GPIO14;
            case 15: return GPIO15;
        }
        return 0;
    }
};

}  // namespace stm32

#endif /*MMCC_GPIO_HPP*/
