#include "USART.hpp"

#include <interface/Core.hpp>

#include <libopencm3/stm32/rcc.h>

namespace stm32 {

constexpr rcc_periph_clken _prh2rcc(const uint32_t prh) {
    switch (prh) {
        case USART1: return RCC_USART1;
        case USART2: return RCC_USART2;
        case USART3: return RCC_USART3;
        case UART4: return RCC_UART4;
        case UART5: return RCC_UART5;
        case USART6: return RCC_USART6;
        case UART7: return RCC_UART7;
        case UART8: return RCC_UART8;
    }
    return RCC_USART1;
}

constexpr rcc_periph_rst _prh2rst(const uint32_t prh) {
    switch (prh) {
        case USART1: return RST_USART1;
        case USART2: return RST_USART2;
        case USART3: return RST_USART3;
        case UART4: return RST_UART4;
        case UART5: return RST_UART5;
        case USART6: return RST_USART6;
        case UART7: return RST_UART7;
        case UART8: return RST_UART8;
    }
    return RST_USART1;
}

constexpr int _prh2index(const uint32_t prh) {
    switch (prh) {
        case USART1: return 1;
        case USART2: return 2;
        case USART3: return 3;
        case UART4: return 4;
        case UART5: return 5;
        case USART6: return 6;
        case UART7: return 7;
        case UART8: return 8;
    }
    return -1;
}

uint8_t USART::_locked_prh = 0x0;

USART::USART(uint32_t prh) : _prh(prh), _valid(false) {
    int prh_i = _prh2index(prh);
    uint8_t prh_mask = 0x1 << (prh_i - 1);
    if (prh_i >= 0) {
        mmcc::InterruptGuard _g;
        if (!(_locked_prh & prh_mask)) {
            rcc_periph_reset_release(_prh2rst(prh));
            rcc_periph_clock_enable(_prh2rcc(prh));
            _locked_prh |= prh_mask;
            _valid = true;
        }
    }
}

USART::~USART() {
    if (_valid) {
        _valid = false;
        if (enabled()) {
            disable();
        }
        int prh_i = _prh2index(_prh);
        uint8_t prh_mask = 0x1 << (prh_i - 1);
        mmcc::InterruptGuard _g;
        rcc_periph_reset_hold(_prh2rst(_prh));
        rcc_periph_clock_disable(_prh2rcc(_prh));
        _locked_prh &= ~prh_mask;
    }
}

bool USART::is_valid() {
    return _valid;
}

void USART::set_baudrate(uint32_t baudrate) {
    if (_valid) {
        usart_set_baudrate(_prh, baudrate);
    }
}
void USART::set_databits(uint8_t bits) {
    if (_valid) {
        usart_set_databits(_prh, bits);
    }
}
void USART::set_stopbits(StopBits bits) {
    if (_valid) {
        usart_set_stopbits(_prh, bits);
    }
}
void USART::set_parity(Parity mode) {
    if (_valid) {
        usart_set_parity(_prh, mode);
    }
}
void USART::set_mode(bool tx, bool rx) {
    if (_valid) {
        uint32_t mode = 0x0;
        if (tx) {
            mode |= USART_MODE_TX;
        }
        if (rx) {
            mode |= USART_MODE_RX;
        }
        usart_set_mode(_prh, mode);
    }
}
void USART::set_flow_control(bool rts, bool cts) {
    if (_valid) {
        uint32_t mode = 0x0;
        if (rts) {
            mode |= USART_FLOWCONTROL_RTS;
        }
        if (cts) {
            mode |= USART_FLOWCONTROL_CTS;
        }
        usart_set_flow_control(_prh, mode);
    }
}
void USART::set_rx_dma(bool en) {
    if (_valid) {
        if (en) {
            usart_enable_rx_dma(_prh);
        }
        else {
            usart_disable_rx_dma(_prh);
        }
    }
}
void USART::set_tx_dma(bool en) {
    if (_valid) {
        if (en) {
            usart_enable_tx_dma(_prh);
        }
        else {
            usart_disable_tx_dma(_prh);
        }
    }
}
void USART::set_rx_interrupt(bool en) {
    if (_valid) {
        if (en) {
            usart_enable_rx_interrupt(_prh);
        }
        else {
            usart_disable_rx_interrupt(_prh);
        }
    }
}
void USART::set_tx_interrupt(bool en) {
    if (_valid) {
        if (en) {
            usart_enable_tx_interrupt(_prh);
        }
        else {
            usart_disable_tx_interrupt(_prh);
        }
    }
}
void USART::set_tx_complete_interrupt(bool en) {
    if (_valid) {
        if (en) {
            usart_enable_tx_complete_interrupt(_prh);
        }
        else {
            usart_disable_tx_complete_interrupt(_prh);
        }
    }
}
void USART::set_idle_interrupt(bool en) {
    if (_valid) {
        if (en) {
            usart_enable_idle_interrupt(_prh);
        }
        else {
            usart_disable_idle_interrupt(_prh);
        }
    }
}
void USART::set_error_interrupt(bool en) {
    if (_valid) {
        if (en) {
            usart_enable_error_interrupt(_prh);
        }
        else {
            usart_disable_error_interrupt(_prh);
        }
    }
}

uint32_t USART::get_baudrate() {
    uint32_t clock = rcc_apb1_frequency;
    if ((_prh == USART1) || (_prh == USART6)) {
        clock = rcc_apb2_frequency;
    }
    return clock / USART_BRR(_prh);
}
uint8_t USART::get_databits() {
    bool d9 = USART_CR1(_prh) & USART_CR1_M;
    return 9 ? d9 : 8;
}
typename USART::StopBits USART::get_stopbits() {
    return USART::StopBits(USART_CR2(_prh) & USART_CR2_STOPBITS_MASK);
}
typename USART::Parity USART::get_parity() {
    return USART::Parity(USART_CR1(_prh) & USART_PARITY_MASK);
}
bool USART::get_mode_tx() {
    return !!(USART_CR1(_prh) & USART_MODE_TX);
}
bool USART::get_mode_rx() {
    return !!(USART_CR1(_prh) & USART_MODE_RX);
}
bool USART::get_flow_control_rts() {
    return !!(USART_CR3(_prh) & USART_FLOWCONTROL_RTS);
}
bool USART::get_flow_control_cts() {
    return !!(USART_CR3(_prh) & USART_FLOWCONTROL_CTS);
}
bool USART::get_rx_dma() {
    return !!(USART_CR3(_prh) & USART_CR3_DMAR);
}
bool USART::get_tx_dma() {
    return !!(USART_CR3(_prh) & USART_CR3_DMAT);
}
bool USART::get_rx_interrupt() {
    return !!(USART_CR1(_prh) & USART_CR1_RXNEIE);
}
bool USART::get_tx_interrupt() {
    return !!(USART_CR1(_prh) & USART_CR1_TXEIE);
}
bool USART::get_tx_complete_interrupt() {
    return !!(USART_CR1(_prh) & USART_CR1_TCIE);
}
bool USART::get_idle_interrupt() {
    return !!(USART_CR1(_prh) & USART_CR1_IDLEIE);
}
bool USART::get_error_interrupt() {
    return !!(USART_CR3(_prh) & USART_CR3_EIE);
}

void USART::enable() {
    if (_valid) {
        usart_enable(_prh);
    }
}
void USART::disable() {
    if (_valid) {
        usart_disable(_prh);
    }
}
bool USART::enabled() {
    if (_valid) {
        return !!(USART_CR1(_prh) & USART_CR1_UE);
    }
    return false;
}

void USART::wait_tx() {
    if (enabled()) {
        usart_wait_send_ready(_prh);
    }
}
bool USART::tx_ready() {
    if (enabled()) {
        return !!(USART_SR(_prh) & USART_SR_TXE);
    }
    return false;
}
void USART::wait_rx() {
    if (enabled()) {
        usart_wait_recv_ready(_prh);
    }
}
bool USART::rx_ready() {
    if (enabled()) {
        return !!(USART_SR(_prh) & USART_SR_RXNE);
    }
    return false;
}

void USART::tx(uint16_t data, bool blocking) {
    if (enabled()) {
        if (blocking) {
            usart_wait_send_ready(_prh);
        }
        usart_send(_prh, data);
    }
}
uint16_t USART::rx(bool blocking) {
    if (enabled()) {
        if (blocking) {
            usart_wait_recv_ready(_prh);
        }
        return usart_recv(_prh);
    }
    return 0;
}

}  // namespace stm32
