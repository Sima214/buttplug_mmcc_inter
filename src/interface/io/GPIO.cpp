#include "GPIO.hpp"

#include <HammingWeight.hpp>

#include <cstdlib>
#include <utility>

#include <libopencm3/stm32/rcc.h>

namespace stm32 {

template<uintptr_t port> constexpr rcc_periph_clken _port2rcc();
template<> constexpr rcc_periph_clken _port2rcc<GPIOA>() {
    return RCC_GPIOA;
}
template<> constexpr rcc_periph_clken _port2rcc<GPIOB>() {
    return RCC_GPIOB;
}
template<> constexpr rcc_periph_clken _port2rcc<GPIOC>() {
    return RCC_GPIOC;
}
template<> constexpr rcc_periph_clken _port2rcc<GPIOD>() {
    return RCC_GPIOD;
}
template<> constexpr rcc_periph_clken _port2rcc<GPIOE>() {
    return RCC_GPIOE;
}
template<> constexpr rcc_periph_clken _port2rcc<GPIOF>() {
    return RCC_GPIOF;
}
template<> constexpr rcc_periph_clken _port2rcc<GPIOG>() {
    return RCC_GPIOG;
}
template<> constexpr rcc_periph_clken _port2rcc<GPIOH>() {
    return RCC_GPIOH;
}
template<> constexpr rcc_periph_clken _port2rcc<GPIOI>() {
    return RCC_GPIOI;
}
template<> constexpr rcc_periph_clken _port2rcc<GPIOJ>() {
    return RCC_GPIOJ;
}
template<> constexpr rcc_periph_clken _port2rcc<GPIOK>() {
    return RCC_GPIOK;
}

template<uintptr_t port> constexpr rcc_periph_rst _port2rst();
template<> constexpr rcc_periph_rst _port2rst<GPIOA>() {
    return RST_GPIOA;
}
template<> constexpr rcc_periph_rst _port2rst<GPIOB>() {
    return RST_GPIOB;
}
template<> constexpr rcc_periph_rst _port2rst<GPIOC>() {
    return RST_GPIOC;
}
template<> constexpr rcc_periph_rst _port2rst<GPIOD>() {
    return RST_GPIOD;
}
template<> constexpr rcc_periph_rst _port2rst<GPIOE>() {
    return RST_GPIOE;
}
template<> constexpr rcc_periph_rst _port2rst<GPIOF>() {
    return RST_GPIOF;
}
template<> constexpr rcc_periph_rst _port2rst<GPIOG>() {
    return RST_GPIOG;
}
template<> constexpr rcc_periph_rst _port2rst<GPIOH>() {
    return RST_GPIOH;
}
template<> constexpr rcc_periph_rst _port2rst<GPIOI>() {
    return RST_GPIOI;
}
template<> constexpr rcc_periph_rst _port2rst<GPIOJ>() {
    return RST_GPIOJ;
}
template<> constexpr rcc_periph_rst _port2rst<GPIOK>() {
    return RST_GPIOK;
}

iPort::iPort(uintptr_t port) : _port(port), _locked_pins(0x0) {}

void iPort::_unlock_pins(pin_mask_t mask) {
    _locked_pins &= ~mask;
}

Pin::Pin(iPort& port, pin_mask_t pins) : _port(port), _pins(pins) {}
Pin::~Pin() {
    if (_pins != 0) {
        _port.get()._unlock_pins(_pins);
        _pins = 0;
    }
}

Pin::Pin(Pin&& o) : _port(o._port), _pins(std::exchange(o._pins, 0)) {}
Pin& Pin::operator=(Pin&& o) {
    _port = o._port;
    _pins = std::exchange(o._pins, 0);
    return *this;
}

bool Pin::is_valid() {
    return _pins != 0;
}
uint8_t Pin::get_pin_count() {
    return mmcc::hamming_weight(_pins);
}

void Pin::set_direction(Direction mode) {
    uint8_t mode_bits;
    switch (mode) {
        case Direction::Input: {
            mode_bits = GPIO_MODE_INPUT;
        } break;
        case Direction::Output: {
            mode_bits = GPIO_MODE_OUTPUT;
        } break;
        case Direction::AlternateFunction: {
            mode_bits = GPIO_MODE_AF;
        } break;
        case Direction::Analog: {
            mode_bits = GPIO_MODE_ANALOG;
        } break;
        default: std::abort();
    }
    uint32_t port = _port.get().get_port_addr();
    {
        mmcc::InterruptGuard _g;
        uint32_t mode_reg = GPIO_MODER(port);
        for (int i = 0; i < 16; i++) {
            if (!((1 << i) & _pins)) {
                continue;
            }
            mode_reg &= ~GPIO_MODE_MASK(i);
            mode_reg |= GPIO_MODE(i, mode_bits);
        }
        GPIO_MODER(port) = mode_reg;
    }
}
void Pin::set_function(Function mode) {
    uint32_t port = _port.get().get_port_addr();
    if (mode == Function::OutputPushPull) {
        GPIO_OTYPER(port) &= ~_pins;
    }
    else if (mode == Function::OutputOpenDrain) {
        GPIO_OTYPER(port) |= _pins;
    }
    else {
        uint8_t af;
        switch (mode) {
            case Function::AlternateFunction00: {
                af = GPIO_AF0;
            } break;
            case Function::AlternateFunction01: {
                af = GPIO_AF1;
            } break;
            case Function::AlternateFunction02: {
                af = GPIO_AF2;
            } break;
            case Function::AlternateFunction03: {
                af = GPIO_AF3;
            } break;
            case Function::AlternateFunction04: {
                af = GPIO_AF4;
            } break;
            case Function::AlternateFunction05: {
                af = GPIO_AF5;
            } break;
            case Function::AlternateFunction06: {
                af = GPIO_AF6;
            } break;
            case Function::AlternateFunction07: {
                af = GPIO_AF7;
            } break;
            case Function::AlternateFunction08: {
                af = GPIO_AF8;
            } break;
            case Function::AlternateFunction09: {
                af = GPIO_AF9;
            } break;
            case Function::AlternateFunction10: {
                af = GPIO_AF10;
            } break;
            case Function::AlternateFunction11: {
                af = GPIO_AF11;
            } break;
            case Function::AlternateFunction12: {
                af = GPIO_AF12;
            } break;
            case Function::AlternateFunction13: {
                af = GPIO_AF13;
            } break;
            case Function::AlternateFunction14: {
                af = GPIO_AF14;
            } break;
            case Function::AlternateFunction15: {
                af = GPIO_AF15;
            } break;
            default: std::abort();
        }
        gpio_set_af(port, af, _pins);
    }
}
void Pin::set_speed(Speed mode) {
    uint8_t mode_bits;
    switch (mode) {
        case Speed::Low: {
            mode_bits = GPIO_OSPEED_2MHZ;
        } break;
        case Speed::Medium: {
            mode_bits = GPIO_OSPEED_25MHZ;
        } break;
        case Speed::High: {
            mode_bits = GPIO_OSPEED_50MHZ;
        } break;
        case Speed::VeryHigh: {
            mode_bits = GPIO_OSPEED_100MHZ;
        } break;
        default: std::abort();
    }
    uint32_t port = _port.get().get_port_addr();
    {
        mmcc::InterruptGuard _g;
        uint32_t mode_reg = GPIO_OSPEEDR(port);
        for (int i = 0; i < 16; i++) {
            if (!((1 << i) & _pins)) {
                continue;
            }
            mode_reg &= ~GPIO_OSPEED_MASK(i);
            mode_reg |= GPIO_OSPEED(i, mode_bits);
        }
        GPIO_OSPEEDR(port) = mode_reg;
    }
}
void Pin::set_pullres(bool up, bool down) {
    uint8_t mode = 0x0;
    if (up) {
        mode |= GPIO_PUPD_PULLUP;
    }
    if (down) {
        mode |= GPIO_PUPD_PULLDOWN;
    }
    uint32_t port = _port.get().get_port_addr();
    {
        mmcc::InterruptGuard _g;
        uint32_t mode_reg = GPIO_PUPDR(port);
        for (int i = 0; i < 16; i++) {
            if (!((1 << i) & _pins)) {
                continue;
            }
            mode_reg &= ~GPIO_PUPD_MASK(i);
            mode_reg |= GPIO_PUPD(i, mode);
        }
        GPIO_PUPDR(port) = mode_reg;
    }
}

pin_mask_t Pin::read() {
    return gpio_get(_port.get().get_port_addr(), _pins);
}
void Pin::write(pin_mask_t v) {
    gpio_port_write(_port.get().get_port_addr(), _pins & v);
}

void Pin::set() {
    gpio_set(_port.get().get_port_addr(), _pins);
}
void Pin::clear() {
    gpio_clear(_port.get().get_port_addr(), _pins);
}
void Pin::toogle() {
    gpio_toggle(_port.get().get_port_addr(), _pins);
}

template<uintptr_t port> Port<port>::Port() : iPort(port) {
    rcc_periph_reset_release(_port2rst<port>());
    rcc_periph_clock_enable(_port2rcc<port>());
}

template<uintptr_t port> Port<port>& Port<port>::get_port() {
    mmcc::InterruptGuard _g;
    static Port<port> _instance;
    return _instance;
}

template<uintptr_t port> Port<port>::~Port() {
    // Object is non-copyable and non-moveable.
    // This guarantees that the destructor
    // can be called only once.
    rcc_periph_reset_hold(_port2rst<port>());
    rcc_periph_clock_disable(_port2rcc<port>());
}

}  // namespace stm32

template class stm32::Port<GPIOA>;
template class stm32::Port<GPIOB>;
template class stm32::Port<GPIOC>;
template class stm32::Port<GPIOD>;
template class stm32::Port<GPIOE>;
template class stm32::Port<GPIOF>;
template class stm32::Port<GPIOG>;
template class stm32::Port<GPIOH>;
