#include "USB.hpp"

#include <cstdlib>

#include <boot.h>
#include <libopencm3/stm32/rcc.h>

namespace stm32 {

void USB::EndpointDescriptor::setup() const {
    stm32::USB& usb = stm32::USB::get_instance();
    setup(usb);
}
void USB::EndpointDescriptor::setup(usbd_endpoint_callback cb) const {
    stm32::USB& usb = stm32::USB::get_instance();
    setup(usb, cb);
}
uint16_t USB::EndpointDescriptor::read(void* buf, uint16_t buf_len) const {
    stm32::USB& usb = stm32::USB::get_instance();
    return read(usb, buf, buf_len);
}
bool USB::EndpointDescriptor::write(void* buf, uint16_t len) const {
    stm32::USB& usb = stm32::USB::get_instance();
    int status = usbd_ep_write_packet(usb._dev, bEndpointAddress, buf, len);
    return status != 0;
}
void USB::EndpointDescriptor::setup(stm32::USB& usb) const {
    usbd_ep_setup(usb._dev, bEndpointAddress, bmAttributes & 0b11, wMaxPacketSize, nullptr);
}
void USB::EndpointDescriptor::setup(stm32::USB& usb, usbd_endpoint_callback cb) const {
    usbd_ep_setup(usb._dev, bEndpointAddress, bmAttributes & 0b11, wMaxPacketSize, cb);
}
uint16_t USB::EndpointDescriptor::read(stm32::USB& usb, void* buf, uint16_t buf_len) const {
    return usbd_ep_read_packet(usb._dev, bEndpointAddress, buf, buf_len);
}
bool USB::EndpointDescriptor::write(stm32::USB& usb, void* buf, uint16_t len) const {
    int status = usbd_ep_write_packet(usb._dev, bEndpointAddress, buf, len);
    return status != 0;
}

USB::USB() :
    _dev(nullptr), _usrptr(nullptr), _inter_cb(nullptr),
    _pins(Port<GPIOA>::get_port().get_pins(11, 12)) {
    if (_pins.get_pin_count() != 2) {
        std::abort();
    }
    rcc_periph_clock_enable(RCC_OTGFS);
    _pins.set_direction(Pin::Direction::AlternateFunction);
    _pins.set_pullres(false, false);
    _pins.set_function(Pin::Function::AlternateFunction10);
}

USB& USB::get_instance() {
    static USB instance;
    return instance;
}

void USB::init(const DeviceDescriptor& dev, const ConfigDescriptor& conf,
               std::pair<const char* const*, uint16_t> strings,
               std::pair<uint8_t*, uint16_t> ctl_buf) {
    // Prepare for interrupts.
    runtime_vector_table.irq[NVIC_OTG_FS_IRQ] = on_usb_interrupt;
    // Initialize hidden state.
    _dev = usbd_init(&otgfs_usb_driver, &dev, &conf, strings.first, strings.second,
                     ctl_buf.first, ctl_buf.second);
}

void USB::poll() {
    usbd_poll(_dev);
}

void USB::register_interrupt_callback(usb_interrupt_callback* cb) {
    _inter_cb = cb;
}

void USB::register_reset_callback(usb_event_callback cb) {
    usbd_register_reset_callback(_dev, cb);
}
void USB::register_suspend_callback(usb_event_callback cb) {
    usbd_register_suspend_callback(_dev, cb);
}
void USB::register_resume_callback(usb_event_callback cb) {
    usbd_register_resume_callback(_dev, cb);
}
void USB::register_sof_callback(usb_event_callback cb) {
    usbd_register_sof_callback(_dev, cb);
}

void USB::register_config_callback(usbd_set_config_callback cb) {
    usbd_register_set_config_callback(_dev, cb);
}

void USB::register_control_callback(usbd_control_callback cb, uint8_t type, uint8_t type_mask) {
    usbd_register_control_callback(_dev, type, type_mask, cb);
}

void USB::register_set_altsetting_callback(usbd_set_altsetting_callback cb) {
    usbd_register_set_altsetting_callback(_dev, cb);
}

void USB::on_usb_interrupt() {
    stm32::USB& usb = stm32::USB::get_instance();
    auto cb = usb._inter_cb;
    if (cb != nullptr) {
        cb(usb);
    }
}

}  // namespace stm32
