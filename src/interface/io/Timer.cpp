#include "Timer.hpp"
#include "libopencm3/stm32/common/timer_common_all.h"
#include "libopencm3/stm32/common/timer_common_f24.h"

#include <libopencm3/stm32/rcc.h>
#include <stdint.h>

template<uintptr_t prh> constexpr rcc_periph_clken _prh2rcc();
template<> constexpr rcc_periph_clken _prh2rcc<TIM1>() {
    return RCC_TIM1;
}
template<> constexpr rcc_periph_clken _prh2rcc<TIM2>() {
    return RCC_TIM2;
}
template<> constexpr rcc_periph_clken _prh2rcc<TIM5>() {
    return RCC_TIM5;
}
template<> constexpr rcc_periph_clken _prh2rcc<TIM3>() {
    return RCC_TIM3;
}
template<> constexpr rcc_periph_clken _prh2rcc<TIM4>() {
    return RCC_TIM4;
}
template<> constexpr rcc_periph_clken _prh2rcc<TIM9>() {
    return RCC_TIM9;
}
template<> constexpr rcc_periph_clken _prh2rcc<TIM10>() {
    return RCC_TIM10;
}
template<> constexpr rcc_periph_clken _prh2rcc<TIM11>() {
    return RCC_TIM11;
}

template<uintptr_t prh> constexpr rcc_periph_rst _prh2rst();
template<> constexpr rcc_periph_rst _prh2rst<TIM1>() {
    return RST_TIM1;
}
template<> constexpr rcc_periph_rst _prh2rst<TIM2>() {
    return RST_TIM2;
}
template<> constexpr rcc_periph_rst _prh2rst<TIM5>() {
    return RST_TIM5;
}
template<> constexpr rcc_periph_rst _prh2rst<TIM3>() {
    return RST_TIM3;
}
template<> constexpr rcc_periph_rst _prh2rst<TIM4>() {
    return RST_TIM4;
}
template<> constexpr rcc_periph_rst _prh2rst<TIM9>() {
    return RST_TIM9;
}
template<> constexpr rcc_periph_rst _prh2rst<TIM10>() {
    return RST_TIM10;
}
template<> constexpr rcc_periph_rst _prh2rst<TIM11>() {
    return RST_TIM11;
}

constexpr tim_ic_psc _int2psc(uint8_t v) {
    switch (v) {
        case 1: return TIM_IC_PSC_OFF;
        case 2: return TIM_IC_PSC_2;
        case 4: return TIM_IC_PSC_4;
        case 8: return TIM_IC_PSC_8;
        default: return TIM_IC_PSC_OFF;
    }
}

namespace stm32::timer {

template<uintptr_t prh> Timer<prh>::Timer() {
    rcc_periph_reset_release(_prh2rst<prh>());
    rcc_periph_clock_enable(_prh2rcc<prh>());
}
template<uintptr_t prh> Timer<prh>::~Timer() {
    rcc_periph_reset_hold(_prh2rst<prh>());
    rcc_periph_clock_disable(_prh2rcc<prh>());
}
template<uintptr_t prh> Timer<prh>& Timer<prh>::get_instance() {
    static Timer<prh> _instance;
    return _instance;
}

template<uintptr_t prh> void Timer<prh>::generate_event(Event event) {
    timer_generate_event(prh, event);
}
template<uintptr_t prh> void Timer<prh>::enable() {
    timer_enable_counter(prh);
}
template<uintptr_t prh> void Timer<prh>::disable() {
    timer_disable_counter(prh);
}
template<uintptr_t prh> void Timer<prh>::reset() {
    rcc_periph_reset_pulse(_prh2rst<prh>());
}

template<uintptr_t prh>
void Timer<prh>::set_mode(CkIntDiv div, Alignment alignment, Direction direction) {
    timer_set_mode(prh, div, alignment, direction);
}
template<uintptr_t prh> void Timer<prh>::set_mode(CkIntDiv div) {
    timer_set_clock_division(prh, div);
}
template<uintptr_t prh> void Timer<prh>::set_mode(Alignment alignment) {
    timer_set_alignment(prh, alignment);
}
template<uintptr_t prh> void Timer<prh>::set_mode(Direction direction) {
    if (direction == Direction::Up) {
        timer_direction_up(prh);
    }
    else if (direction == Direction::Down) {
        timer_direction_down(prh);
    }
}
template<uintptr_t prh> void Timer<prh>::set_mode(RunMode mode) {
    if (mode == RunMode::ContinuousMode) {
        timer_continuous_mode(prh);
    }
    else if (mode == RunMode::OneShotMode) {
        timer_one_shot_mode(prh);
    }
}

template<uintptr_t prh> void Timer<prh>::set_master_mode(MasterMode mode) {
    timer_set_master_mode(prh, mode);
}
template<uintptr_t prh> void Timer<prh>::set_slave_filter(InputChannelFilter flt) {
    timer_slave_set_filter(prh, (tim_ic_filter) flt);
}
template<uintptr_t prh> void Timer<prh>::set_slave_prescaler(uint8_t psc) {
    timer_slave_set_prescaler(prh, _int2psc(psc));
}
template<uintptr_t prh> void Timer<prh>::set_slave_polarity(InputChannelPolarity pol) {
    timer_slave_set_polarity(prh, (tim_et_pol) pol);
}
template<uintptr_t prh> void Timer<prh>::set_slave_mode(SlaveMode mode) {
    timer_slave_set_mode(prh, (uint32_t) mode);
}
template<uintptr_t prh> void Timer<prh>::set_slave_trigger(SlaveTrigger trg) {
    timer_slave_set_trigger(prh, trg);
}

template<uintptr_t prh> void Timer<prh>::set_interrupt(uint32_t irq_mask, bool enable) {
    if (enable) {
        timer_enable_irq(prh, irq_mask);
    }
    else {
        timer_disable_irq(prh, irq_mask);
    }
}
template<uintptr_t prh> bool Timer<prh>::get_interrupt_flags(uint32_t flag, bool clear) {
    bool r = timer_get_flag(prh, flag);
    if (clear) {
        timer_clear_flag(prh, flag);
    }
    return r;
}
template<uintptr_t prh> void Timer<prh>::set_update_on(bool overflow_only) {
    if (overflow_only) {
        timer_update_on_overflow(prh);
    }
    else {
        timer_update_on_any(prh);
    }
}
template<uintptr_t prh> void Timer<prh>::set_update_enable(bool enable) {
    if (enable) {
        timer_enable_update_event(prh);
    }
    else {
        timer_disable_update_event(prh);
    }
}
template<uintptr_t prh> void Timer<prh>::set_dma_trigger(DMATrigger trg) {
    if (trg == DMATrigger::Compare) {
        timer_set_dma_on_compare_event(prh);
    }
    else if (trg == DMATrigger::Update) {
        timer_set_dma_on_update_event(prh);
    }
}

template<uintptr_t prh> void Timer<prh>::set_prescaler(uint32_t value) {
    timer_set_prescaler(prh, value);
}
template<uintptr_t prh> void Timer<prh>::set_period(uint32_t period) {
    timer_set_period(prh, period);
}
template<uintptr_t prh> uint32_t Timer<prh>::get_counter() {
    return timer_get_counter(prh);
}
template<uintptr_t prh> void Timer<prh>::set_counter(uint32_t count) {
    timer_set_counter(prh, count);
}
template<uintptr_t prh> void Timer<prh>::set_preload(bool enable) {
    if (enable) {
        timer_enable_preload(prh);
    }
    else {
        timer_disable_preload(prh);
    }
}

template<uintptr_t prh> void Timer<prh>::set_oc_clear(OutputChannel id, bool enable_clear) {
    if (enable_clear) {
        timer_enable_oc_clear(prh, (tim_oc_id) id);
    }
    else {
        timer_disable_oc_clear(prh, (tim_oc_id) id);
    }
}
template<uintptr_t prh> void Timer<prh>::set_oc_speed(OutputChannel id, bool fast_mode_enable) {
    if (fast_mode_enable) {
        timer_set_oc_fast_mode(prh, (tim_oc_id) id);
    }
    else {
        timer_set_oc_slow_mode(prh, (tim_oc_id) id);
    }
}
template<uintptr_t prh> void Timer<prh>::set_oc_mode(OutputChannel id, OutputChannelMode mode) {
    timer_set_oc_mode(prh, (tim_oc_id) id, (tim_oc_mode) mode);
}
template<uintptr_t prh> void Timer<prh>::set_oc_preload(OutputChannel id, bool enable) {
    if (enable) {
        timer_enable_oc_preload(prh, (tim_oc_id) id);
    }
    else {
        timer_disable_oc_preload(prh, (tim_oc_id) id);
    }
}
template<uintptr_t prh>
void Timer<prh>::set_oc_polarity(OutputChannel id, OutputChannelPolarity polarity) {
    if (polarity == OutputChannelPolarity::Low) {
        timer_set_oc_polarity_low(prh, (tim_oc_id) id);
    }
    else if (polarity == OutputChannelPolarity::High) {
        timer_set_oc_polarity_high(prh, (tim_oc_id) id);
    }
}
template<uintptr_t prh> void Timer<prh>::set_oc_output(OutputChannel id, bool enable) {
    if (enable) {
        timer_enable_oc_output(prh, (tim_oc_id) id);
    }
    else {
        timer_disable_oc_output(prh, (tim_oc_id) id);
    }
}
template<uintptr_t prh> void Timer<prh>::set_oc_value(OutputChannel id, uint32_t value) {
    timer_set_oc_value(prh, (tim_oc_id) id, value);
}

template<uintptr_t prh> void Timer<prh>::set_ti1_ch123_xor(bool enable) {
    if (enable) {
        timer_set_ti1_ch123_xor(prh);
    }
    else {
        timer_set_ti1_ch1(prh);
    }
}
template<uintptr_t prh>
void Timer<prh>::set_ic_filter(InputChannel ic, InputChannelFilter flt) {
    timer_ic_set_filter(prh, (tim_ic_id) ic, (tim_ic_filter) flt);
}
template<uintptr_t prh> void Timer<prh>::set_ic_prescaler(InputChannel ic, uint8_t psc) {
    timer_ic_set_prescaler(prh, (tim_ic_id) ic, _int2psc(psc));
}
template<uintptr_t prh> void Timer<prh>::set_ic_input(InputChannel ic, InputChannelConf in) {
    timer_ic_set_input(prh, (tim_ic_id) ic, (tim_ic_input) in);
}
template<uintptr_t prh>
void Timer<prh>::set_ic_polarity(InputChannel ic, InputChannelPolarity pol) {
    timer_ic_set_polarity(prh, (tim_ic_id) ic, (tim_ic_pol) pol);
}
template<uintptr_t prh> void Timer<prh>::set_ic(InputChannel ic, bool enable) {
    if (enable) {
        timer_ic_enable(prh, (tim_ic_id) ic);
    }
    else {
        timer_ic_disable(prh, (tim_ic_id) ic);
    }
}

template<uintptr_t prh> AdvancedTimer<prh>& AdvancedTimer<prh>::get_instance() {
    static AdvancedTimer<prh> _instance;
    return _instance;
}

template<uintptr_t prh> void AdvancedTimer<prh>::set_repetition_counter(uint32_t value) {
    timer_set_repetition_counter(prh, value);
}

template<uintptr_t prh>
void AdvancedTimer<prh>::compare_control_update_on_trigger(bool enable) {
    if (enable) {
        timer_enable_compare_control_update_on_trigger(prh);
    }
    else {
        timer_disable_compare_control_update_on_trigger(prh);
    }
}
template<uintptr_t prh>
void AdvancedTimer<prh>::set_preload_complementry_enable_bits(bool enable) {
    if (enable) {
        timer_enable_preload_complementry_enable_bits(prh);
    }
    else {
        timer_disable_preload_complementry_enable_bits(prh);
    }
}

template<uintptr_t prh>
void AdvancedTimer<prh>::set_oc_idle_state(OutputChannel oc_id, bool v) {
    if (v) {
        timer_set_oc_idle_state_set(prh, (tim_oc_id) oc_id);
    }
    else {
        timer_set_oc_idle_state_unset(prh, (tim_oc_id) oc_id);
    }
}
template<uintptr_t prh> void AdvancedTimer<prh>::set_idle_state_in_run_mode(bool enable) {
    if (enable) {
        timer_set_enabled_off_state_in_run_mode(prh);
    }
    else {
        timer_set_disabled_off_state_in_run_mode(prh);
    }
}
template<uintptr_t prh> void AdvancedTimer<prh>::set_idle_state_in_idle_mode(bool enable) {
    if (enable) {
        timer_set_enabled_off_state_in_idle_mode(prh);
    }
    else {
        timer_set_disabled_off_state_in_idle_mode(prh);
    }
}

template<uintptr_t prh> void AdvancedTimer<prh>::set_break_main_output(bool enable) {
    if (enable) {
        timer_enable_break_main_output(prh);
    }
    else {
        timer_disable_break_main_output(prh);
    }
}
template<uintptr_t prh> void AdvancedTimer<prh>::break_automatic_output(bool enable) {
    if (enable) {
        timer_enable_break_automatic_output(prh);
    }
    else {
        timer_disable_break_automatic_output(prh);
    }
}
template<uintptr_t prh> void AdvancedTimer<prh>::set_break_polarity(OutputChannelPolarity pol) {
    if (pol == OutputChannelPolarity::Low) {
        timer_set_break_polarity_low(prh);
    }
    else if (pol == OutputChannelPolarity::High) {
        timer_set_break_polarity_high(prh);
    }
}
template<uintptr_t prh> void AdvancedTimer<prh>::set_break(bool enable) {
    if (enable) {
        timer_enable_break(prh);
    }
    else {
        timer_disable_break(prh);
    }
}
template<uintptr_t prh> void AdvancedTimer<prh>::set_break_lock(uint8_t lock_level) {
    switch (lock_level) {
        case 1: {
            timer_set_break_lock(prh, TIM_BDTR_LOCK_LEVEL_1);
        } break;
        case 2: {
            timer_set_break_lock(prh, TIM_BDTR_LOCK_LEVEL_2);
        } break;
        case 3: {
            timer_set_break_lock(prh, TIM_BDTR_LOCK_LEVEL_3);
        } break;
        default: break;
    }
}
template<uintptr_t prh> void AdvancedTimer<prh>::set_deadtime(uint32_t deadtime) {
    timer_set_deadtime(prh, deadtime);
}

}  // namespace stm32::timer

/** Advanced */
template class stm32::timer::Timer<TIM1>;
template class stm32::timer::AdvancedTimer<TIM1>;
/** GP High resolution */
template class stm32::timer::Timer<TIM2>;
template class stm32::timer::Timer<TIM5>;
/** GP Normal */
template class stm32::timer::Timer<TIM3>;
template class stm32::timer::Timer<TIM4>;
/** GP Triggers */
template class stm32::timer::Timer<TIM9>;
template class stm32::timer::Timer<TIM10>;
template class stm32::timer::Timer<TIM11>;
