#ifndef MMCC_USART_HPP
#define MMCC_USART_HPP

#include <Class.hpp>

#include <cstdint>
#include <utility>

#include <libopencm3/stm32/usart.h>

namespace stm32 {

/**
 * Hardware level control of USART peripherals.
 */
class USART : public mmcc::INonCopyable {
   public:
    enum StopBits {
        _0_5 = USART_STOPBITS_0_5,
        _1 = USART_STOPBITS_1,
        _1_5 = USART_STOPBITS_1_5,
        _2 = USART_STOPBITS_2,
    };

    enum Parity { None = USART_PARITY_NONE, Even = USART_PARITY_EVEN, Odd = USART_PARITY_ODD };

   protected:
    static uint8_t _locked_prh;
    uint32_t _prh;
    // Validity tracker for moves.
    bool _valid;

   public:
    USART(uint32_t prh);
    ~USART();

    USART(USART&& o) : _prh(std::exchange(o._prh, 0)), _valid(std::exchange(o._valid, false)) {}
    USART& operator=(USART&& o) {
        _prh = std::exchange(o._prh, 0);
        _valid = std::exchange(o._valid, false);
        return *this;
    }

    bool is_valid();
    operator bool() {
        return is_valid();
    }

    void set_baudrate(uint32_t baudrate);
    void set_databits(uint8_t bits);
    void set_stopbits(StopBits bits);
    void set_parity(Parity mode);
    void set_mode(bool tx, bool rx);
    void set_flow_control(bool rts, bool cts);
    void set_rx_dma(bool en);
    void set_tx_dma(bool en);
    void set_rx_interrupt(bool en);
    void set_tx_interrupt(bool en);
    void set_tx_complete_interrupt(bool en);
    void set_idle_interrupt(bool en);
    void set_error_interrupt(bool en);

    uint32_t get_baudrate();
    uint8_t get_databits();
    StopBits get_stopbits();
    Parity get_parity();
    bool get_mode_tx();
    bool get_mode_rx();
    bool get_flow_control_rts();
    bool get_flow_control_cts();
    bool get_rx_dma();
    bool get_tx_dma();
    bool get_rx_interrupt();
    bool get_tx_interrupt();
    bool get_tx_complete_interrupt();
    bool get_idle_interrupt();
    bool get_error_interrupt();

    void enable();
    void disable();
    bool enabled();

    /**
     * Wait until the send buffer is empty.
     */
    void wait_tx();
    bool tx_ready();
    /**
     * Wait until some data has been received.
     */
    void wait_rx();
    bool rx_ready();

    void tx(uint16_t data, bool blocking = true);
    uint16_t rx(bool blocking = true);
};

}  // namespace stm32

#endif /*MMCC_USART_HPP*/