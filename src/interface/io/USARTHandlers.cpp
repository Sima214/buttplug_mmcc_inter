#include "USARTHandlers.hpp"

namespace stm32 {

USARTSync::USARTSync(USART& hw, char sep) :
    _hw(hw), _caps_tx(hw.get_mode_tx()), _caps_rx(hw.get_mode_rx()), _sep(sep) {}

void USARTSync::send(const char* dat) {
    if (_caps_tx) {
        const char* iter = dat;
        char c = *iter;
        while (c != '\0') {
            _hw.tx(c, true);
            iter++;
            c = *iter;
        }
        if (_sep != '\0') {
            _hw.tx(_sep, true);
        }
    }
}

};  // namespace stm32