#ifndef MMCC_USB_HPP
#define MMCC_USB_HPP

#include <Class.hpp>
#include <interface/io/GPIO.hpp>

#include <cstdint>

#include <libopencm3/cm3/vector.h>
#include <libopencm3/usb/cdc.h>
#include <libopencm3/usb/usbd.h>

namespace stm32 {

class USB : mmcc::INonCopyable {
   public:
    enum Class : uint8_t { CDC = USB_CLASS_CDC, DATA = USB_CLASS_DATA };

    enum CDCSubClass : uint8_t { DLCM = USB_CDC_SUBCLASS_DLCM, ACM = USB_CDC_SUBCLASS_ACM };
    enum CDCProtocol : uint8_t { AT = USB_CDC_PROTOCOL_AT };

    enum TransferType : uint8_t {
        Control = USB_ENDPOINT_ATTR_CONTROL,
        Isochronous = USB_ENDPOINT_ATTR_ISOCHRONOUS,
        Bulk = USB_ENDPOINT_ATTR_BULK,
        Interrupt = USB_ENDPOINT_ATTR_INTERRUPT
    };

    class DeviceDescriptor : public usb_device_descriptor {
       public:
        DeviceDescriptor(Class c, uint16_t vendor, uint16_t product, uint16_t version,
                         uint8_t subclass = 0, uint8_t protocol = 0, uint8_t config_count = 1) {
            bLength = USB_DT_DEVICE_SIZE;
            bDescriptorType = USB_DT_DEVICE;
            bcdUSB = 0x0200;
            bDeviceClass = c;
            bDeviceSubClass = subclass;
            bDeviceProtocol = protocol;
            bMaxPacketSize0 = 64;
            idVendor = vendor;
            idProduct = product;
            bcdDevice = version;
            iManufacturer = 1;
            iProduct = 2;
            iSerialNumber = 3;
            bNumConfigurations = config_count;
        }
    };

    class EndpointDescriptor : public usb_endpoint_descriptor {
       public:
        EndpointDescriptor(uint8_t address, TransferType type, uint16_t max_packet_size,
                           uint8_t interval) {
            bLength = USB_DT_ENDPOINT_SIZE;
            bDescriptorType = USB_DT_ENDPOINT;
            bEndpointAddress = address;
            bmAttributes = type;
            wMaxPacketSize = max_packet_size;
            bInterval = interval;

            extra = nullptr;
            extralen = 0;
        }

        void setup() const;
        void setup(usbd_endpoint_callback cb) const;
        uint16_t read(void* buf, uint16_t buf_len) const;
        bool write(void* buf, uint16_t len) const;

        void setup(stm32::USB& usb) const;
        void setup(stm32::USB& usb, usbd_endpoint_callback cb) const;
        uint16_t read(stm32::USB& usb, void* buf, uint16_t buf_len) const;
        bool write(stm32::USB& usb, void* buf, uint16_t len) const;
    };

    class InterfaceDescriptor : public usb_interface_descriptor {
       public:
        template<uint8_t num_endpoints>
        InterfaceDescriptor(uint8_t interface_number, uint8_t alternate_setting,
                            const EndpointDescriptor (&endpoints)[num_endpoints], Class c,
                            uint8_t subclass = 0, uint8_t protocol = 0, uint8_t iname = 0) {
            bLength = USB_DT_INTERFACE_SIZE;
            bDescriptorType = USB_DT_INTERFACE;
            bInterfaceNumber = interface_number;
            bAlternateSetting = alternate_setting;
            bNumEndpoints = num_endpoints;
            bInterfaceClass = c;
            bInterfaceSubClass = subclass;
            bInterfaceProtocol = protocol;
            iInterface = iname;

            endpoint = endpoints;

            extra = nullptr;
            extralen = 0;
        }

        template<uint8_t num_endpoints, typename EX>
        InterfaceDescriptor(uint8_t interface_number, uint8_t alternate_setting,
                            const EndpointDescriptor (&endpoints)[num_endpoints], Class c,
                            uint8_t subclass, uint8_t protocol, EX& ex, uint8_t iname = 0) :
            InterfaceDescriptor(interface_number, alternate_setting, endpoints, c, subclass,
                                protocol, iname) {
            extra = &ex;
            extralen = sizeof(EX);
        }
    };

    class __attribute__((__packed__)) CDCACMFunctionalDescriptors {
       public:
        usb_cdc_header_descriptor header;
        usb_cdc_call_management_descriptor call_mgmt;
        usb_cdc_acm_descriptor acm;
        usb_cdc_union_descriptor cdc_union;

        CDCACMFunctionalDescriptors(uint8_t ctl_iface, uint8_t dat_iface) {
            header.bFunctionLength = sizeof(struct usb_cdc_header_descriptor);
            header.bDescriptorType = CS_INTERFACE;
            header.bDescriptorSubtype = USB_CDC_TYPE_HEADER;
            header.bcdCDC = 0x0110;
            call_mgmt.bFunctionLength = sizeof(struct usb_cdc_call_management_descriptor);
            call_mgmt.bDescriptorType = CS_INTERFACE;
            call_mgmt.bDescriptorSubtype = USB_CDC_TYPE_CALL_MANAGEMENT;
            call_mgmt.bmCapabilities = 0;
            call_mgmt.bDataInterface = dat_iface;
            acm.bFunctionLength = sizeof(struct usb_cdc_acm_descriptor);
            acm.bDescriptorType = CS_INTERFACE;
            acm.bDescriptorSubtype = USB_CDC_TYPE_ACM;
            acm.bmCapabilities = 0;
            cdc_union.bFunctionLength = sizeof(struct usb_cdc_union_descriptor);
            cdc_union.bDescriptorType = CS_INTERFACE;
            cdc_union.bDescriptorSubtype = USB_CDC_TYPE_UNION;
            cdc_union.bControlInterface = ctl_iface;
            cdc_union.bSubordinateInterface0 = dat_iface;
        }
    };

    class ConfigDescriptor : public usb_config_descriptor {
       public:
        class Interface : public usb_interface {
           public:
            template<uint8_t num_alts> Interface(const InterfaceDescriptor (&desc)[num_alts]) {
                cur_altsetting = nullptr;
                num_altsetting = num_alts;
                iface_assoc = nullptr;
                altsetting = desc;
            }
        };

       protected:
       public:
        template<uint8_t num_interfaces>
        ConfigDescriptor(const Interface (&ifaces)[num_interfaces], uint16_t max_power = 100,
                         bool self_powered = false, bool remote_wakeup = false,
                         uint8_t iname = 0) {
            bLength = USB_DT_CONFIGURATION_SIZE;
            bDescriptorType = USB_DT_CONFIGURATION;
            wTotalLength = 0;
            bNumInterfaces = num_interfaces;
            bConfigurationValue = 1;
            iConfiguration = iname;
            bmAttributes = USB_CONFIG_ATTR_DEFAULT;
            if (self_powered) {
                bmAttributes |= USB_CONFIG_ATTR_SELF_POWERED;
            }
            if (remote_wakeup) {
                bmAttributes |= USB_CONFIG_ATTR_REMOTE_WAKEUP;
            }
            max_power /= 2;
            if (max_power > 250) {
                max_power = 250;
            }
            bMaxPower = max_power;
            interface = ifaces;
        }
    };

    using usb_interrupt_callback = void(USB&);
    using usb_event_callback = void();

   protected:
    usbd_device* _dev;
    void* _usrptr;
    usb_interrupt_callback* _inter_cb;
    Pin _pins;

    USB();

    friend class EndpointDescriptor;

   public:
    static USB& get_instance();

    void set_user_pointer(void* v) {
        _usrptr = v;
    }
    void* get_user_pointer() {
        return _usrptr;
    }

    void init(const DeviceDescriptor&, const ConfigDescriptor&,
              std::pair<const char* const*, uint16_t> strings,
              std::pair<uint8_t*, uint16_t> ctl_buf);
    void poll();
    void disconnect(bool dc = true);

    void register_interrupt_callback(usb_interrupt_callback* cb);

    void register_reset_callback(usb_event_callback cb);
    void register_suspend_callback(usb_event_callback cb);
    void register_resume_callback(usb_event_callback cb);
    void register_sof_callback(usb_event_callback cb);

    void register_config_callback(usbd_set_config_callback cb);
    void register_control_callback(usbd_control_callback cb, uint8_t type, uint8_t type_mask);
    void register_set_altsetting_callback(usbd_set_altsetting_callback cb);

   protected:
    static void on_usb_interrupt();
};

}  // namespace stm32

#endif /*MMCC_USB_HPP*/