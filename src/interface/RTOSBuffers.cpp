#include "RTOSBuffers.hpp"

namespace rtos {

StreamBuffer::StreamBuffer(size_t size, size_t trigger_level) {
    _handle = xStreamBufferCreate(size, trigger_level);
}

StreamBuffer::~StreamBuffer() {
    if (_handle != nullptr) {
        vStreamBufferDelete(_handle);
        _handle = nullptr;
    }
}

size_t StreamBuffer::send(const void* d, size_t l, float ms) {
    TickType_t ticks = ms2ticks(ms);
    return xStreamBufferSend(_handle, d, l, ticks);
}

size_t StreamBuffer::send_from_isr(const void* d, size_t l) {
    BaseType_t task_switch = 0;
    size_t read = xStreamBufferSendFromISR(_handle, d, l, &task_switch);
    if (task_switch) {
        taskYIELD();
    }
    return read;
}

size_t StreamBuffer::receive(void* d, size_t l, float ms) {
    TickType_t ticks = ms2ticks(ms);
    return xStreamBufferReceive(_handle, d, l, ticks);
}

size_t StreamBuffer::receive_from_isr(void* d, size_t l) {
    BaseType_t task_switch = 0;
    size_t read = xStreamBufferReceiveFromISR(_handle, d, l, &task_switch);
    if (task_switch) {
        taskYIELD();
    }
    return read;
}

size_t StreamBuffer::bytes_available() {
    return xStreamBufferBytesAvailable(_handle);
}

size_t StreamBuffer::spaces_available() {
    return xStreamBufferSpacesAvailable(_handle);
}

bool StreamBuffer::set_trigger_level(size_t trigger_level) {
    return xStreamBufferSetTriggerLevel(_handle, trigger_level) == pdTRUE;
}

bool StreamBuffer::reset() {
    return xStreamBufferReset(_handle) == pdPASS;
}

bool StreamBuffer::is_empty() {
    return xStreamBufferIsEmpty(_handle) == pdTRUE;
}

bool StreamBuffer::is_full() {
    return xStreamBufferIsFull(_handle) == pdTRUE;
}

MessageBuffer::MessageBuffer(size_t size) {
    _handle = xMessageBufferCreate(size);
}

MessageBuffer::~MessageBuffer() {
    if (_handle != nullptr) {
        vMessageBufferDelete(_handle);
        _handle = nullptr;
    }
}

size_t MessageBuffer::send(const void* d, size_t l, float ms) {
    TickType_t ticks = ms2ticks(ms);
    return xMessageBufferSend(_handle, d, l, ticks);
}

size_t MessageBuffer::send_from_isr(const void* d, size_t l) {
    BaseType_t task_switch = 0;
    size_t read = xMessageBufferSendFromISR(_handle, d, l, &task_switch);
    if (task_switch) {
        taskYIELD();
    }
    return read;
}

size_t MessageBuffer::receive(void* d, size_t l, float ms) {
    TickType_t ticks = ms2ticks(ms);
    return xMessageBufferReceive(_handle, d, l, ticks);
}

size_t MessageBuffer::receive_from_isr(void* d, size_t l) {
    BaseType_t task_switch = 0;
    size_t read = xMessageBufferReceiveFromISR(_handle, d, l, &task_switch);
    if (task_switch) {
        taskYIELD();
    }
    return read;
}

size_t MessageBuffer::spaces_available() {
    return xMessageBufferSpacesAvailable(_handle);
}

bool MessageBuffer::reset() {
    return xMessageBufferReset(_handle) == pdPASS;
}

bool MessageBuffer::is_empty() {
    return xMessageBufferIsEmpty(_handle) == pdTRUE;
}

bool MessageBuffer::is_full() {
    return xMessageBufferIsFull(_handle) == pdTRUE;
}

}  // namespace rtos
