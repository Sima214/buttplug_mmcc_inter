#include "RTOS.hpp"
#include "RTOSEventGroups.hpp"
#include "projdefs.h"

namespace rtos {

EventGroup::EventGroup() : EventGroup(xEventGroupCreate()) {}

EventGroup::~EventGroup() {
    if (_handle != nullptr) {
        vEventGroupDelete(_handle);
        _handle = nullptr;
    }
}

EventBits_t EventGroup::wait(EventBits_t mask, bool clear, bool mask_and, float ms) {
    TickType_t ticks = ms2ticks(ms);
    return xEventGroupWaitBits(_handle, mask, clear ? pdTRUE : pdFALSE,
                               mask_and ? pdTRUE : pdFALSE, ticks);
}

EventBits_t EventGroup::set(EventBits_t mask) {
    if (is_inside_interrupt()) {
        // BaseType_t task_switch = 0;
        // EventBits_t value = xEventGroupSetBitsFromISR(_handle, mask, &task_switch);
        // if (task_switch) {
        //     taskYIELD();
        // }
        // return value;
        return -1;
    }
    else {
        return xEventGroupSetBits(_handle, mask);
    }
}

EventBits_t EventGroup::clear(EventBits_t mask) {
    if (is_inside_interrupt()) {
        // BaseType_t task_switch = 0;
        // EventBits_t value = xEventGroupClearBitsFromISR(_handle, mask, &task_switch);
        // if (task_switch) {
        //     taskYIELD();
        // }
        // return value;
        return -1;
    }
    else {
        return xEventGroupClearBits(_handle, mask);
    }
}

EventBits_t EventGroup::get() {
    if (is_inside_interrupt()) {
        return xEventGroupGetBitsFromISR(_handle);
    }
    else {
        return xEventGroupGetBits(_handle);
    }
}

EventBits_t EventGroup::sync(EventBits_t set_mask, EventBits_t wait_mask, float ms) {
    TickType_t ticks = ms2ticks(ms);
    return xEventGroupSync(_handle, set_mask, wait_mask, ticks);
}

}  // namespace rtos
