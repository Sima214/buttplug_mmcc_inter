#include "RTOS.hpp"
#include "portable.h"

#include <interface/io/Timer.hpp>

#include <cstdlib>

void _rtos_bindings_on_assert(char* file, int line) {
    rtos::Kernel::get_instance().on_assert(file, line);
}
void _rtos_bindings_on_stack_overflow(TaskHandle_t task, char* task_name) {
    rtos::Kernel::get_instance().on_stack_overflow(task, task_name);
}
void _rtos_bindings_get_idle_task_memory(StaticTask_t** idle_task_tcb_buffer,
                                         StackType_t** idle_task_stack_buffer,
                                         uint32_t* idle_task_stack_size) {
    rtos::Kernel::get_instance().get_idle_task_memory(
         idle_task_tcb_buffer, idle_task_stack_buffer, idle_task_stack_size);
}
void _rtos_bindings_pre_sleep_processing(void* arg) {
    auto& expected_idle_time = *((TickType_t*) arg);
    rtos::Kernel::get_instance().on_pre_sleep_processing(expected_idle_time);
}
void _rtos_bindings_post_sleep_processing(void* arg) {
    auto& expected_idle_time = *((TickType_t*) arg);
    rtos::Kernel::get_instance().on_post_sleep_processing(expected_idle_time);
}
void _rtos_bindings_on_idle() {
    rtos::Kernel::get_instance().on_idle();
}

#if configUSE_TRACE_FACILITY and configGENERATE_RUN_TIME_STATS

    #include <Objects.hpp>

static stm32::timer::Timer<TIM5>& hrtim = stm32::timer::Timer<TIM5>::get_instance();

void _rtos_bindings_stats_init() {
    mmcc::cli_task.register_command("rtos", "Print RTOS statistics.",
                                    rtos::Kernel::print_stats);

    hrtim.set_mode(stm32::timer::CkIntDiv::DIV2, stm32::timer::Alignment::Edge, stm32::timer::Direction::Up);
    hrtim.set_preload(false);
    hrtim.set_prescaler(4800 - 1);
    hrtim.set_period(0xffffffff);
    hrtim.set_counter(0);
    hrtim.enable();
}
uint32_t _rtos_bindings_stats_get_time() {
    return hrtim.get_counter();
}

int rtos::Kernel::print_stats(int, char*[]) {
    uint32_t total_run_time = 0;
    constexpr size_t task_status_buf_len = 8;
    static TaskStatus_t task_status_buf[task_status_buf_len];
    size_t c = uxTaskGetSystemState(task_status_buf, task_status_buf_len, &total_run_time);

    char buf[96];
    int written;

    for (size_t i = 0; i < c; i++) {
        const TaskStatus_t* cts = task_status_buf + i;
        char cts_state;
        switch (cts->eCurrentState) {
            case eRunning: {
                cts_state = 'R';
            } break;
            case eReady: {
                cts_state = 'r';
            } break;
            case eBlocked: {
                cts_state = 'B';
            } break;
            case eSuspended: {
                cts_state = 'S';
            } break;
            case eDeleted: {
                cts_state = 'D';
            } break;
            case eInvalid: {
                cts_state = 'I';
            } break;
            default: {
                cts_state = '-';
            } break;
        }
        written = std::snprintf(buf, sizeof(buf), "%3d\t%s\t%c\t%u\t%u\t%u\t%u\n",
                                cts->xTaskNumber, cts->pcTaskName, cts_state,
                                cts->uxCurrentPriority, cts->uxBasePriority,
                                cts->ulRunTimeCounter, cts->usStackHighWaterMark);
        mmcc::cli_task.print(buf, written);
    }
    written = std::snprintf(buf, sizeof(buf), "Total time: %u\n", total_run_time);
    mmcc::cli_task.print(buf, written);
    size_t total_tick_count = get_instance().get_tickcount();
    written = std::snprintf(buf, sizeof(buf), "Total tick count: %u\n", total_tick_count);
    mmcc::cli_task.print(buf, written);
    HeapStats_t heap_stats;
    vPortGetHeapStats(&heap_stats);
    written = std::snprintf(
         buf, sizeof(buf), "Heap: %u\t%u\t%u\t%u\t%u\t%u\t%u\n",
         heap_stats.xAvailableHeapSpaceInBytes, heap_stats.xSizeOfLargestFreeBlockInBytes,
         heap_stats.xSizeOfSmallestFreeBlockInBytes, heap_stats.xNumberOfFreeBlocks,
         heap_stats.xMinimumEverFreeBytesRemaining, heap_stats.xNumberOfSuccessfulAllocations,
         heap_stats.xNumberOfSuccessfulFrees);
    mmcc::cli_task.print(buf, written);
    return 0;
}

#endif

namespace rtos {

void Kernel::start() {
    on_start();
    vTaskStartScheduler();
    on_end();
}
void Kernel::end() {
    vTaskEndScheduler();
}

void Kernel::suspend_all() {
    vTaskSuspendAll();
}
bool Kernel::resume_all() {
    return xTaskResumeAll() != pdFALSE;
}
void Kernel::step_tick(TickType_t ticks) {
    vTaskStepTick(ticks);
}
bool Kernel::catch_up_ticks(TickType_t ticks) {
    return xTaskCatchUpTicks(ticks) != pdFALSE;
}

TickType_t Kernel::get_tickcount() {
    if (is_inside_interrupt()) {
        return xTaskGetTickCountFromISR();
    }
    else {
        return xTaskGetTickCount();
    }
}
Kernel::State Kernel::get_scheduler_state() {
    return (Kernel::State) xTaskGetSchedulerState();
}
UBaseType_t Kernel::get_number_of_tasks() {
    return uxTaskGetNumberOfTasks();
}

void Kernel::yield() {
    taskYIELD();
}
void Kernel::enter_critical() {
    taskENTER_CRITICAL();
}
void Kernel::exit_critical() {
    taskEXIT_CRITICAL();
}
uint32_t Kernel::enter_critical_from_isr() {
    return taskENTER_CRITICAL_FROM_ISR();
}
void Kernel::exit_critical_from_isr(uint32_t mask) {
    taskEXIT_CRITICAL_FROM_ISR(mask);
}
void Kernel::disable_interrupts() {
    taskDISABLE_INTERRUPTS();
}
void Kernel::enable_interrupts() {
    taskENABLE_INTERRUPTS();
}

void Kernel::on_assert(char*, int) {
    std::abort();
}
void Kernel::on_stack_overflow(TaskHandle_t, char*) {
    std::abort();
}
void Kernel::on_pre_sleep_processing(TickType_t&) {}
void Kernel::on_post_sleep_processing(TickType_t&) {}
void Kernel::on_idle() {}

}  // namespace rtos
