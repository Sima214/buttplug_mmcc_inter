#ifndef MMCC_RTOSTASK_HPP
#define MMCC_RTOSTASK_HPP

#include <Class.hpp>
#include <RTOS.hpp>

#include <cstdlib>
#include <utility>

#include <FreeRTOS.h>
#include <task.h>

#include <_types/_uint32_t.h>

namespace rtos {

class iTask : public mmcc::INonCopyable {
   public:
    enum State {
        Running = eRunning,
        Ready = eReady,
        Blocked = eBlocked,
        Suspended = eSuspended,
        Deleted = eDeleted,
        Invalid = eInvalid
    };

    using NotificationAction = eNotifyAction;

   protected:
    TaskHandle_t _handle;

   public:
    iTask(TaskHandle_t handle) : _handle(handle) {}
    ~iTask() = default;
    iTask(iTask&& o) : _handle(std::exchange(o._handle, nullptr)) {}
    iTask& operator=(iTask&& o) {
        _handle = std::exchange(o._handle, nullptr);
        return *this;
    }

    void delay_ticks(TickType_t ticks);
    void delay_ticks_until(TickType_t& previous_wake_time, TickType_t time_increment);
    void delay(float ms);
    void abort_delay();

    void set_priority(UBaseType_t prio);
    UBaseType_t get_priority();

    void suspend();
    void resume();
    void stop();

    bool notify(UBaseType_t index, NotificationAction action, uint32_t value);
    bool notify_give(UBaseType_t index) {
        return notify(index, NotificationAction::eIncrement, 0);
    }
    uint32_t notify_take(UBaseType_t index, bool clear_count_on_exit, float ms_to_wait);
    std::pair<bool, uint32_t> notify_and_query(UBaseType_t index, NotificationAction action,
                                               uint32_t value);
    std::pair<bool, uint32_t> notify_wait(UBaseType_t index, uint32_t clear_mask_on_entry,
                                          uint32_t clear_mask_on_exit, float ms_to_wait = rtos::MAX_DELAY);
    bool notify_state_clear(UBaseType_t index);
    uint32_t notify_value_clear(UBaseType_t index, uint32_t clear_mask);

    State get_state();
    const char* task_get_name();
};

class Task : public iTask {
   protected:
    Task(TaskHandle_t handle) : iTask(handle) {
        if (handle == nullptr) {
            std::abort();
        }
    }

   public:
    virtual ~Task();

   protected:
    virtual void run() = 0;

    static void _stack_run_bindings(void*);
};

class HeapTask : public Task {
   public:
    HeapTask(const char* const name, const uint32_t stack_size, UBaseType_t priority);
    virtual ~HeapTask() = default;
};

template<size_t N = configMINIMAL_STACK_SIZE> class StaticTask : public Task {
   protected:
    StaticTask_t _state;
    StackType_t _stack[N];

   public:
    StaticTask(const char* const name, UBaseType_t priority) :
        Task(xTaskCreateStatic(_stack_run_bindings, name, N, this, priority, _stack, &_state)) {
    }
    virtual ~StaticTask() = default;
};

}  // namespace rtos

#endif /*MMCC_RTOSTASK_HPP*/