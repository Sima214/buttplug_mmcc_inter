#ifndef MMCC_RTOSMUTEX_HPP
#define MMCC_RTOSMUTEX_HPP

#include <Class.hpp>
#include <RTOS.hpp>

#include <utility>

#include <FreeRTOS.h>
#include <semphr.h>

namespace rtos {

template<bool recursive = false> class Mutex : public mmcc::INonCopyable {
   protected:
    SemaphoreHandle_t _handle = nullptr;

   protected:
    Mutex(SemaphoreHandle_t handle) : _handle(handle) {}

   public:
    Mutex() : Mutex(recursive ? xSemaphoreCreateRecursiveMutex() : xSemaphoreCreateMutex()) {}
    ~Mutex() {
        if (_handle != nullptr) {
            vSemaphoreDelete(_handle);
            _handle = nullptr;
        }
    }
    Mutex(Mutex&& o) : _handle(std::exchange(o._handle, nullptr)) {}
    Mutex& operator=(Mutex&& o) {
        _handle = std::exchange(o._handle, nullptr);
        return *this;
    }

    size_t get_count() {
        return uxSemaphoreGetCount(_handle);
    }
    bool take(float ms = rtos::MAX_DELAY) {
        TickType_t ticks = ms2ticks(ms);
        bool taken;
        if constexpr (recursive) {
            taken = xSemaphoreTakeRecursive(_handle, ticks) == pdTRUE;
        }
        else {
            taken = xSemaphoreTake(_handle, ticks) == pdTRUE;
        }
        return taken;
    }
    bool give() {
        bool ok;
        if constexpr (recursive) {
            ok = xSemaphoreGiveRecursive(_handle) == pdTRUE;
        }
        else {
            ok = xSemaphoreGive(_handle) == pdTRUE;
        }
        return ok;
    }
};

template<bool recursive = false> class StaticMutex : public Mutex<recursive> {
   protected:
    StaticSemaphore_t _handle_storage;

   public:
    StaticMutex() :
        Mutex<recursive>(recursive ? xSemaphoreCreateRecursiveMutexStatic(&_handle_storage) :
                                     xSemaphoreCreateMutexStatic(&_handle_storage)) {}
};

}  // namespace rtos

#endif /*MMCC_RTOSMUTEX_HPP*/