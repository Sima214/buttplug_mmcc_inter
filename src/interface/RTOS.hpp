#ifndef MMCC_RTOS_HPP
#define MMCC_RTOS_HPP

#include <Class.hpp>

#include <limits>

#include <FreeRTOS.h>
#include <task.h>

namespace rtos {

inline bool is_inside_interrupt() {
    return xPortIsInsideInterrupt() != pdFALSE;
}

constexpr float MAX_DELAY = std::numeric_limits<float>::infinity();

constexpr TickType_t ms2ticks(float ms) {
    if (ms == MAX_DELAY) {
        return portMAX_DELAY;
    }
    return ((TickType_t) (ms * configTICK_RATE_HZ)) / 1000;
}

class Kernel : public mmcc::INonCopyable {
   public:
    enum State {
        SchedulerNotStarted = taskSCHEDULER_NOT_STARTED,
        SchedulerRunning = taskSCHEDULER_RUNNING,
        SchedulerSuspended = taskSCHEDULER_SUSPENDED
    };

   protected:
    static Kernel& instance_ref;

   public:
    static Kernel& get_instance() {
        return instance_ref;
    }

    Kernel() = default;
    virtual ~Kernel() = default;
    Kernel& operator=(Kernel&&) = delete;
    Kernel(Kernel&&) = delete;

    void start();
    void end();
    void suspend_all();
    bool resume_all();
    void step_tick(TickType_t);
    bool catch_up_ticks(TickType_t);

    TickType_t get_tickcount();
    State get_scheduler_state();
    UBaseType_t get_number_of_tasks();

    static void yield();
    static void enter_critical();
    static void exit_critical();
    static uint32_t enter_critical_from_isr();
    static void exit_critical_from_isr(uint32_t);
    static void disable_interrupts();
    static void enable_interrupts();

    virtual void on_assert(char* file, int line);
    virtual void on_stack_overflow(TaskHandle_t task, char* task_name);

    virtual void get_idle_task_memory(StaticTask_t** idle_task_tcb_buffer,
                                      StackType_t** idle_task_stack_buffer,
                                      uint32_t* idle_task_stack_size) = 0;

    virtual void on_pre_sleep_processing(TickType_t& expected_idle_time);
    virtual void on_post_sleep_processing(TickType_t& expected_idle_time);

    virtual void on_idle();
    virtual void on_start() = 0;
    virtual void on_end() = 0;

    static int print_stats(int argc, char* argv[]);
};

}  // namespace rtos

#endif /*MMCC_RTOS_HPP*/